<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	//"password" => "Passwords must be at least six characters and match the confirmation.",
    "password" => "Пароль должен включать не менее 6 символов",
	//"user" => "We can't find a user with that e-mail address.",
    "user" => "Пользователь с таким почтовым адресом не зарегестрирован.",
	//"token" => "This password reset token is invalid.",
    "token" => "Указанный код сброса пароля недействителен.",
	//"sent" => "We have e-mailed your password reset link!",
    "sent" => "На указанный почтовый ящик выслано письмо со ссылкой для восстановления пароля.",
	//"reset" => "Your password has been reset!",
    "reset" => "Пароль был успешно сброшен.",

];
