<?php

return [
    'home' => 'Главная',
    'news' => 'Новости',
    'employees' => 'Сотрудники кафедры',
    'publications' => 'Публикации',
    'gallery' => 'Галерея',
    'studyplan' => 'Учебный план',
];