<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "The :attribute must be accepted.",
	"active_url"           => "Значение поля ':attribute' не соответствует URL формату.",
	"after"                => "The :attribute must be a date after :date.",
	"alpha"                => "':attribute' может содержать только литеры.",
	"alpha_dash"           => "':attribute' может содержать только литеры, цифры, и нижние подчеркивания.",
	"alpha_num"            => "':attribute' может содержать только литеры и цифры.",
	"array"                => "Поле ':attribute' должно быть массивом.",
	"before"               => "The :attribute must be a date before :date.",
	"between"              => [
		"numeric" => "Значение ':attribute' должно быть между :min и :max.",
		"file"    => "Размер ':attribute' должен быть между :min и :max килобайт.",
		"string"  => "Поле ':attribute' должно быть от :min до :max символов.",
		"array"   => "The :attribute must have between :min and :max items.",
	],
	"boolean"              => "Поле ':attribute' должно принимать значения true или false.",
	"confirmed"            => "The :attribute confirmation does not match.",
	"date"                 => "The :attribute is not a valid date.",
	"date_format"          => "Значение поля ':attribute' не соответствует формату :format.",
	"different"            => "Значения полей ':attribute' и ':other' должны быть разными.",
	"digits"               => "The :attribute must be :digits digits.",
	"digits_between"       => "Значение поля ':attribute' должно содержать от :min до :max цифр.",
	"email"                => "Значение поля ':attribute' должно быть указано в email формате.",
	"filled"               => "Поле ':attribute' является обязательным.",
	"exists"               => "Значение поля ':attribute' задано неверно.",
	"image"                => "Файл ':attribute' должен быть изображнием.",
	"in"                   => "Значение поля ':attribute' задано неверно.",
	"integer"              => "Значение поля ':attribute' должно быть целочисленным.",
	"ip"                   => "The :attribute must be a valid IP address.",
	"max"                  => [
		"numeric" => "The :attribute may not be greater than :max.",
		"file"    => "The :attribute may not be greater than :max kilobytes.",
		"string"  => "The :attribute may not be greater than :max characters.",
		"array"   => "The :attribute may not have more than :max items.",
	],
    "message_sender_receiver" => "Вы пытаетесь отправить сообщение самому себе",
	"mimes"                => "The :attribute must be a file of type: :values.",
	"min"                  => [
		"numeric" => "Значение поля ':attribute' должно быть больше или равно :min.",
		"file"    => "Размер файла ':attribute' должен быть по крайней мере :min килобайт.",
		"string"  => "Поле ':attribute' должно содержать минимум :min символов.",
		"array"   => "The ':attribute' must have at least :min items.",
	],
	"not_in"               => "The selected :attribute is invalid.",
	"numeric"              => "The :attribute must be a number.",
	"regex"                => "The :attribute format is invalid.",
	"required"             => "Поле ':attribute' является обязательным.",
	"required_if"          => "The :attribute field is required when :other is :value.",
	"required_with"        => "The :attribute field is required when :values is present.",
	"required_with_all"    => "The :attribute field is required when :values is present.",
	"required_without"     => "The :attribute field is required when :values is not present.",
	"required_without_all" => "The :attribute field is required when none of :values are present.",
	"same"                 => "Поле ':attribute' и ':other' должны совпадать.",
	"size"                 => [
		"numeric" => "The :attribute must be :size.",
		"file"    => "The :attribute must be :size kilobytes.",
		"string"  => "The :attribute must be :size characters.",
		"array"   => "The :attribute must contain :size items.",
	],
	"unique"               => "Указанное значение ':attribute' уже занято.",
	"url"                  => "Неверный формат поля ':attribute'.",
	"timezone"             => "The :attribute must be a valid zone.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => [
		'attribute-name' => [
			'rule-name' => 'custom-message',
		],
        'receivers' => [
            'receivers_list' => 'Неверный список получателей'
        ]
	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => [
        'title_ru' => 'Заголовок (русскоязычный)',
        'title_en' => 'Заголовок (англоязычный)',
        'text_ru'  => 'Текст (русскоязычный)',
        'text_en'  => 'Текст (англоязычный)',
        'hours'    => 'Количество часов',
        'degree'   => 'Уровень',
        'semester' => 'Семестр',
        'name'     => 'Наименование',
        'year'     => 'Год',
        'employee_id' => 'Преподаватель',
        'name_ru' => 'Наименование',
        'text'     => 'Текст',
        'title'    => 'Заголовок',
        'submitter_email' => 'Email'
    ],

];
