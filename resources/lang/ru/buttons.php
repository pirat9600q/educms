<?php

return [
    'add' => 'Добавить :object',
    'edit' => 'Редактировать :object',
    'delete' => 'Удалить :object',
];