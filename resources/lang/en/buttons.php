<?php

return [
    'add' => 'Add :object',
    'edit' => 'Edit :object',
    'delete' => 'Delete :object',
];