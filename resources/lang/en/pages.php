<?php

return [
    'home' => 'Home',
    'news' => 'News',
    'employees' => 'Employees',
    'publications' => 'Publications',
    'gallery' => 'Gallery',
    'studyplan' => 'Study plan',
];