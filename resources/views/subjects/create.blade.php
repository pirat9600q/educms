@extends('app')

@section('content')
    @include('errors')
    <div class="panel panel-default">
        <div class="panel-body">
            @include('subjects._form', [
                'action' => action('SubjectsController@store'),
                'method' => 'POST',
                'subject'=> null,
            ])
        </div>
    </div>
@endsection

@section('additional_styles')

@endsection

@section('additional_scripts')

@endsection