{!! Form::open([
    'url' => $action,
    'method' => $method,
    'class' => 'form-horizontal'
]) !!}

    <div class="form-group">
        {!! Form::label('name', 'Название', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
            {!! Form::text('name', $subject ? $subject->name : null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('year', 'Год', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
            {!! Form::text('year', $subject ? $subject->year : null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('employee_id', 'Преподаватель', ['class' => 'control-label col-xs-3']) !!}
        <div class="col-xs-9">
            {!! Form::select('employee_id', $employees, $subject ? $subject->employee_id : null) !!}
        </div>
    </div>

    <div class="col-xs-offset-3 col-xs-9">
        {!! Form::submit('Отправить', ['class' => 'btn btn-success']) !!}
    </div>

{!! Form::close() !!}