@extends('app')

@section('content')
    @if (Auth::user() && Auth::user()->can('update.subjects'))
        <a href="{{ action('SubjectsController@edit', $subject->id) }}" class="btn btn-default">
            <span class="glyphicon glyphicon-edit"></span>
            Редактировать
        </a>
    @endif
    @if (Auth::user() && Auth::user()->can('delete.subjects'))
        {!! Form::open(['url' => action('SubjectsController@destroy', $subject->id),
        'method' => 'DELETE', 'style' => 'display:inline']) !!}
        <a href="#" onclick="$(this).closest('form').submit();" class="btn btn-danger">
            <span class="glyphicon glyphicon-remove"></span>
            Удалить
        </a>
        {!! Form::close() !!}
        <br/> <br/>
    @endif
    <div class="panel panel-default">
        <div class="panel-body">
            <p><b>Название: </b> {{ $subject->name }}</p>

            <p><b>Год: </b> {{ $subject->year }}</p>

            <p><b>Преподаватель: </b>
                <a href="{{ action('EmployeesController@show', $subject->employee->id) }}">
                    {{ $subject->employee->fullName() }}
                </a>
            </p>
        </div>
    </div>
@endsection

@section('additional_styles')

@endsection

@section('additional_scripts')

@endsection