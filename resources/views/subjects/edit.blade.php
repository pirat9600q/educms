@extends('app')

@section('content')
    @if (Auth::user() && Auth::user()->can('delete.subjects'))
        {!! Form::open(['url' => action('SubjectsController@destroy', $subject->id),
        'method' => 'DELETE', 'style' => 'display:inline']) !!}
        <a href="#" onclick="$(this).closest('form').submit();" class="btn btn-danger">
            <span class="glyphicon glyphicon-remove"></span>
            Удалить
        </a>
        {!! Form::close() !!}
        <br/> <br/>
    @endif
    @include('errors')
    <div class="panel panel-default">
        <div class="panel-body">
            @include('subjects._form', [
                'action' => action('SubjectsController@update', $subject->id),
                'method' => 'PATCH',
                'subject'=> $subject,
            ])
        </div>
    </div>
@endsection

@section('additional_styles')

@endsection

@section('additional_scripts')

@endsection