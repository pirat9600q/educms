@extends('app')

@section('content')
    @if(Auth::user() && Auth::user()->can('create.subjects'))
        <a href="{{ action('SubjectsController@create') }}" class="btn btn-success">
            <span class="glyphicon glyphicon-plus"></span>
            Добавить
        </a> <br><br>
    @endif
    <div class="panel panel-default">
        <div class="panel-body">
            @foreach($employees as $emp)
                @if($emp->subjects->count() > 0)
                    <h3>{{ $emp->fullName() }}</h3>
                    <ul>
                    @foreach($emp->subjects()->orderBy('year', 'desc')->get() as $subject)
                        <li>
                            @if(Auth::user() && Auth::user()->can('update.subjects'))
                                <a href="{{ action('SubjectsController@edit', $subject->id) }}">
                                    <span class="glyphicon glyphicon-edit"></span>
                                </a>
                            @endif
                            {{ $subject->name }} ({{ $subject->year }})
                        </li>
                    @endforeach
                    </ul>
                    <br/>
                @endif
            @endforeach
        </div>
    </div>
@endsection