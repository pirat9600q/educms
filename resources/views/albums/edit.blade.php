@extends('app')

@section('content')
    @include('errors')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">

                            <div class="col-xs-4">
                                @if ($images->count() > 0)
                                    <img src="{{ $images->first()->image->url('thumbnail') }}" class="img-thumbnail" id="album-thumbnail" alt=""/>
                                @else
                                    <img src="{{ asset('/img/image-placeholder.png') }}" class="img-thumbnail" id="album-thumbnail" alt=""/>
                                @endif
                            </div>


                        <div class="col-xs-8">
                            <form action="{{ action('AlbumsController@update', $album->id) }}" method="POST" class="form">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <input type="hidden" name="_method" value="PATCH"/>

                                <div class="form-group">
                                    <input type="text" class="form-control" name="name_ru" value="{{ $album->name_ru }}"/>
                                </div>

                                <div class="form-group">
                                    <textarea name="description_ru" class="form-control" id="" cols="30" rows="3">{{ $album->description_ru }}</textarea>
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" name="name_en" value="{{ $album->name_en }}"/>
                                </div>

                                <div class="form-group">
                                    <textarea name="description_en" class="form-control" id="" cols="30" rows="3">{{ $album->description_en }}</textarea>
                                </div>

                                <div class="form-group">
                                    <input class="btn btn-success" type="submit" value="Сохранить"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <p class="help-block text-center"><small>Порядок изображений можно изменять путём перетаскивания.</small></p>
                    <div class="row" id="image-grid">
                    @foreach($images as $image)
                        <div class="col-xs-3 ui-state-default" style="padding-bottom: 10px;" id="{{ $image->id }}">
                            <a href="{{ $image->image->url('original') }}"
                                data-lightbox="{{ $image->album->id }}"
                                data-title="{{ $image->name() }}">
                                <img class="img-thumbnail" src="{{ $image->image->url('thumbnail') }}" alt="{{ $image->name() }}"/>
                            </a>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('additional_scripts')
    <script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript">
        $('#image-grid').sortable({
            update: function () {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                var data = $(this).sortable('toArray');

                $.ajax({
                    url: '{{ action('AlbumsController@sort', $album->id) }}',
                    type: 'POST',
                    data: {
                        order: data,
                        _token: CSRF_TOKEN
                    },
                    dataType: 'JSON',
                    success: function (msg) {
                        $('#album-thumbnail').attr({
                            // TODO: needs optimization, low priority
                            src: $('#image-grid:first').children().first().children().children().attr('src')
                        });
                    }
                });
            }
        });
    </script>
@endsection