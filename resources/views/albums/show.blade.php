@extends('app')

@section('content')
    @if (!Auth::guest())
        @if (Auth::user()->can('update.albums'))
             <a href="{{ action('AlbumsController@upload', $album->id) }}" class="btn btn-primary btn-sm">
                <span class="glyphicon glyphicon-camera"></span> Загрузить изображения
             </a>
             <a href="{{ action('AlbumsController@edit', $album->id) }}" class="btn btn-default btn-sm">
                 <span class="glyphicon glyphicon-pencil"></span> Редактировать альбом
             </a>
             {!! Form::open([
                'url' => action('AlbumsController@destroy', $album->id),
                'method' => 'DELETE',
                'style' => 'display: inline;']) !!}

                <button type="submit" class="btn btn-danger btn-sm">
                    <span class="glyphicon glyphicon-remove"></span> Удалить альбом
                </button>
             {!! Form::close() !!}
             <br/> <br/>
         @endif
    @endif
    <div class="panel panel-default">
        <div class="panel-body">
            @foreach($images as $image) 
                <div class="col-xs-3" style="padding-bottom: 10px;">
                    <a href="{{ $image->image->url('original') }}"
                        data-lightbox="{{ $image->album->id }}"
                        data-title="{{ $image->name() }}">

                        <img class="img-thumbnail" src="{{ $image->image->url('thumbnail') }}" alt="{{ $image->name() }}"/>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('additional_styles')
<link rel="stylesheet" href="{{ asset('/css/lightbox.css') }}"/>
@endsection

@section('additional_scripts')
<script type="text/javascript" src="{{ asset('/js/lightbox.min.js') }}"></script>
@endsection