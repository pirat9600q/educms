@extends('app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Загрузка изображений
        </div>

        {!! Form::open([
            'url' => action('AlbumsController@store'),
            'files' => 'true',
        ]) !!}

            {!! Form::file('image', ['style' => 'display: none']) !!}
            <div id="upload" class="dropzone">

            </div>
        {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('additional_styles')
<link rel="stylesheet" href="{{ asset('/css/dropzone.css') }}"/>
@endsection

@section('additional_scripts')
<script src="{{ asset('/js/dropzone.js') }}"></script>
<script type="text/javascript">
    Dropzone.options.upload = {
        url: '{{ action('AlbumsController@upload', $album->id) }}',
        paramName: 'image',
        addRemoveLinks: 'true',
        acceptedFiles: 'image/*',
        dictDefaultMessage: 'Нажмите на эту область, чтобы выбрать файлы. <br/>' +
         'Также, Вы можете выделить файлы и перетащить их в эту область для загрузки.',
        sending: function(file, xhr, formData) {
            formData.append("_token", $('[name=_token]').val());
        }
    };
</script>
@endsection