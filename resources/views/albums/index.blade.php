@extends('app')

@section('content')
    @if (!Auth::guest())
        @if (Auth::user()->can('create.albums'))
             <a href="{{ action('AlbumsController@create') }}" class="btn btn-success btn-sm">
                <span class="glyphicon glyphicon-plus"></span> {{ _t('buttons.add', ['object' => _t('labels.album')]) }}
             </a> <br/> <br/>
         @endif
    @endif
    @foreach($albums as $album)
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-xs-4">
                            <a href="{{ action('AlbumsController@show', $album->id) }}">
                                @if ($album->images->count() > 0)
                                    <img src="{{ asset($album->images()->orderBy('order')->first()->image->url('thumbnail')) }}" alt="" class="img-thumbnail"/>
                                @else
                                    <img src="{{ $album->thumbnail->url('thumbnail') }}" alt="" class="img-thumbnail"/>
                                @endif
                            </a>
                        </div>
                        <div class="col-xs-8">
                            <a href="{{ action('AlbumsController@show', $album->id) }}">
                                <h4>{{ $album->name() }}</h4>
                            </a>
                            {{ $album->description_ru }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

@endsection