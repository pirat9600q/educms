@extends('app')

@section('content')
    @include('errors')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ _t('labels.creating') . " " . _t('labels.of_album') }}
        </div>

        <div class="panel-body">
            {!! Form::open(['url' => action('AlbumsController@store')]) !!}
            <p>
                Русская версия
            </p>
            <div class="form-group">
                {!! Form::label('name_ru', 'Название') !!}
                {!! Form::text('name_ru', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('description_ru', 'Описание') !!}
                {!! Form::textarea('description_ru', null, ['class' => 'form-control', 'rows' => '3']) !!}
            </div>

            <hr>
            <p>
                Английская версия
            </p>

            <div class="form-group">
                {!! Form::label('name_en', 'Название') !!}
                {!! Form::text('name_en', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('description_en', 'Описание') !!}
                {!! Form::textarea('description_en', null, ['class' => 'form-control', 'rows' => '3']) !!}
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Отправить</button>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

@endsection
