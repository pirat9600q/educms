<ul class="dropdown-menu main-menu">
<li><a href="{{ action('HomeController@index') }}">Главная</a></li>
<li><a href="{{ action('NewsController@index') }}">Новости</a></li>
<li><a href="{{ action('EmployeesController@index') }}">Сотрудники кафедры</a></li>
<li class="dropdown-submenu">
<a href=""><i class="glyphicon glyphicon-chevron-right icon-arrow-right"></i> Студентам </a>
<ul class="dropdown-menu main-menu-submenu">
<li><a href="{{ action('StudyplansController@index') }}">Учебный план</a></li>
<li><a href="{{ action('DisciplinesController@index') }}">Список дисциплин</a></li>
<li><a href="{{ action('SubjectsController@index') }}">Темы дипломов</a></li>
</ul>
</li>
<li class="dropdown-submenu">
<a href=""><i class="glyphicon glyphicon-chevron-right icon-arrow-right"></i> Для абитуриентов </a>
<ul class="dropdown-menu main-menu-submenu">
<li><a href="{{ url('program') }}">Программа обучения</a></li>
<li><a href="{{ url('terms') }}">Условия поступления</a></li>
<li><a href="{{ url('science') }}">Научная работа</a></li>
<li><a href="{{ url('common') }}">Общая информация</a></li>
</ul>
</li>
<li><a href="{{ action('PublicationsController@index') }}">Публикации</a></li>
<li><a href="{{ action('AlbumsController@index') }}">Галерея</a></li>
<li><a href="{{ url('path') }}">Схема проезда</a></li>
</ul>