@extends('app')

@section('content')
    @if (Auth::user() && Auth::user()->can('create.disciplines'))
        <a href="{{ action('DisciplinesController@create') }}" class="btn btn-success btn-sm">
            <span class="glyphicon glyphicon-plus"></span> Добавить дисциплину
        </a>
        <br/> <br/>
    @endif
    <div class="panel panel-default">
        <div class="panel-body">
            @foreach($disciplines as $d)
                <p>
                    <a href="{{ action('DisciplinesController@show', $d->id) }}">{{ $d->name }}</a>
                    @if($d->abbreviation != '')
                        ({{ $d->abbreviation }})
                    @endif
                </p>
            @endforeach
        </div>
    </div>
@endsection

@section('additional_styles')

@endsection

@section('additional_scripts')

@endsection