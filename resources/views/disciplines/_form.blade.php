{!! Form::open([
    'url' => $action,
    'method' => $method,
    'class' => 'form-horizontal',
    'id' => 'submit-form',
]) !!}

<div class="form-group">
    {!! Form::label('name', 'Наименование', ['class' => 'control-label col-xs-3']) !!}
    <div class="col-xs-7">
        {!! Form::text('name', $discipline ? $discipline->name : null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('abbreviation', 'Аббревиатура', ['class' => 'control-label col-xs-3']) !!}
    <div class="col-xs-7">
        {!! Form::text('abbreviation', $discipline ? $discipline->abbreviation : null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('description', 'Описание', ['class' => 'control-label col-xs-3']) !!}
    <div class="col-xs-7">
        {!! Form::textarea('description', $discipline ? $discipline->description : null, ['class' => 'form-control', 'rows' => 3]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('employees', 'Преподаватели', ['class' => 'control-label col-xs-3']) !!}
    <div class="col-xs-7">
        @if($discipline)
            @foreach($discipline->employees as $emp)
                <input value="{{ $emp->id }}" type="hidden" name="employees[]" class="employee-data employee-{{ $emp->id }}"/>
                <p class="employee-{{ $emp->id }}">
                    <a class="employee-{{ $emp->id }}" onclick="$('.employee-{{ $emp->id }}').remove();" style="cursor: pointer;">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                    {{ $emp->fullName() }}
                </p>
            @endforeach
        @endif
        <div id="remote" class="form-group">
            <input class="form-control typeahead" type="text" placeholder="Поиск..."/>
        </div>
    </div>
</div>

<div class="col-xs-offset-3 col-xs-7">
    {!! Form::submit('Отправить', ['class' => 'btn btn-success']) !!}
</div>
{!! Form::close() !!}

@section('additional_scripts')
    <script type="text/javascript" src="{{ asset('/js/typeahead.bundle.min.js') }}"></script>
    <script>
        var employees = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '{{ action('EmployeesController@search', '%QUERY') }}',
                wildcard: '%QUERY',
                beforeSend: function (jqXhr, data) {
                    $('.employee-data').each(function () {

                    });
                }
            }
        });

        $('#remote .typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 1,
            limit: 10
        }, {
            name: 'employees',
            display: function (employee) {
                return employee.surname + ' ' + employee.first_name + ' ' + employee.patronymic;
            },
            source: employees,
            templates: {
                suggestion: function (employee) {
                    return '<div class="tt-suggestion">' + employee.surname + ' ' + employee.first_name + ' '
                            + employee.patronymic + '</div>';
                }
            }
        }).on('typeahead:select', function (event, data) {
            var $btnRemove = $(document.createElement('a'))
                    .attr('class', 'employee-' + data.id)
                    .attr('onClick', '$(".employee-' + data.id + '").remove();')
                    .attr('style', 'cursor:pointer;')
                    .html('<span class="glyphicon glyphicon-remove"></span> ');

            $(document.createElement('input'))
                    .attr('value', data.id)
                    .attr('class', 'employee-data employee-' + data.id)
                    .attr('name', 'employees[]')
                    .attr('type', 'hidden')
            .insertBefore('#remote');

            $(document.createElement('p'))
                    .attr('class', 'employee-' + data.id)
                    .html(data.surname + ' ' + data.first_name + ' ' + data.patronymic)
                    .prepend($btnRemove)
            .insertBefore('#remote');

            $(this).typeahead('val', '');
        });
    </script>
@endsection