@extends('app')

@section('content')
    @include('errors')
    <div class="panel panel-default">
        <div class="panel-body">
            @include('disciplines._form', [
                'action' => action('DisciplinesController@store'),
                'method' => 'POST',
                'discipline' => null,
            ])
        </div>
    </div>
@endsection

@section('additional_styles')

@endsection

@section('additional_scripts')

@endsection