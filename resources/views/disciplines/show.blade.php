@extends('app')

@section('content')
    @if (Auth::user() && Auth::user()->can('update.disciplines'))
        <a href="{{ action('DisciplinesController@edit', $discipline->id) }}" class="btn btn-default">
            <span class="glyphicon glyphicon-edit"></span>
            Редактировать
        </a>
    @endif
    @if (Auth::user() && Auth::user()->can('delete.disciplines'))
        {!! Form::open(['url' => action('DisciplinesController@destroy', $discipline->id),
        'method' => 'DELETE', 'style' => 'display:inline']) !!}
        <a href="#" onclick="$(this).closest('form').submit();" class="btn btn-danger">
            <span class="glyphicon glyphicon-remove"></span>
            Удалить
        </a>
        {!! Form::close() !!}
        <br/><br/>
    @endif
    <div class="panel panel-default">
        <div class="panel-body">
            <p>
                <b>Наименование: </b>{{ $discipline->name }}
                @if($discipline->abbreviation != '')
                    ({{ $discipline->abbreviation }})
                @endif
            </p>
            @if($discipline->description != '')
                <p>
                    <b>Описание: </b>
                    {{ $discipline->description }}
                </p>
            @endif

            @if($discipline->employees()->count() > 0)
                <b>Преподаватели: </b> <br/>
                <ul>
                    @foreach($discipline->employees as $emp)
                        <li><a href="{{ action('EmployeesController@show', $emp->id) }}">{{ $emp->fullName() }}</a></li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
@endsection