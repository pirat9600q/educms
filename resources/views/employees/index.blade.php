@extends('app')

@section('content')

    @if (!Auth::guest())
        @if (Auth::user()->can('create.employees'))
        <div class="row">
            <div class="col-xs-12">
                <a href="{{ action('EmployeesController@create') }}" class="btn btn-success btn-sm">
                <span class="glyphicon glyphicon-plus"></span> Добавить
                </a> <br/> <br/>
            </div>
         </div>
         @endif
        @if (Auth::user()->can('update.employees'))
        <blockquote>
            Вы можете изменять порядок вывода преподавателей перетаскивая их мышкой
        </blockquote>
        @endif
    @endif

    <div class="row emp-grid" id="sortable-grid">
        @foreach($employees as $emp)
        <div class="col-xs-3 item ui-state-default" id="{{ $emp->id }}">
            <div class="emp">
                <a href="{{ action('EmployeesController@show', $emp->id) }}">
                    <img class="img-thumbnail" src="{{ $emp->photo->url('info') }}" alt="{{ $emp->fullName() }}"/>
                </a>
                <div class="emp-info">
                    {{ $emp->fullName() }}
                </div>
            </div>
        </div>
        @endforeach
    </div>

@endsection

@section('additional_scripts')
    @parent

    @if (!Auth::guest() && Auth::user()->can('update.employees'))
    <script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
    <script>
        $(function() {
            $('#sortable-grid').sortable({
                update: function() {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    var data = $(this).sortable('toArray');

                    $.ajax({
                        url: '{{ action('EmployeesController@sort') }}',
                        type: 'POST',
                        data: {
                            order: data,
                            _token: CSRF_TOKEN
                        },
                        dataType: 'text/json',
                        success: function () {
                            window.location.reload();
                        }
                    });
                }
            })
        })
    </script>
    @endif

@endsection
