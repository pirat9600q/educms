@extends('app')

@section('content')
    @include('errors')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-3">
                    <img class="img-thumbnail" src="{{ $emp->photo->url('info') }}" alt="{{ $emp->fullName() }}"/>
                    @if($emp->questions_enabled)
                        <a href="{{ action('QuestionsController@ask', $emp->id) }}" class="btn btn-primary" style="margin-top: 5px;margin-bottom: 5px; width: 100%" id="btnAsk">
                            Задать вопрос
                        </a>
                    @endif
                </div>
                <div class="col-xs-9">
                    <div id="infoPanel">
                        <h4>
                            <span id="infoSurname">{{ $emp->surname }}</span>
                            <span id="infoFirstName">{{ $emp->first_name }}</span>
                            <span id="infoPatronymic">{{ $emp->patronymic }}</span>
                            @if ($user)
                                @if($user->can("update.employee_{$emp->id}|update.employees"))
                                    <div class="pull-right">
                                        <a class="btn btn-default btn-xs" id="btnInfoEdit">
                                            <div class="glyphicon glyphicon-pencil"></div>
                                        </a>
                                        @if($user->can("delete.employee.{$emp->id}|delete.employees"))
                                            <form action="{{ action('EmployeesController@destroy', $emp->id) }}" method="POST" style="display: inline">
                                                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                                                <input type="hidden" name="_method" id="_method" value="DELETE"/>
                                                <a class="btn btn-default btn-xs" id="btnInfoDelete">
                                                    <div class="glyphicon glyphicon-remove"></div>
                                                </a>
                                            </form>
                                        @endif
                                    </div>
                                @endif
                            @endif
                        </h4>
                        @if ($emp->degree != '')
                         <div id="infoDegreePanel">
                             <b>Звание: </b><br/>
                             <div id="infoDegree">{{ $emp->degree }}</div>
                             <br/>
                        </div>
                        @endif

                        @if ($emp->post != '')
                        <div id="infoPostPanel">
                            <b>Должность: </b><br/>
                            <div id="infoPost">{{ $emp->post }}</div>
                            <br/>
                        </div>
                        @endif
                    </div>
                    @if($user)
                        @if($user->can("update.employee_{$emp->id}|update.employees"))
                            {!! Form::open([
                                'method' => 'PATCH',
                                'url' => action('EmployeesController@update', $emp->id),
                                'files' => true,
                                'class' => 'form-horizontal',
                                'id' => 'editInfoForm',
                                'style' => 'display: none'
                            ]) !!}
                                <div class="form-group">
                                    {!! Form::label('surname', 'Фамилия', ['class' => 'col-xs-3 col-md-2 control-label']) !!}
                                    <div class="col-xs-9 col-md-10">
                                        {!! Form::text('surname', $emp->surname, ['class' => 'form-control', 'id' => 'txtSurname']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('first_name', 'Имя', ['class' => 'col-xs-3 col-md-2 control-label']) !!}
                                    <div class="col-xs-9 col-md-10">
                                        {!! Form::text('first_name', $emp->first_name, ['class' => 'form-control', 'id' => 'txtFirstName']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('patronymic', 'Отчество', ['class' => 'col-xs-3 col-md-2 control-label']) !!}
                                    <div class="col-xs-9 col-md-10">
                                        {!! Form::text('patronymic', $emp->patronymic, ['class' => 'form-control', 'id' => 'txtPatronymic']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('post', 'Должность', ['class' => 'col-xs-3 col-md-2 control-label']) !!}
                                    <div class="col-xs-9 col-md-10">
                                    {!! Form::text('post', $emp->post, ['class' => 'form-control', 'id' => 'txtPost']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('degree', 'Звание', ['class' => 'col-xs-3 col-md-2 control-label']) !!}
                                    <div class="col-xs-9 col-md-10">
                                        {!! Form::text('degree', $emp->degree, ['class' => 'form-control', 'id' => 'txtDegree']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('questions_enabled', 'Вопросы', ['class' => 'col-xs-3 col-md-2 control-label']) !!}
                                    <div class="col-xs-9 col-md-10">
                                        {!! Form::checkbox('questions_enabled', 1, $emp->questions_enabled) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('photo', 'Фото', ['class' => 'col-xs-3 col-md-2 control-label']) !!}
                                    <div class="col-xs-9 col-md-10">
                                        {!! Form::file('photo', null, ['class' => 'form-control', 'id' => 'txtDegree']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-3 col-md-10">
                                        <a class="btn btn-default btn-success btn-sm" id="btnInfoSave">Сохранить</a>
                                        <a class="btn btn-default btn-sm" id="btnInfoCancel">Отмена</a>
                                    </div>
                                 </div>
                            {!! Form::close() !!}
                        @endif
                    @endif
                </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <div class="col-xs-12">
                    <div class="panel-group" id="infogroup" role="tablist" aria-multiselectable="true">
                        @if (($emp->pagetext != '') || ($emp->pagetext == '' && $user != NULL && $user->can("update.employee_{$emp->id}|update.employees")))
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="pagetextHeading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#infogroup" href="#pagetextCollapse" aria-expanded="false" aria-controls="pagetextCollapse">
                                            Научное направление
                                        </a>
                                        @if ($user)
                                            @if($user->can("update.employee_{$emp->id}|update.employees"))
                                                <div class="pull-right">
                                                    <a class="btn btn-default btn-xs" id="btnEditPagetext">
                                                        {{ _t('buttons.edit', ['object' => '']) }}
                                                    </a>
                                                </div>
                                            @endif
                                        @endif
                                    </h4>
                                </div>
                                <div id="pagetextCollapse" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="pagetextHeading">
                                     <div class="panel-body" id="divPagetext">{{ $emp->pagetext }}</div>
                                </div>
                            </div>
                        @endif

                        @if ($emp->disciplines->count() > 0)
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="disciplinesHeading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" class="collapsed" data-parent="#infogroup" href="#disciplinesCollapse" aria-expanded="false" aria-controls="disciplinesCollapse">
                                            Преподаваемые дисциплины
                                        </a>
                                    </h4>
                                </div>
                                <div id="disciplinesCollapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="disciplinesHeading">
                                    <div class="panel-body">
                                        <ul>
                                        @foreach($emp->disciplines()->orderBy('name')->get() as $discipline)
                                            <li>
                                                <a href="{{ action('DisciplinesController@show', $discipline->id) }}">
                                                    {{ $discipline->name }}
                                                </a>
                                            </li>
                                        @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if ($emp->subjects->count() > 0)
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="subjectsHeading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" class="collapsed" data-parent="#infogroup" href="#subjectsCollapse" aria-expanded="false" aria-controls="subjectsCollapse">
                                            Темы дипломов
                                        </a>
                                    </h4>
                                </div>
                                <div id="subjectsCollapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="subjectsHeading">
                                    <div class="panel-body">
                                        <ul>
                                        @foreach($emp->subjects()->orderBy('year')->get() as $subject)
                                            <li>
                                                {{ $subject->name }}
                                            </li>
                                        @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if ($emp->publications->count() > 0)
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="publicationsHeading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" class="collapsed" data-parent="#infogroup" href="#publicationsCollapse" aria-expanded="false" aria-controls="publicationsCollapse">
                                            Публикации
                                        </a>
                                    </h4>
                                </div>
                                <div id="publicationsCollapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="publicationsHeading">
                                     <div class="panel-body">
                                        <ul>
                                        @foreach(array_slice($emp->publications->toArray(), 0, 10) as $p)
                                            <li><a href="{{ action('PublicationsController@show', $p['id']) }}">{{ $p['title'] }}</a></li>
                                        @endforeach
                                        </ul>

                                        @if($thereAreMorePublications)
                                            <p>
                                                <a href="{{ action('EmployeesController@showPublications', $emp->id) }}">
                                                    Показать все {{ $emp->publications()->count() }} публикаций...
                                                </a>
                                            </p>
                                        @endif
                                     </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@if ($user)
    @if($user->can("update.employee_{$emp->id}|update.employees"))

        @section('additional_scripts')
            <script type="text/javascript">
                $('#btnEditPagetext').click(function () {
                    var $divPagetext = $('#divPagetext');
                    var $btnPagetext = $('#btnEditPagetext');
                    var $panelBody = $divPagetext.parent();
                    var $buttonContainer = $btnPagetext.parent();
                    var text = $divPagetext.text();

                    $divPagetext.hide();
                    $btnPagetext.hide();

                    var $txtPagetext = $(document.createElement('textarea'));
                    var $btnPagetextSave = $(document.createElement('a'));
                    var $btnPagetextCancel = $(document.createElement('a'));

                    $txtPagetext.attr({
                        class: 'form-control',
                        rows: 4,
                        id: 'txtPagetext'
                    });
                    $txtPagetext.text(text);
                    $txtPagetext.appendTo($panelBody);

                    $btnPagetextCancel.attr({
                        class: $btnPagetext.attr('class'),
                        id: 'btnPagetextCancel',
                        style: 'margin-right:4px'
                    });
                    $btnPagetextCancel.html('Отмена');
                    $btnPagetextCancel.click(function () {
                        $txtPagetext.remove();
                        $btnPagetextSave.remove();
                        $btnPagetextCancel.remove();

                        $divPagetext.show();
                        $btnPagetext.show();
                    });
                    $btnPagetextCancel.appendTo($buttonContainer);

                    $btnPagetextSave.attr({
                        class: $btnPagetext.attr('class'),
                        id: 'btnPagetextSave'
                    });
                    $btnPagetextSave.html('Сохранить');
                    $btnPagetextSave.click(function () {
                        var modifiedText = $txtPagetext.val();
                        $.ajax({
                            method: 'POST',
                            url: '{{ action('EmployeesController@update', $emp->id) }}',
                            data: {
                                _token: "{{ csrf_token() }}",
                                _method: "PATCH",
                                id: '{{ $emp->id }}',
                                pagetext: modifiedText
                            }
                        }).done(function(response) { console.log('done') })
                          .error(function(data) {
                              console.log('server errors',data);
                          });

                        $txtPagetext.remove();
                        $btnPagetextSave.remove();
                        $btnPagetextCancel.remove();

                        $divPagetext.text(modifiedText);
                        $divPagetext.show();
                        $btnPagetext.show();
                    });
                    $btnPagetextSave.appendTo($buttonContainer);
                });

                var $btnInfoEdit = $('#btnInfoEdit');

                var $infoPanel = $('#infoPanel');
                var $editInfoForm = $('#editInfoForm');

                var $btnInfoSave = $('#btnInfoSave');
                var $btnInfoCancel = $('#btnInfoCancel');

                $btnInfoEdit.click(function () {
                    $infoPanel.hide();
                    $editInfoForm.show();
                });

                $btnInfoCancel.click(function () { $editInfoForm.hide(); $infoPanel.show(); });
                $btnInfoSave.click(function () { $editInfoForm.submit() });

                $('#btnInfoDelete').click(function () {
                    $form = $(this).parent();
                    if (confirm('Удалить страницу?')) {
                        $form.submit();
                        return false;
                    }
                });
            </script>
        @endsection

    @endif
@endif