@extends('app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
        @foreach($publications as $p)
            <p><a href="{{ action('PublicationsController@show', $p->id) }}">{{ $p->title }}</a></p>
        @endforeach
        {!! $publications->render() !!}
        </div>

    </div>


@endsection