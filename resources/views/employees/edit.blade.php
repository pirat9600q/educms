@extends('app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">{{ Lang::get('labels.editing') . " " . Lang::get('labels.of_employee') }}</div>
        </div>
        <div class="panel-body">
            @include('employees._form', [
                'method'  => 'PATCH',
                'action'  => url('news/' . $employee->id),
                'article' => $employee,
            ])
        </div>
    </div>
@endsection