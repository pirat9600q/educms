@extends('app')

@section('content')
    @include('errors')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">{{ "Добавление информации о преподавателе" }}</div>
        </div>
        <div class="panel-body">
            {!! Form::open(['url' => action('EmployeesController@store'), 'class' => 'form-horizontal', 'files' => true]) !!}
                <div class="form-group">
                    {!! Form::label('surname', 'Фамилия', ['class' => 'col-xs-2 control-label']) !!}
                    <div class="col-xs-10">
                        {!! Form::text('surname', null, ['class' => 'form-control', 'id' => 'txtSurname']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('first_name', 'Имя', ['class' => 'col-xs-2 control-label']) !!}
                    <div class="col-xs-10">
                        {!! Form::text('first_name', null, ['class' => 'form-control', 'id' => 'txtFirstName']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('patronymic', 'Отчество', ['class' => 'col-xs-2 control-label']) !!}
                    <div class="col-xs-10">
                        {!! Form::text('patronymic', null, ['class' => 'form-control', 'id' => 'txtPatronymic']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('post', 'Должность', ['class' => 'col-xs-2 control-label']) !!}
                    <div class="col-xs-10">
                    {!! Form::text('post', null, ['class' => 'form-control', 'id' => 'txtPost']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('degree', 'Звание', ['class' => 'col-xs-2 control-label']) !!}
                    <div class="col-xs-10">
                        {!! Form::text('degree', null, ['class' => 'form-control', 'id' => 'txtDegree']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('pagetext', 'Научная деятельность', ['class' => 'col-xs-2 control-label']) !!}
                    <div class="col-xs-10">
                        {!! Form::textarea('pagetext', null, ['class' => 'form-control', 'id' => 'txtDegree', 'rows' => 4]) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('photo', 'Фотография', ['class' => 'col-xs-2 control-label']) !!}
                    <div class="col-xs-10">
                        {!! Form::file('photo', null, ['class' => 'form-control', 'id' => 'filePhoto']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Отправить</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection