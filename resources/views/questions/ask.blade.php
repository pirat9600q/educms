@extends('app')

@section('content')
@include('errors')
<div class="panel panel-default">
    <div class="panel-body">
        {!! Form::open(['url' => action('QuestionsController@store', $emp->id), 'method' => 'POST']) !!}
            @if (!Auth::user())
                <div class="form-group">
                    {!! Form::label('submitter_email', 'Email', ['class' => 'control-label']) !!}
                    {!! Form::text('submitter_email', old('submitter_email'), ['class' => 'form-control']) !!}
                </div>
            @endif

            <div class="form-group">
                {!! Form::label('text', 'Вопрос', ['class' => 'control-label']) !!}
                {!! Form::textarea('text', old('text'), ['class' => 'form-control', 'style' => 'resize: none', 'rows' => '3']) !!}
                <span id="helpBlock" class="help-block">
                    Максимальная длина вопроса 255 символов.
                </span>
            </div>

            <div class="text-center">
                {!! Form::submit('Отправить', ['class' => 'btn btn-success']) !!}
            </div>

        {!! Form::close() !!}

        <?php foreach ($questions as $q): ?>
            <?php if (!$q->answer && Auth::guest()) continue; ?>
                <hr/>
                <b>{{ $q->text }}</b>
                <br>
                <p>
                    @if (!$q->answer)
                        @if (Auth::user() && Auth::user()->can("update.employee.{$emp->id}|update.employees"))
                            <a href="{{ action('QuestionsController@answer', $q->id) }}">Ответить</a>
                        @endif
                    @else
                        {{ $q->answer }}
                    @endif
                    <span class="help-block small">
                        {{ $q->created_at }}
                        @if (Auth::user() && Auth::user()->is('admin'))
                            ({{ $q->submitter_email }} | {{ $q->submitter_ip_address }})
                        @endif
                        @if (Auth::user() && Auth::user()->can("update.employee.{$emp->id}|update.employees"))
                            <a href="{{ action('QuestionsController@destroy', $q->id) }}" class="glyphicon glyphicon-remove"></a>
                        @endif
                    </span>
                </p>
        <?php endforeach; ?>
        {!! $questions->render() !!}
    </div>
</div>
@endsection