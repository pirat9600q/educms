@extends('app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
        <span class="help-block">{{ $question->created_at }}</span>
            <b>{{ $question->text }}</b> <br/> <br/>
            {!! Form::open(['url' => action('QuestionsController@storeAnswer', $question->id
            ), 'method' => 'post']) !!}
                <div class="form-group">
                    {!! Form::textarea('answer', old('answer'), ['class' => 'form-control', 'rows' => 3]) !!}
                </div>

                <div class="text-center">
                    {!! Form::submit('Отправить', ['class' => 'btn btn-success']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection