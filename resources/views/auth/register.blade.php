@extends('app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">{{ _t('labels.registration') }}</div>
        <div class="panel-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form class="form-horizontal" role="form" method="POST" action="{{ action('Auth\AuthController@postRegister') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label class="col-md-4 control-label">{{ _t('labels.email') }}</label>
                    <div class="col-md-6">
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">{{ _t('labels.password') }}</label>
                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">{{ _t('labels.confirm') }}</label>
                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password_confirmation">
                    </div>
                </div>

                <hr/>

                <div class="form-group">
                    <label class="col-md-4 control-label">Фамилия</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">{{ _t('labels.name') }}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Отчетство</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="patronymic" value="{{ old('patronymic') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Группа</label>
                    <div class="col-md-6">
                        <select name="group">
                        @foreach(\App\Group::all() as $group)
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            {{ _t('labels.register') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
