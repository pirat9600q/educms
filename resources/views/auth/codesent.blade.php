@extends('app')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <p class="text-center">
            Заявка на регистрацию учетной записи отправлена и находится на премодерации. <br/>
            В ближайшее время на ваш почтовый ящик <b>{{ $user->email }}</b>
            будет выслано письмо с кодом подтверждения. <br/>
            <a href="{{ action('HomeController@index') }}">На главную</a>
        </p>
    </div>
</div>
@endsection