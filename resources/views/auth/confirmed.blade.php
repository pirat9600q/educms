@extends('app')

@section('content')
<div class="panel panel-default">
    <div class="panle-body">
        <p class="text-center">
            Ваша учетная запись была успешно активирована. <br/>
            <a href="{{ action('HomeController@index') }}">На главную</a>
        </p>
    </div>
</div>
@endsection