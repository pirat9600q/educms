@extends('emails.base')

@section('content')
    Здравствуйте, <b>{{ $name }}!</b> <br/>
    Ваша регистрация на кафедральном сайте была одобрена. <br/>
    Для завершения создания учетной записи пройдите по ссылке ниже.

    <br/> <br/>

    <a href="{{ action('Auth\AuthController@getVerify', $code) }}">Ссылка для подтверждения</a>
@endsection
