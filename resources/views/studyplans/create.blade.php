@extends('app')

@section('content')
    @include('errors')
    <div class="panel panel-default">
        <div class="panel-body">
            @include('studyplans._form', [
                'action' => action('StudyplansController@store'),
                'method' => 'POST',
                'plan'   => null
            ])
        </div>
    </div>
@endsection