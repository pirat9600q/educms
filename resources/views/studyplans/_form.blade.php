{!! Form::open([
    'url' => $action,
    'method' => $method,
    'class' => 'form-horizontal'
]) !!}

<div class="form-group">
    {!! Form::label('discipline_id', 'Дисциплина', ['class' => 'control-label col-xs-3']) !!}
    <div class="col-xs-9">
        {!! Form::select('discipline_id', $disciplines, $plan ? $plan->discipline_id : null) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('semester', 'Семестр', ['class' => 'control-label col-xs-3']) !!}
    <div class="col-xs-9">
        {!! Form::text('semester', $plan ? $plan->semester : 1, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('hours', 'Количество часов', ['class' => 'control-label col-xs-3']) !!}
    <div class="col-xs-9">
        {!! Form::text('hours', $plan ? $plan->hours : 0, ['class' => 'col-xs-8 form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('degree', 'Уровень', ['class' => 'control-label col-xs-3']) !!}
    <div class="col-xs-9">
        {!! Form::select('degree', ['bachelor' => 'Бакалавр', 'master' => 'Магистр']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('has_project', 'Курсовой проект', ['class' => 'control-label col-xs-3']) !!}
    <div class="col-xs-9">
        {!! Form::checkbox('has_project', 'true', $plan ? $plan->has_project : 0) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('has_exam', 'Экзамен', ['class' => 'control-label col-xs-3']) !!}
    <div class="col-xs-8">
        {!! Form::checkbox('has_exam', 'true', $plan ? $plan->has_exam : 0) !!}
    </div>
</div>

<div class="col-xs-offset-3 col-xs-9">
    {!! Form::submit('Отправить', ['class' => 'btn btn-success']) !!}
</div>
{!! Form::close() !!}