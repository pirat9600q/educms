@extends('app')

@section('content')
    @if (!Auth::guest())
        @if (Auth::user()->can('create.studyplan'))
             <a href="{{ action('StudyplansController@create') }}" class="btn btn-success btn-sm">
                <span class="glyphicon glyphicon-plus"></span> Добавить
             </a>
         @endif
        @if (Auth::user()->can('create.disciplines'))
            <a href="{{ action('DisciplinesController@create') }}" class="btn btn-info btn-sm">
                <span class="glyphicon glyphicon-plus"></span> Добавить дисциплину
            </a>
        @endif
        <br/> <br/>
    @endif
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="btn-group">
                        <a href="{{ action('StudyplansController@index',  ['degree' => 'bachelor']) }}"
                           class="btn btn-default {{ 'bachelor' != $selectedDegree ?: 'active' }}">
                            Бакалавр
                        </a>
                        <a href="{{ action('StudyplansController@index', ['degree' => 'master']) }}" class="btn btn-default {{ 'master' != $selectedDegree ?: 'active' }}">
                            Магистр
                        </a>
                    </div>
                    <div class="btn-group">
                        @foreach($semesters as $semester)
                            <a href="{{ action('StudyplansController@index', ['semester' => $semester]) }}"
                               class="btn btn-default {{ $semester != $selectedSemester ?: 'active' }}">
                                {{ $semester }}
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>

            @if($studyplan->count() > 0)
            <br/>

            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-bordered">
                        <tr>
                            <th>Наименование</th>
                            <th class="text-center">Курсовой проект</th>
                            <th class="text-center">Экзамен</th>
                            @if (Auth::user() && Auth::user()->can('update.studyplan'))
                                <th class="text-center">Действия</th>
                            @endif
                        </tr>
                        @foreach($studyplan as $item)
                            <tr>
                                <td><a href="{{ route('discipline.show', $item->discipline->id) }}">{{ $item->discipline->name }}</a></td>
                                <td class="text-center">
                                    @if ($item->has_project)
                                        <span class="glyphicon glyphicon-ok"></span>
                                    @else
                                        <span class="glyphicon glyphicon-minus"></span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if ($item->has_exam)
                                        <span class="glyphicon glyphicon-ok"></span>
                                    @else
                                        <span class="glyphicon glyphicon-minus"></span>
                                    @endif
                                </td>
                                @if (Auth::user() && Auth::user()->can('update.studyplan'))
                                    <td class="text-center">
                                        <a href="{{ action('StudyplansController@edit', $item->id) }}">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </a>
                                        {!! Form::open(['url' => action('StudyplansController@destroy', $item->id),
                                            'method' => 'DELETE', 'style' => 'display:inline']) !!}
                                            <a href="#" onclick="$(this).closest('form').submit();">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </a>
                                        {!! Form::close() !!}
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            @endif
        </div>
    </div>
@endsection