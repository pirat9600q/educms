@extends('app')

@section('content')
    @include('errors')
    <div class="panel panel-default">
        <div class="panel-body">
            @include('studyplans._form', [
                'action' => action('StudyplansController@update', $plan->id),
                'method' => 'PATCH',
                'plan'   => $plan,
            ])
        </div>
    </div>
@endsection

@section('additional_styles')

@endsection

@section('additional_scripts')

@endsection