@extends('master')

@section('main')
<div class="container">
    <div class="row">
        <div class="col-xs-3" style="text-align: center">
            <img src="{{ asset('/img/is_transparent.png') }}" alt="Кафедра Информационных систем" style="width: 100%; height: auto; max-height: 250px; max-width: 200px"/>
        </div>
        <div class="col-xs-9">
            <img src="{{ asset('/img/logo.png') }}" alt="Кафедра Информационных систем" style="width: 100%; height: auto; max-height: 250px;"/>
        </div>
    </div><br/>

    <div class="row">
        <div class="col-xs-3">
            @include('menu_' . user_lang_or_default())
        </div>
        <div class="col-xs-9">
            {!! Breadcrumbs::renderIfExists() !!}
            @yield('content')
        </div>
    </div>
</div>
@endsection