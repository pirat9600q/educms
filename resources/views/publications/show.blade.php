@extends('app')

@section('content')
    @if (Auth::user() && Auth::user()->can('update.publications'))
        <a href="{{ action('PublicationsController@edit', $publication->id) }}" class="btn btn-default">
            <span class="glyphicon glyphicon-edit"></span>
            Редактировать
        </a>
    @endif
    @if (Auth::user() && Auth::user()->can('delete.publications'))
        {!! Form::open(['url' => action('PublicationsController@destroy', $publication->id),
        'method' => 'DELETE', 'style' => 'display:inline']) !!}
        <a href="#" onclick="$(this).closest('form').submit();" class="btn btn-danger">
            <span class="glyphicon glyphicon-remove"></span>
            Удалить
        </a>
        {!! Form::close() !!}
        <br/><br/>
    @endif
    <h4>{{ $publication->title }}</h4>
    @if($publication->description != '')
        <b>Описание: </b> <br/>
        <p>{{ $publication->description }}</p>
    @endif
    <b>Авторы - сотрудники кафедры: </b> <br/>
    <ul>
    @foreach($publication->employees as $emp)
        <li><a href="{{ action('EmployeesController@show', $emp->id) }}">{{ $emp->fullName() }}</a></li>
    @endforeach
    </ul>
@endsection