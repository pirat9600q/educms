@extends('app')

@section('content')
    @include('errors')
    <div class="panel panel-default">
        <div class="panel-body">
            @include('publications._form', [
                'action' => action('PublicationsController@update', $publication->id),
                'method' => 'PATCH',
                'publication' => $publication,
            ])
        </div>
    </div>
@endsection

@section('additional_styles')

@endsection

@section('additional_scripts')

@endsection