@extends('app')

@section('content')
    @include('errors')
    <div class="panel panel-default">
        <div class="panel-body">
            @include('publications._form', [
                'action' => action('PublicationsController@store'),
                'method' => 'POST',
                'publication' => null,
            ])
        </div>
    </div>
@endsection

@section('additional_styles')

@endsection

@section('additional_scripts')

@endsection