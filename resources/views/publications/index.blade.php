@extends('app')

@section('content')
    @if (Auth::user() && Auth::user()->can('create.publications'))
        <a href="{{ action('PublicationsController@create') }}" class="btn btn-success btn-sm">
            <span class="glyphicon glyphicon-plus"></span> Добавить публикацию
        </a>
        <br/> <br/>
    @endif
    <div class="panel panel-default">
        <div class="panel-body">
        @foreach($publications as $p)
            <p><a href="{{ action('PublicationsController@show', $p->id) }}">{{ $p->title }}</a></p>
        @endforeach
        </div>

    </div>

    {!! $publications->render() !!}
@endsection