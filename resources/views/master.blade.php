<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ \App\Setting::getValue('global.title') }}</title>

    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}"/>
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    @yield('additional_styles')

    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>-->
    <!--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
    <!--[if lt IE 9]>
    <script src="{{ asset('/js/html5shiv.min.js') }}"></script>
    <script src="{{ asset('/js/respond.min.js') }}"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-main">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Навигация</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">{{ \App\Setting::getValue('global.departmentName') }}</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                {{--<li><a href="{{ url('/ru') }}">РУС</a></li>--}}
                {{--<li><a href="{{ url('/en') }}">ENG</a></li>--}}
                @if (Auth::guest())
                    <li><a href="{{ route('auth.login') }}">{{ _t('labels.login') }}</a></li>
                    <li><a href="{{ route('auth.register') }}">{{ _t('labels.registration') }}</a></li>
                @else
                    <li>
                        <a href="{{ action('MessagesController@inbox') }}">
                            Сообщения
                            <?php $unreadMessagesCount = Auth::user()->unreadMessages()->count(); ?>
                            @if($unreadMessagesCount > 0)
                                <span class="badge">{{ $unreadMessagesCount }}</span>
                            @endif
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name() }} <span class="caret"></span></a>
                        <ul class="dropdown-menu top-menu" role="menu">
                            @if(Auth::user()->is('admin'))
                                <li><a href="{{ action('Admin\DashboardController@index') }}">Панель администратора</a></li>
                            @endif
                            <li><a href="{{ route('auth.logout') }}">{{ _t('labels.logout') }}</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

@yield('main')

{{--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>--}}
{{--<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>--}}
<script src="{{ asset('/js/jquery.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/app.js') }}"></script>
@yield('additional_scripts')

<footer class="footer">
    <div class="container">
        <p id="footer-text">&copy; 2015 Кафедра Информационных систем</p>
    </div>
</footer>
</body>
</html>
