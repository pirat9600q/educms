@extends('app')

@section('content')
    <div class="panel panel-default">
        @if(!Auth::guest() && Auth::user()->can('update.news'))
            <div class="panel-heading">
                <a class="btn btn-default" href="{{action('HomeController@edit')}}">Редактировать</a>
            </div>
        @endif
        <div class="panel-body">
            {!! $content !!}
        </div>
    </div>
@endsection
