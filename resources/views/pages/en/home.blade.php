@extends('app')@section('content')<div class="panel panel-default"><div class="panel-body"><p>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; He is currently recognized that science - the science and technology of the XXI century. In our university computer science most fully represented in the department of information systems, which prepares bachelors and master&#39;s degree &quot;Information control systems and technologies.&quot; Our graduates - specialists in the use of computers and other means information technology in various fields of human activity, ranging from small businesses and ending research institutions, major banks, industrial production of all types of ownership, government agencies. They develop, build and operate computerized information systems for the collection, storage, processing and transmission of information.<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; The central element of such systems is the data warehouse. Therefore, the main task of the developer of the information system is structuring of information and knowledge about the subject area and their representation in a computer-interpretable form. Based on data and knowledge stored in memory, information systems perform their necessary treatment, make inferences, provide information to the user in the form that it takes.<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; If the motto of &quot;Copier&quot; was a statement, &quot;We document the whole world&quot;, the slogan of a specialist in the field of information systems is the statement, &quot;We represent the world in electronic form.&quot;<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; For this educational - professional program in the direction of &quot;6.050101 - Computer sciences&quot; include the study of the following fundamental and professionally - oriented disciplines:</p>

<ul>
	<li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Discrete Mathematics <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Computer Graphics <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Numerical methods <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Information Security Technology <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Information Theory <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Mathematical Methods of Operations Research <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>The theory of algorithms <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>The theory of decision making <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>algorithmic and programming <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Object-oriented programming <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Web-technology and Web-design <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Operating Systems <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>System Programming <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Technology program creating mnyh-products <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>The organization of data and knowledge bases <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>The methods and artificial intelligence systems <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Data Mining <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Cross-platform programming <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Technical means of information systems <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Platform Java <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Platform .Net <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Basics of Designing the user interface <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Database technology <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>The Basics of digital signal and image processing <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>1C <!-- li--> &nbsp;&nbsp;&nbsp;&nbsp;</li>
	<li>Additional sections of computer graphics, etc. <!-- li--> <!-- ul--><br />
	&nbsp; &nbsp; The department has full-time and part-time department, the opportunity to get a second degree, Graduate of the department for many years actively IP &nbsp; collaborates with the Federal Technical University of Zurich. Results of cooperation provided on the site laboratories intellectual &nbsp; digital signal processing department. C 2007 The Department conducts Ukrainian scientific-practical conference of young scientists and students&#39; information processes and technologies. &quot; <!-- div--></li>
</ul>
</div></div>@endsection