<ul class="dropdown-menu main-menu">
<li><a href="{{ action('HomeController@index') }}">Home</a></li>
<li><a href="{{ action('NewsController@index') }}">News</a></li>
<li><a href="{{ action('EmployeesController@index') }}">Employees</a></li>
<li class="dropdown-submenu">
<a href=""><i class="glyphicon glyphicon-chevron-right icon-arrow-right"></i> For students </a>
<ul class="dropdown-menu main-menu-submenu">
<li><a href="{{ action('StudyplansController@index') }}">Study plan</a></li>
<li><a href="{{ action('DisciplinesController@index') }}">Disciplines</a></li>
<li><a href="{{ action('SubjectsController@index') }}">Diplomas</a></li>
</ul>
</li>
<li class="dropdown-submenu">
<a href=""><i class="glyphicon glyphicon-chevron-right icon-arrow-right"></i> For applicants </a>
<ul class="dropdown-menu main-menu-submenu">
<li><a href="{{ url('program') }}">Study program</a></li>
<li><a href="{{ url('terms') }}">Admission</a></li>
<li><a href="{{ url('science') }}">Science</a></li>
<li><a href="{{ url('common') }}">Common info</a></li>
</ul>
</li>
<li><a href="{{ action('PublicationsController@index') }}">Publications</a></li>
<li><a href="{{ action('AlbumsController@index') }}">Gallery</a></li>
<li><a href="{{ url('path') }}">Map</a></li>
</ul>