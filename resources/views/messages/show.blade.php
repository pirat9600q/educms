@extends('messages.mailbox')

@section('content')
    {!! Form::open(['class' => 'form-horizontal']) !!}

    <div class="form-group-sm">
        @if(Auth::user()->id != $message->sender_id)
            {!! Form::label('receiver', 'От:', ['class' => 'control-label col-xs-1']) !!}
            <div class="col-xs-11">
                <p class="form-control-static">{{ $message->sender->fullName() }}</p>
            </div>
        @endif

        {!! Form::label('receiver', 'Кому:', ['class' => 'control-label col-xs-1']) !!}
        <div class="col-xs-11">
            <p class="form-control-static">
                {{ implode(', ', $message->receivers()->get()->map(function($u) {return $u->fullName(); })->toArray()) }}
            </p>
        </div>
    </div>

    <div class="form-group-sm">
        {!! Form::label('title', 'Тема:', ['class' => 'control-label col-xs-1']) !!}
        <div class="col-xs-11">
            <p class="form-control-static">{{ $message->title }}</p>
        </div>
    </div>

    <hr/>

    <div class="form-group-sm">
        <div class="col-xs-12">
            <p class="form-control-static">
                {{ $message->text }}
            </p>
        </div>
    </div>

    {!! Form::close() !!}
@endsection

@section('additional_styles')

@endsection

@section('additional_scripts')

@endsection