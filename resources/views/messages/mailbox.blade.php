@extends('master')

@section('main')
<div class="container">
    <div class="row">
        <div class="col-xs-3">
            <ul class="dropdown-menu main-menu">
                <li>
                    <a href="{{ action('MessagesController@inbox') }}">
                        <span class="glyphicon glyphicon-log-in"></span>&nbsp;
                        Входящие({{Auth::user()->incomingMessages()->count()}},
                        новых {{Auth::user()->unreadMessages()->count()}})
                    </a>
                </li>
                <li>
                    <a href="{{ action('MessagesController@outbox') }}">
                        <span class="glyphicon glyphicon-log-out"></span>&nbsp;
                        Отправленные({{Auth::user()->outcomingMessages()->count()}})
                    </a>
                </li>
                <li>
                    <a href="{{ action('MailingListsController@index') }}">
                        <span class="glyphicon glyphicon-log-out"></span>&nbsp;
                        Списки рассылки
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-xs-9">
            {{--{!! Breadcrumbs::renderIfExists() !!}--}}
            <div class="panel panel-default">
                <div class="panel-body">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
