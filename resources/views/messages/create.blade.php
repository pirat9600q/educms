@extends('messages.mailbox')

@section('content')
    @include('errors')
    {!! Form::open([
        'url' => action('MessagesController@send'),
        'method' => 'POST',
        'class' => 'form-horizontal'
    ]) !!}

    <div class="form-group">
        {!! Form::label('receiver', 'Кому:', ['class' => 'control-label col-xs-1']) !!}
        <div class="col-xs-11">
            {{--{!! Form::text('receiver_name', null, ['class' => 'form-control']) !!}--}}
            <div id="remote" class="form-group">
                <input class="form-control typeahead" type="text"/>
            </div>
            {!! Form::hidden('receiver', 0, ['id' => 'receiver']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('title', 'Тема:', ['class' => 'control-label col-xs-1']) !!}
        <div class="col-xs-11">
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12">
            {!! Form::textarea('text', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="text-center">
        {!! Form::submit('Отправить', ['class' => 'btn btn-success']) !!}
    </div>
    {!! Form::close() !!}
@endsection

@section('additional_scripts')
    <script type="text/javascript" src="{{ asset('/js/typeahead.bundle.min.js') }}"></script>
    <script>
        var users = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '{{ action('UsersController@search', '%QUERY') }}',
                wildcard: '%QUERY'
            }
        });

        $('#remote .typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 1,
            limit: 5
        }, {
            name: 'users',
            display: function (user) {
                return user.lastname + ' ' + user.firstname + ' ' + user.patronymic;
            },
            source: users,
            templates: {
                suggestion: function (user) {
                    return '<div class="tt-suggestion">' + user.lastname + ' ' + user.firstname + ' '
                            + user.patronymic + '</div>';
                }
            }
        }).on('typeahead:select', function (event, data) {
            $('#receiver').val(data.id);
        });
    </script>
@endsection
