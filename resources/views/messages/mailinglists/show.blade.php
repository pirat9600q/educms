@extends('messages.mailbox')

@section('content')

    {!! Form::open(['class' => 'form-horizontal']) !!}

        <a href="{{ action('MailingListsController@edit', ['id' => $mailingList->id]) }}" class="btn btn-default">
            <span class="glyphicon glyphicon-pencil"></span>
            Редактировать
        </a>

        <a href="{{ action('MailingListsController@composeMessage', ['id' => $mailingList->id]) }}" class="btn btn-default">
            <span class="glyphicon glyphicon-pencil"></span>
            Отправить сообщение
        </a>

        <div class="form-group-sm">
            {!! Form::label('name', 'Название:', ['class' => 'control-label col-xs-2']) !!}
            <div class="col-xs-10">
                <p class="form-control-static">{{ $mailingList->name }}</p>
            </div>
        </div>

        <div class="form-group-sm">
            {!! Form::label('receivers', 'Получатели:', ['class' => 'control-label col-xs-2']) !!}
            {{--@foreach($mailingList->receivers as $receiver)--}}
                {{--<div class="col-xs-10">--}}
                    {{--<p class="form-control-static">--}}
                        {{--{{ $receiver->fullName() }}--}}
                    {{--</p>--}}
                {{--</div>--}}
            {{--@endforeach--}}

            <div class="col-xs-10">
                <p class="form-control-static">
                    {{ implode(', ', $mailingList->receivers->map(function($u) { return $u->fullName(); })->toArray()) }}
                </p>
            </div>


        </div>

    {!! Form::close() !!}

@endsection
