{!! Form::open([
'url' => $action,
'method' => $method,
'class' => 'form-horizontal',
'id' => 'submit-form',
]) !!}

<div class="form-group">
    {!! Form::label('name', 'Название', ['class' => 'control-label col-xs-3']) !!}
    <div class="col-xs-7">
        {!! Form::text('name', $mailingList ? $mailingList->name : null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('receivers', 'Получатели', ['class' => 'control-label col-xs-3']) !!}
    <div class="col-xs-7">
        @if($mailingList)
            @foreach($mailingList->receivers as $receiver)
                <input value="{{ $receiver->id }}" type="hidden" name="receivers[]" class="receiver-data receiver-{{ $receiver->id }}"/>
                <p class="receiver-{{ $receiver->id }}">
                    <a class="receiver-{{ $receiver->id }}" onclick="$('.receiver-{{ $receiver->id }}').remove();" style="cursor: pointer;">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                    {{ $receiver->fullName() }}
                </p>
            @endforeach
        @endif
        <div id="remote" class="form-group">
            <input class="form-control typeahead" type="text" placeholder="Поиск..."/>
        </div>
    </div>
</div>

<div class="col-xs-offset-3 col-xs-7">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-success']) !!}
</div>

{!! Form::close() !!}

@section('additional_scripts')
    <script type="text/javascript" src="{{ asset('/js/typeahead.bundle.min.js') }}"></script>
    <script>
        var receivers = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '{{ action('UsersController@search', '%QUERY') }}',
                wildcard: '%QUERY',
                beforeSend: function (jqXhr, data) {
                    $('.receiver-data').each(function () {

                    });
                }
            }
        });

        $('#remote .typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 1,
            limit: 10
        }, {
            name: 'receivers',
            display: function (receiver) {
                return receiver.lastname + ' ' + receiver.firstname + ' ' + receiver.patronymic;
            },
            source: receivers,
            templates: {
                suggestion: function (receiver) {
                    return '<div class="tt-suggestion">' + receiver.lastname + ' ' + receiver.firstname + ' '
                            + receiver.patronymic + '</div>';
                }
            }
        }).on('typeahead:select', function (event, data) {
            var $btnRemove = $(document.createElement('a'))
                    .attr('class', 'receiver-' + data.id)
                    .attr('onClick', '$(".receiver-' + data.id + '").remove();')
                    .attr('style', 'cursor:pointer;')
                    .html('<span class="glyphicon glyphicon-remove"></span> ');

            $(document.createElement('input'))
                    .attr('value', data.id)
                    .attr('class', 'receiver-data receiver-' + data.id)
                    .attr('name', 'receivers[]')
                    .attr('type', 'hidden')
                    .insertBefore('#remote');

            $(document.createElement('p'))
                    .attr('class', 'receiver-' + data.id)
                    .html(data.lastname + ' ' + data.firstname + ' ' + data.patronymic)
                    .prepend($btnRemove)
                    .insertBefore('#remote');

            $(this).typeahead('val', '');
        });
    </script>
@endsection
