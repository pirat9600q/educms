@extends('messages.mailbox')

@section('content')

    @include('errors')

    {!! Form::open([
        'url' => action('MailingListsController@sendMessage', ['id' => $mailingList->id]),
        'method' => 'POST',
        'class' => 'form-horizontal'
        ]) !!}

        <div class="form-group">
            {!! Form::label('mailingList', 'Список рассылки:', ['class' => 'control-label col-xs-3']) !!}
            <div class="col-xs-9">
                <div id="remote" class="form-group">
                    <p class="form-control-static">{{ $mailingList->name  }}</p>
                </div>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('title', 'Тема:', ['class' => 'control-label col-xs-3']) !!}
            <div class="col-xs-9">
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                {!! Form::textarea('text', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="text-center">
            {!! Form::submit('Отправить', ['class' => 'btn btn-success']) !!}
        </div>

    {!! Form::close() !!}

@endsection
