@extends('messages.mailbox')

@section('content')

    {!! Form::open([
    'url' => action('MailingListsController@destroy'),
    'method' => 'DELETE',
    'class' => 'form-horizontal'
    ]) !!}

    <a href="{{ action('MailingListsController@create') }}" class="btn btn-default">
        <span class="glyphicon glyphicon-pencil"></span>
        Создать
    </a>

    <button type="submit" class="btn btn-default pull-right disabled" id="btnDelete">
        <span class="glyphicon glyphicon-trash"></span>
        Удалить
    </button>

    <table class="table table-condensed table-hover">
        <thead>
        <tr>
            <th style="width: 10px;"></th>
            <th class="text-center">Название</th>
        </tr>
        </thead>
        <tbody>
        @foreach($mailingLists as $mailingList)
                <tr>
                    <td><input type="checkbox" name="mailingLists[]" value="{{ $mailingList->id }}"
                               class="delete-checkbox" /></td>
                    <td>
                        <a href="{{ action('MailingListsController@show', $mailingList->id) }}">{{ $mailingList->name }}</a>
                    </td>
                </tr>
                @endforeach
        </tbody>
    </table>

    {!! Form::close() !!}

@endsection

@section('additional_scripts')
    <script type="text/javascript">
        var checked = 0;
        var $btnDelete = $('#btnDelete');

        $('.delete-checkbox').click(function () {
            if ($(this)[0].checked) {
                ++checked;
            } else {
                --checked;
            }

            refreshButtonState();
        });

        function refreshButtonState() {
            if (checked > 0) {
                $btnDelete.removeClass('disabled');
            } else {
                $btnDelete.addClass('disabled');
            }
        }
    </script>
@endsection


@endsection
