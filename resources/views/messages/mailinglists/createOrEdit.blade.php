@extends('messages.mailbox')

@section('content')

    @include('errors')

    @include('messages.mailinglists._form')

@endsection
