@extends('messages.mailbox')

@section('content')
    {!! Form::open([
    'url' => action('MessagesController@destroyAll'),
    'method' => 'DELETE',
    'class' => 'form-horizontal'
    ]) !!}

    <a href="{{ action('MessagesController@create') }}" class="btn btn-default">
        <span class="glyphicon glyphicon-pencil"></span>
        Написать сообщение
    </a>
    {{--<a href="{{ action('MessagesController@destroyAll') }}" class="btn btn-default pull-right disabled" id="btnDelete">--}}
        {{--<span class="glyphicon glyphicon-trash"></span>--}}
        {{--Удалить--}}
    {{--</a>--}}
    {{--{!! Form::open(['url' => action('MessagesController@destroyAll'), 'method' => 'DELETE', 'class' => 'form-inline']) !!}--}}
        <button type="submit" class="btn btn-default pull-right disabled" id="btnDelete">
            <span class="glyphicon glyphicon-trash"></span>
            Удалить
        </button>


    <table class="table table-condensed table-hover">
        <thead>
            <tr>
                <th style="width: 10px;"></th>
                <th class="text-center">Тема</th>
                <th class="col-md-3 text-center">Дата</th>
            </tr>
        </thead>
        <tbody>
        @foreach($messages as $msg)
            @if (isset($msg->opened) && !$msg->opened)
                <tr class="info">
            @else
                <tr>
            @endif

                <?php $message = $msg->message ?>
                <td><input type="checkbox" name="messages[]" value="{{ $message->id }}" class="delete-checkbox"/></td>
                <td><a href="{{ action('MessagesController@show', $message->id) }}">{{ $message->title }}</a></td>
                <td class="text-center">{{ $message->created_at }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! Form::close() !!}
@endsection

@section('additional_scripts')
    <script type="text/javascript">
        var checked = 0;
        var $btnDelete = $('#btnDelete');

        $('.delete-checkbox').click(function () {
            if ( $(this)[0].checked ) {
                ++checked;
            } else {
                --checked;
            }

            refreshButtonState();
        });

        function refreshButtonState() {
            if (checked > 0) {
                $btnDelete.removeClass('disabled');
            } else {
                $btnDelete.addClass('disabled');
            }
        }
    </script>
@endsection