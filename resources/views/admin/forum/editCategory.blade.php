@extends('admin.forum.base')

@section('panel_content')
    @include('admin.forum._form', ['method' => 'PATCH',
        'action' => action('Admin\ForumController@updateCategory', ['id' => $category->id])])
@endsection
