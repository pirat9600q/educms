@extends('admin.forum.base')

@section('panel_content')
    <h2>Создание категории</h2>
    @include('admin.forum._form', ['method' => 'POST', 'action' => action('Admin\ForumController@storeCategory')])
@endsection
