{!! Form::open(['url' => $action, 'method' => $method, 'class' => 'form-horizontal']) !!}
{!! Form::hidden('_method', $method) !!}
{!! Form::hidden('weight', $category ? $category->weight : $weight) !!}
@include('errors')
<div class="form-group">
    {!! Form::label('title', 'Заголовок', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('title', $category ? $category->title : old('title'), ['class' => 'form-control'])
        !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('subtitle', 'Подзаголовок', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('subtitle', $category ? $category->subtitle : old('subtitle'), ['class' =>
        'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('parent_category', 'Родительская категория', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('parent_category', $topLevelCategories, $category ? $category->parent_category : $parent_category, ['class' =>
        'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            Отправить
        </button>
    </div>
</div>
{!! Form::close() !!}
