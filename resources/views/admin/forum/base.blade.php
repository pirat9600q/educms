@extends('admin.app')

@section('active_forum')
    active
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Категории
        </div>
        <div class="panel-body">
            @yield('panel_content')
        </div>
    </div>
@endsection
