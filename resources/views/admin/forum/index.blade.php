@extends('admin.forum.base')

@section('panel_content')
<table class="table table-hover table-condensed table-bordered" style="width:50%">
    <p>
        <a href="{{action('Admin\ForumController@createCategory')}}" class="btn btn-default">Создать категорию</a>
    </p>
    <thead>
        <th>Категория</th>
        <th>Действие</th>
    </thead>
    <tbody>
    @foreach($categories as $category)
        <tr>
            <td>
                @if($category->parent_category != null)
                    @foreach(range(1,4) as $i)
                        &nbsp;
                    @endforeach
                @endif
                {{$category->title}}
            </td>
            <td>
                @if($category->parent_category == null)
                <a href="{{ action('Admin\ForumController@createCategory', ['parent_category' => $category->id]) }}"
                   class="glyphicon glyphicon-plus"></a>
                @endif
                <a href="{{ action('Admin\ForumController@editCategory', ['id' => $category->id]) }}"
                        class="glyphicon glyphicon-pencil"></a>
                {!! Form::open(['url' => action('Admin\ForumController@destroyCategory', ['id' => $category->id]), 'method' => 'POST', 'style' => 'display:inline']) !!}
                    {!! Form::hidden('_method', 'DELETE') !!}
                    <a href=""
                       data-title="{{$category->title}}"
                       class="glyphicon glyphicon-minus remove-link"></a>
                {!! Form::close() !!}

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection

@section('additional_scripts')
    @parent

    <script>
        $('.remove-link').click(function(e) {
            e.preventDefault();
            if(confirm('Удалить категорию \'' + $(this).data('title') + '\' ?')) {
                $(this).parent().submit();
                console.log('confirm');
            }
        });
    </script>

@endsection
