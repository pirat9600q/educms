<div class="list-group">
    <a href="{{ url('/admin/categories') }}" class="list-group-item @yield('active_categories')">
        <h4 class="list-group-item-heading"><span class="glyphicon glyphicon-list-alt"></span> Разделы</h4>
        <p class="list-group-item-text">Настройки разделов, формирование меню сайта</p>
    </a>
    <a href="{{ action('Admin\UsersController@index') }}" class="list-group-item @yield('active_users')">
        <h4 class="list-group-item-heading"><span class="glyphicon glyphicon-user"></span> Пользователи</h4>
        <p class="list-group-item-text">Управление пользователями и группами пользователей</p>
    </a>
    <a href="{{ action('Admin\SettingsController@index') }}" class="list-group-item @yield('active_settings')">
        <h4 class="list-group-item-heading"><span class="glyphicon glyphicon-cog"></span> Настройки</h4>
        <p class="list-group-item-text">Общие настройки, настройки оформления и безопасности</p>
    </a>
    <a href="{{ action('Admin\ForumController@index') }}" class="list-group-item @yield('active_forum')">
        <h4 class="list-group-item-heading"><span class="glyphicon glyphicon-th-list"></span> Форум</h4>
        <p class="list-group-item-text">Управление категориями форума</p>
    </a>
</div>
