@extends('app')

@section('content')
    {!! Form::open(['url' => action('HomeController@update'), 'method' => 'POST']) !!}
        {!! Form::hidden('_method', 'PATCH') !!}
        <ul class="nav nav-pills">
            <li role="presentation" class="active"><a id="russianVersion">Русская версия</a></li>
            <li role="presentation"><a id="englishVersion">Английская версия</a></li>
        </ul> <br/>
        <div class="form-group" id="contentRu">
            <div class="col-xs-12">
                <textarea class="form-control" name="content_ru" id="content_ru">
                    {!! $page ? $page->content_ru : old('content_ru') !!}
                </textarea>
            </div>
        </div>
        <div class="form-group" id="contentEn">
            <div class="col-xs-12">
                <textarea class="form-control" name="content_en" id="content_en">
                    {!! $page ? $page->content_en : old('content_en') !!}
                </textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-success">
                    {{ trans('labels.send') }}
                </button>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('additional_scripts')
    @parent
    <script type="text/javascript" src="{{ asset('/js/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript">
        CKEDITOR.replace('content_ru', {
            language: '{{ App::getLocale() }}'
        });
        CKEDITOR.replace('content_en', {
            language: '{{ App::getLocale() }}'
        });

        $('#titleEn').hide();
        $('#contentEn').hide();

        var version = "russian";
        $('#russianVersion').click(function() {
            if (version == "russian") return;
            toggleInputs();
            setSwitch('russian');
            version = 'russian';
        });

        $('#englishVersion').click(function() {
            if (version == "english") return;
            toggleInputs();
            setSwitch('english');
            version = 'english';
        });

        function toggleInputs() {
            $('#contentRu').toggle();
            $('#contentEn').toggle();
        }

        function setSwitch(newVersion) {
            $('#' + version + 'Version').parent().removeClass('active');
            $('#' + newVersion + 'Version').parent().addClass('active');
        }
    </script>
@endsection