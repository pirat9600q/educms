@extends('admin.app')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Настройки</div>
    <div class="panel-body">
    {!! Form::open([
        'url' => action('Admin\SettingsController@update'),
        'method' => 'PATCH',
        'class' => 'form-horizontal'
    ]) !!}
        {{-- Заголовок сайта --}}
        <div class="form-group">
            {!! Form::label('global.title', 'Заголовок сайта', $labelStyle) !!}
            <div class="{{ $inputDivStyle }}">
                {!! Form::text('global.title', $settings['global.title'], $inputStyle) !!}
            </div>
            <div class="col-lg-offset-2"></div>
        </div>

        {{-- Наименование кафедры --}}
        <div class="form-group">
            {!! Form::label('global.departmentName', 'Наименование кафедры', $labelStyle) !!}
            <div class="{{ $inputDivStyle }}">
                {!! Form::text('global.departmentName', $settings['global.departmentName'], $inputStyle) !!}
            </div>
            <div class="col-lg-offset-2"></div>
        </div>

        <hr/>

        {{-- Разрешить регистрацию --}}
        <div class="form-group">
            {!! Form::label('auth.allowRegistration', 'Разрешить регистрацию', $labelStyle) !!}
            <div class="{{ $inputDivStyle }}">
                {!! Form::checkbox('auth.allowRegistration', true, $settings['auth.allowRegistration'], ['data-size' => 'small']) !!}
            </div>
            <div class="col-lg-offset-2"></div>
        </div>

        {{-- Разрешить вход --}}
        <div class="form-group">
            {!! Form::label('auth.allowLogin', 'Разрешить вход', $labelStyle) !!}
            <div class="{{ $inputDivStyle }}">
                {!! Form::checkbox('auth.allowLogin', true, $settings['auth.allowLogin'], ['data-size' => 'small']) !!}
            </div>
            <div class="col-lg-offset-2"></div>
        </div>

        <hr/>

        <div class="col-lg-offset-3 col-md-offset-4 col-sm-offset-4 col-xs-offset-0 {{ $inputDivStyle }}">
            {!! Form::submit('Сохранить', ['class' => 'btn btn-success']) !!}
        </div>
    {!! Form::close() !!}
    </div>
</div>
@endsection

@section('active_settings')
    active
@endsection

@section('additional_styles')
<link rel="stylesheet" href="{{ asset('/css/bootstrap-switch.min.css') }}"/>
@endsection

@section('additional_scripts')
<script src="{{ asset('/js/bootstrap-switch.min.js') }}"></script>
<script>
    $('[type="checkbox"]').bootstrapSwitch();
</script>
@endsection