<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Панель администратора</title>

    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}"/>
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	@yield('additional_styles')

	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>-->
    <!--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
	<!--[if lt IE 9]>
        <script src="{{ asset('/js/html5shiv.min.js') }}"></script>
        <script src="{{ asset('/js/respond.min.js') }}"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Панель администратора</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

				<ul class="nav navbar-nav navbar-right">
				    <li><a href="{{ action('HomeController@index') }}">На главную</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name() }} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('auth.logout') }}">{{ _t('labels.logout') }}</a></li>
                        </ul>
                    </li>
				</ul>
			</div>
		</div>
	</nav>

    <div class="container-fluid">
        <div class="row">
            <div style="width: 300px;position:fixed;height:100%;">
                @include('admin.menu')
            </div>
            <div style="padding-left:302px;padding-right:2px;">
                @yield('content')
            </div>
        </div>

    </div>

	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	@yield('additional_scripts')
</body>
</html>
