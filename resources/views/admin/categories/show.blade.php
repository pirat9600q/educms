@extends('admin.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">{{ $category->title_ru }}</div>
        </div>

        <div class="panel-body"></div>

        <div class="panel-footer">
            {{ $category->created_at }}
        </div>
    </div>
@endsection

@section('active_categories')
    active
@endsection