@extends('admin.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Разделы</div>
        <div class="panel-body">
            <a href="{{ url("admin/categories/create/stub") }}" class="btn btn-success btn-sm">
                <span class="glyphicon glyphicon-bookmark"></span> Добавить категорию
            </a>
            <a href="{{ url("admin/categories/create/page") }}" class="btn btn-success btn-sm">
                <span class="glyphicon glyphicon-file"></span> Добавить страницу
            </a> <br/> <br/>

            <table class="table table-hover table-condensed table-bordered" style="width: 50%">
                <thead>
                    <tr>
                        <th style="text-align: center">Страница</th><th style="text-align: center">Действия</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>
                            @for ($i = 0; $i < $category->depth * 4; $i++)
                                &nbsp;
                            @endfor

                            @if ($category->type == 'stub')
                                <span class="glyphicon glyphicon-bookmark"></span>
                            @elseif($category->type == 'page')
                                <span class="glyphicon glyphicon-file"></span>
                            @else
                                <span class="glyphicon glyphicon-cog"></span>
                            @endif

                            {{ $category->title() }}
                        </td>
                        <td>
                            <a href="{{ url('/admin/categories/' . $category->id . '/up') }}" class="glyphicon glyphicon-chevron-up"></a>
                            <a href="{{ url('/admin/categories/' . $category->id . '/down') }}" class="glyphicon glyphicon-chevron-down"></a>

                            @if ($category->type == 'page')
                                <a href="{{ url("{$category->content}") }}" class="glyphicon glyphicon-eye-open" target="_blank"></a>
                            @elseif ($category->type == 'controller')
                                <a href="{{ action("{$category->content}") }}" class="glyphicon glyphicon-eye-open" target="_blank"></a>
                            @endif

                            <a href="{{ url('/admin/categories/' . $category->id . '/edit') }}" class="glyphicon glyphicon-pencil"></a>
                            @if (!($category->type == 'stub' && $category->children()->count() > 0) && $category->type != 'controller')
                                <form class="remove-form" action="{{ url('/admin/categories/' . $category->id) }}" method="POST" style="display: inline;">
                                    <input type="hidden" id="_method" name="_method" value="DELETE"/>
                                    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                                    <a class="glyphicon glyphicon-remove remove-link" href=""></a>
                                </form>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('additional_scripts')
    <script type="text/javascript">
        $('.remove-link').click(function() {
            var $form = $(this).parent();
            var title = $form.parent().parent().children().first().html().replace(/(&nbsp;)+/g, '').trim();
            if (confirm('Удалить страницу "' + title + '"?')) {
                $form.submit();
                return false;
            }
        });
    </script>
@endsection

@section('active_categories')
    active
@endsection