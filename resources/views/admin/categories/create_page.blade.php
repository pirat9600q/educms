@extends('admin.app')

@section('content')
    @include('errors')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">Создание страницы</div>
        </div>
        <div class="panel-body">
            <form action="{{ url('admin/categories') }}" accept-charset="UTF-8" class="form-horizontal" method="POST">
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="type" value="page"/>

                <ul class="nav nav-pills">
                  <li role="presentation" class="active"><a href="#" id="russianVersion">Русская версия</a></li>
                  <li role="presentation"><a href="#" id="englishVersion">Английская версия</a></li>
                </ul> <br/>
                <div class="form-group" id="titleRu">
                    <div class="col-xs-12">
                        <input type="text" class="form-control" name="title_ru" id="title_ru" placeholder="Заголовок" value="{{ old('title_ru') }}">
                    </div>
                </div>
                <div class="form-group" id="titleEn">
                    <div class="col-xs-12">
                        <input type="text" class="form-control" name="title_en" id="title_en" placeholder="Заголовок" value="{{ old('title_en') }}">
                    </div>
                </div>
                <div class="form-group" id="textRu">
                    <div class="col-xs-12">
                        <textarea class="form-control" name="text_ru" id="text_ru" value="{{ old('text_ru') }}"></textarea>
                    </div>
                </div>
                <div class="form-group" id="textEn">
                    <div class="col-xs-12">
                        <textarea class="form-control" name="text_en" id="text_en" value="{{ old('text_en') }}"></textarea>
                    </div>
                </div>
                <div class="form-group" id="parentId">
                    <label for="content" class="col-xs-12">Адрес (наименование файла)</label>
                    <div class="col-xs-12">
                        <input type="text" class="form-control" name="content" id="content" placeholder="" value="{{ old('content') }}">
                    </div>
                </div>
                <div class="form-group" id="parentId">
                    <label for="parent_id" class="col-xs-12">Расположить в </label>
                    <div class="col-xs-12">
                        <select name="parent_id" id="parent_id">
                            <option value="null">*Корень сайта</option>
                            @foreach($stubs as $stub)
                                <option value="{{ $stub->id }}">{{ $stub->title_ru }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-success">Отправить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('additional_scripts')
    <script type="text/javascript" src="{{ asset('/js/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript">
        CKEDITOR.replace('text_ru', {
            language: '{{ user_lang_or_default() }}'
        });
        CKEDITOR.replace('text_en', {
            language: '{{ user_lang_or_default() }}'
        });

        $('#titleEn').hide();
        $('#textEn').hide();

        var version = "russian";
        $('#russianVersion').click(function() {
            if (version == "russian") return;
            toggleInputs();
            setSwitch('russian');
            version = 'russian';
        });

        $('#englishVersion').click(function() {
            if (version == "english") return;
            toggleInputs();
            setSwitch('english');
            version = 'english';
        });

        function toggleInputs() {
            $('#titleRu').toggle();
            $('#titleEn').toggle();
            $('#textRu').toggle();
            $('#textEn').toggle();
        }

        function setSwitch(newVersion) {
            $('#' + version + 'Version').parent().removeClass('active');
            $('#' + newVersion + 'Version').parent().addClass('active');
        }
    </script>
@endsection

@section('active_categories')
    active
@endsection