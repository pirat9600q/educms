<form action="{{ $action }}" accept-charset="UTF-8" class="form-horizontal" method="POST">
    <input type="hidden" name="_method" value="{{ $method }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <ul class="nav nav-pills">
      <li role="presentation" class="active"><a href="#" id="russianVersion">Русская версия</a></li>
      <li role="presentation"><a href="#" id="englishVersion">Английская версия</a></li>
    </ul> <br/>
    <div class="form-group" id="titleRu">
        <div class="col-xs-12">
            <input type="text" class="form-control" name="title_ru" id="title_ru"
            value="{{ $category ? $category->title_ru : old('title_ru') }}" placeholder="Заголовок">
        </div>
    </div>
    <div class="form-group" id="titleEn">
        <div class="col-xs-12">
            <input type="text" class="form-control" name="title_en" id="title_en"
            value="{{ $category ? $category->title_en : old('title_en') }}" placeholder="Заголовок">
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success">
                Отправить
            </button>
        </div>
    </div>
</form>
