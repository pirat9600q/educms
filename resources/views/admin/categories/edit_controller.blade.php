@extends('admin.app')

@section('content')
    @include('errors')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">Создание раздела</div>
        </div>
        <div class="panel-body">
            <form action="{{ action('Admin\CategoriesController@update', $category->id) }}" accept-charset="UTF-8" class="form-horizontal" method="POST">
                <input type="hidden" name="_method" value="PATCH">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="type" value="controller"/>
                <input type="hidden" name="title_ru" value="{{ $category->title_ru }}"/>
                <input type="hidden" name="title_en" value="{{ $category->title_en }}"/>
                <input type="hidden" name="content" value="{{ $category->content }}"/>

                <div class="form-group">
                    <label class="col-xs-2 control-label">Контроллер: </label>
                    <div class="col-xs-10">
                        <p class="form-control-static">{{ $category->title_ru }}</p>
                    </div>
                </div>
                <div class="form-group" id="parentId">
                    <label class="col-xs-2 control-label">Расположить в: </label>
                    <div class="col-xs-10">
                        <select name="parent_id" id="parent_id">
                            <option value="null">*Корень сайта</option>
                            @foreach($stubs as $stub)
                                <option value="{{ $stub->id }}" {{ $stub->id == $category->parent_id ? 'selected' : '' }}>
                                    {{ $stub->title_ru }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-success">Отправить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('additional_scripts')
    <script type="text/javascript">
        $('#titleEn').hide();

        var version = "russian";
        $('#russianVersion').click(function() {
            if (version == "russian") return;
            toggleInputs();
            setSwitch('russian');
            version = 'russian';
        });

        $('#englishVersion').click(function() {
            if (version == "english") return;
            toggleInputs();
            setSwitch('english');
            version = 'english';
        });

        function toggleInputs() {
            $('#titleRu').toggle();
            $('#titleEn').toggle();
            $('#textRu').toggle();
            $('#textEn').toggle();
        }

        function setSwitch(newVersion) {
            $('#' + version + 'Version').parent().removeClass('active');
            $('#' + newVersion + 'Version').parent().addClass('active');
        }
    </script>
@endsection

@section('active_categories')
    active
@endsection