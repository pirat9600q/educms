@extends('admin.app')

@section('content')
    @include('errors')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">Создание раздела</div>
        </div>
        <div class="panel-body">
            <form action="{{ action('Admin\CategoriesController@update', $category->id) }}" accept-charset="UTF-8" class="form-horizontal" method="POST">
                <input type="hidden" name="_method" value="PATCH">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="type" value="stub"/>
                <input type="hidden" name="content" value="{{ $category->content }}"/>

                <ul class="nav nav-pills">
                  <li role="presentation" class="active"><a href="#" id="russianVersion">Русская версия</a></li>
                  <li role="presentation"><a href="#" id="englishVersion">Английская версия</a></li>
                </ul> <br/>
                <div class="form-group" id="titleRu">
                    <label class="col-xs-2 control-label">Заголовок: </label>
                    <div class="col-xs-10">
                        <input type="text" class="form-control" name="title_ru" id="title_ru" placeholder="Заголовок"
                             value="{{ $category->title_ru }}">
                    </div>
                </div>
                <div class="form-group" id="titleEn">
                    <label class="col-xs-2 control-label">Заголовок: </label>
                    <div class="col-xs-10">
                        <input type="text" class="form-control" name="title_en" id="title_en" placeholder="Заголовок"
                             value="{{ $category->title_en }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-2 control-label">Расположить в: </label>
                    <div class="col-xs-10">
                        <select name="parent_id" id="parent_id">
                            <option value="null">*Корень сайта</option>
                            @foreach($stubs as $stub)
                                @if($stub->id != $category->id)
                                <option value="{{ $stub->id }}" {{ $stub->id == $category->parent_id ? 'selected' : '' }}>
                                    {{ $stub->title_ru }}
                                </option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-success">Отправить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('additional_scripts')
    <script type="text/javascript">
        $('#titleEn').hide();

        var version = "russian";
        $('#russianVersion').click(function() {
            if (version == "russian") return;
            toggleInputs();
            setSwitch('russian');
            version = 'russian';
        });

        $('#englishVersion').click(function() {
            if (version == "english") return;
            toggleInputs();
            setSwitch('english');
            version = 'english';
        });

        function toggleInputs() {
            $('#titleRu').toggle();
            $('#titleEn').toggle();
            $('#textRu').toggle();
            $('#textEn').toggle();
        }

        function setSwitch(newVersion) {
            $('#' + version + 'Version').parent().removeClass('active');
            $('#' + newVersion + 'Version').parent().addClass('active');
        }
    </script>
@endsection

@section('active_categories')
    active
@endsection