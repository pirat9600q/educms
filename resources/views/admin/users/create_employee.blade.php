@extends('admin.app')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        @include('errors')
        @include('admin.users._form_employee', ['user' => null, 'action' => action('Admin\UsersController@storeEmployee'), 'method' => 'POST'])
    </div>
</div>
@endsection

@section('active_users') active @endsection