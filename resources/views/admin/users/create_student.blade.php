@extends('admin.app')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        @include('errors')
        @include('admin.users._form_student', ['user' => null, 'action' => action('Admin\UsersController@storeStudent'), 'method' => 'POST'])
    </div>
</div>
@endsection

@section('active_users') active @endsection