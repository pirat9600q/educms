@extends('admin.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Пользователь <b>{{ $user->name() }}</b></div>
        <div class="panel-body">
            <a href="{{ action('Admin\UsersController@edit', $user->id) }}" class="btn btn-success btn-sm">
                <span class="glyphicon glyphicon-pencil"></span>
                Редактировать
            </a>
            @if ($user->active)
            <a href="{{ action('Admin\UsersController@deactivate', $user->id) }}" class="btn btn-danger btn-sm">
                <span class="glyphicon glyphicon-remove-sign"></span>
                Деактивировать
            </a>
            @else
            <a href="{{ action('Admin\UsersController@activate', $user->id) }}"class="btn btn-info btn-sm">
                <span class="glyphicon glyphicon-ok-sign"></span>
                Активировать
            </a>
            @endif
            @if($user->id != Auth::user()->id)
            <a class="btn btn-default btn-sm btnDelete" href="{{ action('Admin\UsersController@destroy', $user->id) }}" name="{{ $user->fullName() }}" id="{{ $user->id }}" >
                <span class="glyphicon glyphicon-remove"></span>
                Удалить
            </a>
            @endif
            <form class="form-horizontal">
                <div class="form-group-sm">
                    <label class="col-xs-3 control-label">Фамилия: </label>
                    <div class="col-xs-9">
                        <p class="form-control-static">{{ $user->lastname }}</p>
                    </div>
                </div>

                <div class="form-group-sm">
                    <label class="col-xs-3 control-label">Имя: </label>
                    <div class="col-xs-9">
                        <p class="form-control-static">{{ $user->firstname }}</p>
                    </div>
                </div>

                @if ($user->patronymic != '')
                <div class="form-group-sm">
                    <label class="col-xs-3 control-label">Отчетство: </label>
                    <div class="col-xs-9">
                        <p class="form-control-static">{{ $user->patronymic }}</p>
                    </div>
                </div>
                @endif

                <div class="form-group-sm">
                    <label class="col-xs-3 control-label">Email: </label>
                    <div class="col-xs-9">
                        <p class="form-control-static">{{ $user->email }}</p>
                    </div>
                </div>

                <div class="form-group-sm">
                    <label class="col-xs-3 control-label">Тип учетной записи: </label>
                    <div class="col-xs-9">
                        <p class="form-control-static">
                            @if($user->is('student'))
                                Студент
                            @elseif($user->is('employee'))
                                Сотрудник кафедры
                            @endif

                            @if($user->is('admin'))
                                (Администратор)
                            @endif
                        </p>
                    </div>
                </div>

                <div class="form-group-sm">
                    <label class="col-xs-3 control-label">Статус: </label>
                    <div class="col-xs-9">
                        <p class="form-control-static">{{ $user->active ? 'Активен' : 'Не активен' }}</p>
                    </div>
                </div>

                @if(!$user->is('student'))
                <div class="form-group-sm">
                    <label class="col-xs-3 control-label">Права доступа: </label>
                    <div class="col-xs-9">
                        <p class="form-control-static">

                        @if ($user->can('update.news'))
                            Редактирование новостей <br/>
                        @endif

                        @if ($user->can('update.employees'))
                            Редактирование информации о преподавателях <br/>
                        @endif

                        @if ($user->can('update.studyplan'))
                            Редактирование учебного плана <br/>
                        @endif

                        @if ($user->can('update.publications'))
                            Редактирование публикаций <br/>
                        @endif

                        @if ($user->can('update.disciplines'))
                            Редактирование дисциплин <br/>
                        @endif

                        @if ($user->can('update.subjects'))
                            Редактирование тем дипломов <br/>
                        @endif

                        @if ($user->can('update.albums'))
                            Редактирование галереи <br/>
                        @endif

                        @if ($user->can('update.pages'))
                            Редактирование информационных страниц <br/>
                        @endif

                        <?php $employees = $user->employeesPages(); ?>
                        @if($employees)
                            @foreach($employees as $emp)
                                Страница преподавателя
                                <a href="{{ action('EmployeesController@show', $emp->id) }}">{{ $emp->fullName() }}</a>
                                <br/>
                            @endforeach
                        @endif
                        </p>
                    </div>
                </div>
                @endif
            </form>
        </div>
    </div>
@endsection

@section('active_users') active @endsection

@section('additional_scripts')
    <script type="text/javascript">
        $('.btnDelete').click(function (event) {
            if (! confirm('Удалить пользователя?')) {
                event.preventDefault();
                return false;
            }
        });
    </script>
@endsection