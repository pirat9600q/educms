{!! Form::checkbox('perm[]', 'news', $user ? $user->hasPermission('news') : false) !!}
Редактирование новостей <br/>

{!! Form::checkbox('perm[]', 'employees', $user ? $user->hasPermission('employees') : false) !!}
Редактирование информации о преподавателях <br/>

{!! Form::checkbox('perm[]', 'studyplan', $user ? $user->hasPermission('studyplan') : false) !!}
Редактирование учебного плана <br/>

{!! Form::checkbox('perm[]', 'publications', $user ? $user->hasPermission('publications') : false) !!}
Редактирование публикаций <br/>

{!! Form::checkbox('perm[]', 'disciplines', $user ? $user->hasPermission('disciplines') : false) !!}
Редактирование дисциплин <br/>

{!! Form::checkbox('perm[]', 'subjects', $user ? $user->hasPermission('subjects') : false) !!}
Редактирование тем дипломов <br/>

{!! Form::checkbox('perm[]', 'albums', $user ? $user->hasPermission('albums') : false) !!}
Редактирование галереи <br/>

<hr/>
    <select name="employee" id="employee">
        <option value="-1">*Отсутствует</option>
        @foreach(App\Employee::orderBy('surname')->get() as $emp)
            <option value="{{ $emp->id }}">{{ $emp->fullName() }}</option>
        @endforeach
    </select>
<hr/>
{!! Form::checkbox('admin', true, $user ? $user->is('admin') : false) !!}
Администратор <br/>

<br/>