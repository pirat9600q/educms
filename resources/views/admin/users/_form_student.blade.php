{!! Form::open(['url' => $action, 'method' => $method, 'class' => 'form-horizontal']) !!}
    <div class="form-group">
        {!! Form::label('email', 'Email', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('email', $user ? $user->email : old('email'), ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('password', 'Пароль', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>
    </div>

    <hr/>

    <div class="form-group">
        {!! Form::label('lastname', 'Фамилия', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('lastname', $user ? $user->lastname : old('lastname'), ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('firstname', 'Имя', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('firstname', $user ? $user->firstname : old('firstname'), ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('patronymic', 'Отчество', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('patronymic', $user ? $user->patronymic : old('patronymic'), ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('group', 'Группа', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('group', App\Group::all()->lists('name', 'id')) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                Отправить
            </button>
        </div>
    </div>
{!! Form::close() !!}