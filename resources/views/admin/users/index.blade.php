@extends('admin.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Пользователи и группы</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6">
                    <a class="btn btn-success btn-sm" style="margin: 2px;" href="{{ action('Admin\UsersController@createStudent') }}">
                        <span class="glyphicon glyphicon-plus"></span>
                        Зарегистрировать студента
                    </a>
                    <a class="btn btn-success btn-sm" style="margin: 2px;" href="{{ action('Admin\UsersController@createEmployee') }}">
                        <span class="glyphicon glyphicon-plus"></span>
                        Зарегистрировать сотрудника
                    </a>
                    <br/> <br/>
                </div>
                <div class="col-xs-6">
                    <div id="remote" class="form-group">
                        <input class="form-control typeahead" type="text" placeholder="Поиск..."/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Запросы на регистрацию</div>
                        <table class="table">
                            <tbody>
                            @foreach($users as $user)
                                @if(! $user->confirmed && $user->confirmation_code == null)
                                <tr>
                                    <td><a href="{{ action('Admin\UsersController@show', $user->id) }}">{{ $user->fullName() }}</a></td>
                                    <td>
                                        <a class="btn btn-success btn-xs pull-right" style="margin: 2px;" href="{{ action('Admin\UsersController@confirm', $user->id) }}">
                                           <span class="glyphicon glyphicon-ok-sign"></span>
                                            Подтвердить
                                        </a>
                                        <a class="btn btn-danger btn-xs pull-right btnDelete" id="{{ $user->id }}" name="{{ $user->fullname() }}" style="margin: 2px;" href="{{ action('Admin\UsersController@destroy', $user->id) }}">
                                           <span class="glyphicon glyphicon-remove-sign"></span>
                                            Отклонить
                                        </a>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Последние зарегистрированные</div>
                        <table class="table">
                            <tbody>
                            @foreach($users as $user)
                                @if($user->active && $user->confirmed)
                                <tr>
                                    <td><a href="{{ action('Admin\UsersController@show', $user->id) }}">{{ $user->fullName() }}</a></td>
                                    <td>
                                    @if($user->id != Auth::user()->id)
                                        @if($user->active)
                                            <a class="btn btn-danger btn-xs pull-right" href="{{ action('Admin\UsersController@deactivate', $user->id) }}">
                                                <span class="glyphicon glyphicon-remove-sign"></span>
                                                Деактивировать
                                            </a>
                                            <form action=""></form>
                                        @else
                                            <a class="btn btn-info btn-xs pull-right" href="{{ action('Admin\UsersController@activate', $user->id) }}">
                                                <span class="glyphicon glyphicon-ok-sign"></span>
                                                Активировать
                                            </a>
                                        @endif
                                    @endif
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('active_users') active @endsection

@section('additional_scripts')
    <script type="text/javascript" src="{{ asset('/js/typeahead.bundle.min.js') }}"></script>
    <script>
        var users = new Bloodhound({
              datumTokenizer: Bloodhound.tokenizers.whitespace,
              queryTokenizer: Bloodhound.tokenizers.whitespace,
              remote: {
                url: '{{ action('Admin\UsersController@search', '%QUERY') }}',
                wildcard: '%QUERY'
              }
        });

        $('#remote .typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 1,
            limit: 10
        }, {
            name: 'users',
            display: function (user) {
                return user.lastname + ' ' + user.firstname + ' ' + user.patronymic;
            },
            source: users,
            templates: {
                suggestion: function (user) {
                    return '<div class="tt-suggestion">' + user.lastname + ' ' + user.firstname + ' '
                        + user.patronymic + '</div>';
                }
            }
        }).on('typeahead:select', function (event, data) {
            location.href = '{{ url('/') }}/admin/users/' + data.id;
        });

        $('.btnDelete').click(function (event) {
            if (! confirm('Удалить пользователя?')) {
                event.preventDefault();
                return false;
            }
        });
    </script>
@endsection