@extends('admin.app')

@section('content')
    @include('errors')
    <div class="panel panel-default">
        <div class="panel-body">
            @include('admin.users._form_student',
                ['user' => $user, 'action' => action('Admin\UsersController@updateStudent', $user->id), 'method' => 'PATCH'])
        </div>
    </div>
@endsection

@section('active_users') active @endsection