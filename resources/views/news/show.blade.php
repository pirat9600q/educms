@extends('app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">{{ $article->title_ru }}</div>
        </div>

        <div class="panel-body">
            {!! $article->text_ru !!}
        </div>

        <div class="panel-footer">
            {{ $article->created_at }}
            @if (!Auth::guest())
                <div class="pull-right">
                    @if (Auth::user()->can('update.news'))
                        <a href="{{ url("news/$article->id/edit") }}" class="btn btn-default btn-xs">
                            {{ _t('buttons.edit', ['object' => '']) }}
                        </a>
                    @endif
                    @if (Auth::user()->can('delete.news'))
                        <form action="news/{{ $article->id }}" method="POST" style="display: inline"
                        onsubmit="return confirm('Удалить выбранную новость?')">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" name="delete" id="delete" class="btn btn-default btn-xs"
                                value="{{ _t('buttons.delete', ['object' => '']) }}">
                        </form>
                    @endif
                </div>
            @endif
        </div>
    </div>
@endsection