@extends('app')

@section('content')
    @include('errors')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">{{ Lang::get('labels.creating') . " " . Lang::get('labels.of_article') }}</div>
        </div>
        <div class="panel-body">
            @include('news._form', [
                'method'  => 'POST',
                'action'  => url('news'),
                'article' => null,
            ])
        </div>
    </div>
@endsection