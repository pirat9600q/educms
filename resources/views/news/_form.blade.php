<form action="{{ $action }}" accept-charset="UTF-8" class="form-horizontal" method="POST">
    <input type="hidden" name="_method" value="{{ $method }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <ul class="nav nav-pills">
      <li role="presentation" class="active"><a id="russianVersion">Русская версия</a></li>
      <li role="presentation"><a id="englishVersion">Английская версия</a></li>
    </ul> <br/>
    <div class="form-group" id="titleRu">
        <div class="col-xs-12">
            <input type="text" class="form-control" name="title_ru" id="title_ru"
            value="{{ $article ? $article->title_ru : old('title_ru') }}" placeholder="{{ trans('labels.title') }}">
        </div>
    </div>
    <div class="form-group" id="titleEn">
        <div class="col-xs-12">
            <input type="text" class="form-control" name="title_en" id="title_en"
            value="{{ $article ? $article->title_en : old('title_en') }}" placeholder="{{ trans('labels.title') }}">
        </div>
    </div>
    <div class="form-group" id="textRu">
        <div class="col-xs-12">
            <textarea class="form-control" name="text_ru" id="text_ru">
                {!! $article ? $article->text_ru : old('text_ru') !!}
            </textarea>
        </div>
    </div>
    <div class="form-group" id="textEn">
        <div class="col-xs-12">
            <textarea class="form-control" name="text_en" id="text_en">
                {!! $article ? $article->text_en : old('text_en') !!}
            </textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success">
                {{ trans('labels.send') }}
            </button>
        </div>
    </div>
</form>

@section('additional_scripts')
    <script type="text/javascript" src="{{ asset('/js/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript">
        CKEDITOR.replace('text_ru', {
            language: '{{ App::getLocale() }}'
        });
        CKEDITOR.replace('text_en', {
            language: '{{ App::getLocale() }}'
        });

        $('#titleEn').hide();
        $('#textEn').hide();

        var version = "russian";
        $('#russianVersion').click(function() {
            if (version == "russian") return;
            toggleInputs();
            setSwitch('russian');
            version = 'russian';
        });

        $('#englishVersion').click(function() {
            if (version == "english") return;
            toggleInputs();
            setSwitch('english');
            version = 'english';
        });

        function toggleInputs() {
            $('#titleRu').toggle();
            $('#titleEn').toggle();
            $('#textRu').toggle();
            $('#textEn').toggle();
        }

        function setSwitch(newVersion) {
            $('#' + version + 'Version').parent().removeClass('active');
            $('#' + newVersion + 'Version').parent().addClass('active');
        }
    </script>
@endsection