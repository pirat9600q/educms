<?php

$host = "localhost";
$port = "3306";
$username = "root";
$password = "";

$alocrimea_is = new mysqli($host, $username, $password, 'alocrimea_is', $port);
$alocrimea_is->set_charset('utf8');

if ($alocrimea_is->connect_errno) {
    echo "Failed to connect to MYSQL: {$alocrimea_is->connect_error} <br>";
}

$result = $alocrimea_is->query("SELECT * FROM teacher");
$entries = [];
while ($row = $result->fetch_assoc()) {
    $entry = [
        'id' => $row['id'],
        'first_name' => '\'' . explode(' ', $row['fio'])[0] . '\'',
        'surname' => '\'' . explode(' ', $row['fio'])[1] . '\'',
        'patronymic' => '\'' . explode(' ', $row['fio'])[2] . '\'',
        'degree' => '\'' . $row['rank'] . '\'',
        'pagetext' => '\'' . addslashes($row['pagetext']) . '\'',
        'post' => '\'' . addslashes($row['occupation']) . '\'',
        'photo' => $row['photoid'],
    ];

    $entries[] = "[" . implode(', ', $entry) . "]";
}

$content = "<?php\nreturn [\n" . implode(",\n\t", $entries) . "\n];";

echo file_put_contents("employees.php", $content);
