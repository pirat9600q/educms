<?php

return <<<EOD
<p>the curriculum provides for the study of the disciplines of natural science training, Humanities and socio-economic orientation, and vocational-oriented courses, which are divided into four cycles.</p>

<h2>learning programming languages</h2>

<ul>
<li>low level Languages: Assembler;</li>
<li>Structural programming languages: Pascal, C;</li>
<li>Object-oriented languages: Object Pascal, C++, Java;</li>
<li>Problem-oriented languages mathematical calculations: Matlab, Mathcad, Maple</li>
<li>Languages artificial intelligence: Lisp, Prolog;</li>
<li>Languages for WEB programming: HTML, XML, Jscript, PHP;</li>
<li>Integrated visual design environment: Delphi, C++ Builder, Microsoft Visual Studio, Active HDL;</li>
<li>Languages modeling systems: GPSS, VHDL;</li>
<li>Languages databases: SQL, Lotus Notes Script;</li>
<li>database management System: Interbase, MySQL;</li>
<li>Object-oriented design: Rational Rose, UML;</li>
<li>Modeling electronic devices: EWB, SMacer;</li>
</ul>

<h2>Professionally-oriented disciplines<strong>&nbsp;</strong></h2>

<h2><strong>Computer mathematics</strong></h2>

<ul>
<li>Discrete mathematics</li>
<li>probability Theory, stochastic processes and mathematical statistics</li>
<li>Computer graphics</li>
<li>Numerical methods</li>
<li>Technology of information protection</li>
<li>information Theory</li>
<li>Technologies of distributed systems and parallel computing</li>
<li>Mathematical methods of operations research</li>
<li>Theory of algorithms</li>
<li>decision Theory</li>
</ul>

<p><strong>Technology programming and data processing</strong></p>

<ul>
<li>Algorithmization and programming</li>
<li>Object-oriented programming</li>
<li>Web technology and Web design</li>
<li>Operating systems</li>
<li>System programming</li>
<li>Technology creation programs-mnich products</li>
<li>the Organization of data and knowledge bases</li>
<li>Methods and systems artificial intelligence</li>
<li>data mining</li>
<li>Cross-platform programming</li>
<li>Technical means of information systems</li>
<li>Java</li>
<li>Platform .Net</li>
<li>basic design of the user interface</li>
<li>database Technology</li>
<li>Basics of digital processing of signals and images</li>
<li>1C</li>
<li>advanced topics in computer graphics</li>
</ul>

<p><strong>system Engineering and system integration</strong></p>

<ul>
<li>System analysis</li>
<li>the Design of information systems</li>
<li>Modeling systems</li>
<li>Methods and means of storing information</li>
<li>Management of IT-projects</li>
<li>Wireless & networks / Mobile technology</li>
<li>Mobile information technology</li>
</ul>

<p><br />
<strong>computer Architecture and computer networks</strong></p>

<ul>
<li>Electrical engineering and electronics</li>
<li>digital electronics and computer architecture</li>
<li>Computer networks</li>
<li>Technology data transfer</li>
<li>Technology computer-aided design</li>
</ul>

<p>&nbsp;</p>
EOD;
