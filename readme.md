## Educms
CMS for educational purposes. In a current state, ships with prepared content.


## Requirements
In order to run this app you'll need:

- PHP >= 5.4

- MCrypt PHP Extension

- Gd PHP Extension

- Curl PHP Extension


## Installation
To install framework and all required dependencies, simply run the following comand in the project root (where `composer.json` located).

```bash
composer install
```

Make sure to check `config/database.php` for database setup and `config/app.php` for general settings. Then run:

```bash
php artisan migrate:refresh --seed
```

This will create all needed tables and fill it with data.
That's it.