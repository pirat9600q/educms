<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

    /**
     * Fillable fields.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'value'
    ];

    protected $allowedTypes = [
        'string', 'boolean', 'integer', 'double'
    ];

    /**
     * Returns value as integer.
     *
     * @return int
     */
    protected function asInteger()
    {
        return intval($this->attributes['value']);
    }

    /**
     * Returns value as boolean.
     *
     * @return bool
     */
    protected function asBoolean()
    {
        return boolval($this->attributes['value']);
    }

    /**
     * Returns value as float.
     *
     * @return float
     */
    protected function asDouble()
    {
        return doubleval($this->attributes['value']);
    }

    public function getValueAttribute()
    {
        switch ($this->type)
        {
            case 'string':
                return $this->attributes['value'];
            case 'integer':
                return $this->asInteger();
            case 'double':
                return $this->asDouble();
            case 'boolean':
                return $this->asBoolean();
        }
    }

    public function setValueAttribute($value)
    {
        if (in_array(gettype($value), $this->allowedTypes))
        {
            $this->attributes['value'] = $value;
            $this->attributes['type'] = gettype($value);
        }
    }

    /**
     * Quick value return by given $key.
     *
     * @param $key
     * @return mixed
     */
    public static function getValue($key)
    {
        $pair = static::where('key', '=', $key)->first();

        return $pair ? $pair->value : null;
    }

    /**
     * Quick value set or update (if exists).
     *
     * @param $key
     * @param $value
     * @return mixed
     */
    public static function setValue($key, $value)
    {
        $pair = static::where('key', '=', $key)->first();

        if ($pair)
        {
            $pair->value = $value;
        } else
        {
            $pair = new Setting();
            $pair->key = $key;
            $pair->value = $value;
        }

        return $pair->save();;
    }
}
