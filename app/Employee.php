<?php namespace App;

use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model implements StaplerableInterface {

    use EloquentTrait;

    protected $fillable = [
        'first_name',
        'surname',
        'patronymic',
        'degree',
        'post',
        'pagetext',
        'photo',
        'order',
        'questions_enabled'
    ];

    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('photo', [
            'styles'      => [
                'info' => [
                    'dimensions'      => '180#x250#',
                    'convert_options' => ['quality' => '90']
                ],
            ],
            'url'         => '/img/employees/:attachment/:id_partition/:style/:secure_hash.:extension',
            'default_url' => '/img/image-placeholder-info.png',
        ]);

        parent::__construct($attributes);
    }

    /**
     * Full name of employee.
     *
     * @return string
     */
    public function fullName()
    {
        return $this->surname . " " . $this->first_name . " " . $this->patronymic;
    }

    /**
     * All publications associated with employee.
     */
    public function publications()
    {
        return $this->belongsToMany('App\Publication');
    }

    /**
     * Employee's disciplines.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function disciplines()
    {
        return $this->belongsToMany('App\Discipline');
    }

    /**
     * Employee's subjects.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subjects()
    {
        return $this->hasMany('App\Subject');
    }

    /**
     * Employee's questions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany('App\Question');
    }
}
