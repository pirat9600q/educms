<?php namespace App;

use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Database\Eloquent\Model;

class Image extends Model implements StaplerableInterface {

    use EloquentTrait;

    protected $fillable = [
        'name_ru',
        'name_en',
        'image',
        'order'
    ];

    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('image', [
            'styles'      => [
                'thumbnail' => [
                    'dimensions'      => '250#x150#',
                    'convert_options' => ['quality' => '60']
                ],
                'medium'    => [
                    'dimensions'      => '640x480',
                    'convert_options' => ['quality' => '80']
                ],
            ],
            'url'         => '/img/gallery/:id/:style/:secure_hash.:extension',
            'default_url' => '/img/image-placeholder.png',
        ]);
        parent::__construct($attributes);
    }

    /**
     * Image belongs to a single album.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function album()
    {
        return $this->belongsTo('App\Album');
    }

    /**
     * Returns name of given $lang locale.
     * Otherwise, returns name of image depending on user language settings.
     *
     * @param null $lang
     * @return mixed
     */
    public function name($lang = null)
    {
        if ($lang == null || !in_array($lang, ['ru', 'en']))
        {
            $lang = user_lang_or_default();
        }

        if ($lang == 'en' && ($this->name_en == null || $this->name_en == ''))
        {
            $lang = 'ru';
        }

        return $this->attributes['name_' . $lang];
    }

}
