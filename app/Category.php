<?php namespace App;

use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Kalnoy\Nestedset\Node;

class Category extends Node {

    protected $fillable = [
        'title_ru',
        'title_en',
        'type',
        'content',
    ];

    /**
     * Used only in private controller methods
     * to switch between locales.
     *
     * @var string
     */
    private static $locale = 'ru';

    /**
     * {@inheritdoc}
     *
     * @param array $attributes
     * @return bool
     */
    public function update(array $attributes = array())
    {
        $oldContent = $this->content;
        $saved = parent::update([
            'title_ru' => $attributes['title_ru'],
            'title_en' => $attributes['title_en'],
            'type'     => $attributes['type'],
            'content'  => $attributes['content'],
        ]);
        if (!$saved) return false;

        // TODO: parent_id -> incoming 'null' string is bad, change to something else (i.e. number).
        if (isset($attributes['parent_id']))
        {
            $this->parent_id =
                $attributes['parent_id'] == 'null' ? null
                    : (int) $attributes['parent_id'];
            $this->save();
        }

        if ($this->type === 'page')
        {
            StaticPage::delete($oldContent);
            StaticPage::write($this->content == '/' ? 'home' : $this->content, $attributes['text_ru'], 'ru');
            StaticPage::write($this->content == '/' ? 'home' : $this->content, $attributes['text_en'], 'en');
        }

        static::storeHtmlMenuTree();

        return true;
    }

    /**
     * {@inheritdoc}
     *
     * @param array $attributes
     * @param Node $parent
     * @return static
     */
    public static function create(array $attributes = array(), Node $parent = null)
    {
        $node = parent::create([
            'title_ru' => $attributes['title_ru'],
            'title_en' => $attributes['title_en'],
            'type'     => $attributes['type'],
            'content'  => $attributes['content'],
        ], $parent);
        if ($node == null) return null;

        if (isset($attributes['parent_id']))
        {
            $node->parent_id = $attributes['parent_id'] == 'null' ?
                null : (int) $attributes['parent_id'];
            $node->save();
        }

        if ($node->type == 'page')
        {
            StaticPage::write($node->content == '/' ? 'home' : $node->content, $attributes['text_ru'], 'ru');
            StaticPage::write($node->content == '/' ? 'home' : $node->content, $attributes['text_en'], 'en');
        }

        static::storeHtmlMenuTree();

        return $node;
    }


    /**
     * Returns title of page depending on current locale.
     * Used only when generating html code for menu.
     *
     * @param $lang
     * @return mixed
     */
    public function title($lang = null)
    {
        if ($lang == null) $lang = user_lang_or_default();

        return $this->attributes['title_' . $lang];
    }

    /**
     * Returns category data as link to page.
     *
     * @return string
     */
    private function asPage()
    {
        $template = "<li><a href=\"{{ url('%s') }}\">%s</a></li>" . PHP_EOL;

        return sprintf($template, $this->content, $this->title(static::$locale));
    }

    /**
     * Returns category data as controller action.
     *
     * @return string
     */
    private function asController()
    {
        $template = "<li><a href=\"{{ action('%s') }}\">%s</a></li>" . PHP_EOL;

        return sprintf($template, $this->content, $this->title(static::$locale));
    }


    /**
     * Returns category data as stub.
     *
     * @return string
     */
    private function asStub()
    {
        $template = "<a href=\"\"><i class=\"glyphicon glyphicon-chevron-right icon-arrow-right\"></i> %s </a>" . PHP_EOL;

        return sprintf($template, $this->title(static::$locale));
    }

    /**
     * Returns category data depending on its type.
     *
     * @return string
     */
    public function getHtmlContent()
    {
        switch ($this->type)
        {
            case "page":
                return $this->asPage();
            case "controller":
                return $this->asController();
            case "stub":
                return $this->asStub();
        }
    }

    /**
     * Returns HTML menu.
     *
     * @return string
     */
    public static function toHtmlMenuTree()
    {
        $nodes = static::defaultOrder()->get()->toTree();
        $result = "<ul class=\"dropdown-menu main-menu\">" . PHP_EOL;

        foreach ($nodes as $node)
        {
            $result .= static::createHtmlMenuItem($node);
        }

        $result .= "</ul>";

        return $result;
    }

    /**
     * Saves HTML menu to view file.
     */
    public static function storeHtmlMenuTree()
    {
        $path = views_path() . DIRECTORY_SEPARATOR;

        static::$locale = 'ru';
        file_put_contents($path . 'menu_ru.blade.php', static::toHtmlMenuTree());

        static::$locale = 'en';
        file_put_contents($path . 'menu_en.blade.php', static::toHtmlMenuTree());
    }

    /**
     * Returns HTML code for a single menu entry.
     *
     * @param Category $node
     * @return string
     */
    private static function createHtmlMenuItem(Category $node)
    {
        if ($node->type != "stub") return $node->getHtmlContent();
        $result = "<li class=\"dropdown-submenu\">" . PHP_EOL
            . $node->getHtmlContent()
            . "<ul class=\"dropdown-menu main-menu-submenu\">" . PHP_EOL;

        foreach ($node->children as $child)
        {
            $result .= static::createHtmlMenuItem($child);
        }

        $result .= "</ul>" . PHP_EOL . "</li>" . PHP_EOL;

        return $result;
    }

    /**
     * Creates stub from attributes array.
     *
     * @param array $attrs
     */
    public static function createStub(array $attrs)
    {
        if ($attrs['parent_id'] == 'null')
        {
            $attrs['parent_id'] = null;
        }

        if ($attrs['title_en'] == '')
        {
            $attrs['title_en'] = $attrs['title_ru'];
        }

        $category = new Category($attrs);
        $category->parent_id = $attrs['parent_id'];

        if ($category->save())
        {
            Category::storeHtmlMenuTree();
        };
    }


    /**
     * Deletes category.
     *
     * @return bool|mixed|null
     */
    public function delete()
    {
        $content = $this->content;
        $type = $this->type;
        $deleted = parent::delete();

        if ($deleted)
        {
            ($type == 'page') && StaticPage::delete($content);
            static::storeHtmlMenuTree();
        }

        return $deleted;
    }
}
