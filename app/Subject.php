<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model {

    protected $fillable = [
        'name', 'year', 'employee_id'
    ];

    /**
     * Employee that holds this subject.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

}
