<?php namespace App\Providers;

use Codesleeve\Stapler\Config\NativeConfig;
use Codesleeve\Stapler\Stapler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Stapler::boot();

        $config = new NativeConfig;
        Stapler::setConfigInstance($config);

        // Location of application's document root:
        $config->set('stapler.public_path', base_path() . DS . 'public');

        // Location of application's base folder.
        $config->set('stapler.base_path', base_path());

        // Validator rule that ensures that sender and receiver
        // are different users.
        Validator::extend('message_sender_receiver', function($attribute, $value, $parameters) {
            return $value != Auth::user()->id;
        });

        Validator::extend('receivers_list', function($attribute, $value, $parameters) {
            return count($value) > 0 && !in_array(Auth::user()->id, $value);
        });

        // Main app views location.
        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'educms');

        // Load translations from main app resources location.
        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang/vendor/forum', 'forum');
    }

    /**
     * Register any application services.
     *
     * This service provider is a great spot to register your various container
     * bindings with the application. As you can see, we are registering our
     * "Registrar" implementation here. You can add your own bindings too!
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Illuminate\Contracts\Auth\Registrar',
            'App\Services\Registrar'
        );
    }

}
