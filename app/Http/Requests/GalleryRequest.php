<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class GalleryRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::user() && Auth::user()->can('update.albums');
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'name_ru' => 'required|min:3|max:255',
            'name_en' => 'required|min:3|max:255',
            'description_ru' => 'required|min:3|max:255',
            'description_en' => 'required|min:3|max:255',
            'thumbnail' => 'image'
		];
	}

}
