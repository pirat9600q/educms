<?php namespace App\Http\Requests;


use Illuminate\Support\Facades\Auth;

class SubjectsRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::user() && Auth::user()->can('update.subjects');
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|min:3|max:255',
            'year' => 'required|integer|min:1900',
            'employee_id' => 'required|integer|exists:employees,id',
		];
	}

}
