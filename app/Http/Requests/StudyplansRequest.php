<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class StudyplansRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::user() && Auth::user()->can('update.studyplan');
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'discipline_id' => 'required|numeric|exists:disciplines,id',
            'semester' => 'required|integer|min:1',
            'hours' => 'required|integer|min:0',
            'has_project' => '',
            'degree' => 'required|in:bachelor,master',
		];
	}

}
