<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class NewsRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest() && Auth::user()->can('update.news,create.news');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_ru' => 'required|min:6|max:255',
            'text_ru'  => 'required|min:6|max:4096',
            'title_en' => 'min:6|max:255',
            'text_en'  => 'min:10|max:4096',
        ];
    }

}
