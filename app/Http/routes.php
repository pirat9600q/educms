<?php
use App\Category;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Frontend routes
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('/users/search/{term?}', ['uses' => 'UsersController@search']);
Route::group(['middleware' => ['auth', 'acl']], function ()
{
    Route::get('/home/edit', ['as' => 'home.edit', 'can' => 'update.news', 'uses' => 'HomeController@edit']);
    Route::patch('/home/update', ['as' => 'home.edit', 'can' => 'update.news', 'uses' => 'HomeController@update']);
});

/**
 * News
 */
Route::group(['middleware' => 'acl'], function ()
{
    Route::get(
        'news', ['as' => 'news', 'uses' => 'NewsController@index', 'can' => '']);
    Route::get(
        'news/create', ['as' => 'news.create', 'uses' => 'NewsController@create', 'can' => 'create.news']);
    Route::get(
        'news/{id}/edit', ['as' => 'news.edit', 'uses' => 'NewsController@edit', 'can' => 'update.news']);
    Route::post(
        'news', ['uses' => 'NewsController@store', 'can' => 'create.news,update.news']);
    Route::patch(
        'news/{id}', ['uses' => 'NewsController@update', 'can' => 'update.news']);
    Route::get(
        'news/{id}', ['as' => 'news.show', 'uses' => 'NewsController@show', 'can' => '']);
    Route::delete(
        'news/{id}', ['uses' => 'NewsController@destroy', 'can' => 'delete.news']);
});

/**
 * Employees
 */
Route::group(['middleware' => 'acl'], function ()
{
    Route::get(
        'employees', ['as' => 'employees', 'uses' => 'EmployeesController@index', 'can' => '']);
    Route::get(
        'employees/search/{term?}', ['uses' => 'EmployeesController@search', 'can' => '']);
    Route::get(
        'employees/create', ['as' => 'employees.create', 'uses' => 'EmployeesController@create', 'can' => 'create.employees']);
    Route::post(
        'employees', ['uses' => 'EmployeesController@store', 'can' => 'create.employees,update.employees']);
    Route::post(
        'employees/sort', ['uses' => 'EmployeesController@sort', 'can' => 'update.employees']);
    Route::patch(
        'employees/{id}', ['uses' => 'EmployeesController@update', 'can' => '']);
    Route::get(
        'employees/{id}', ['as' => 'employees.show', 'uses' => 'EmployeesController@show', 'can' => '']);
    Route::get(
        'employees/{id}/publications', ['as' => 'employees.publications', 'uses' => 'EmployeesController@showPublications', 'can' => '']);
    Route::delete(
        'employees/{id}', ['uses' => 'EmployeesController@destroy', 'can' => 'delete.employees']);
    Route::get(
        'employees/{id}/ask', ['as' => 'employees.ask', 'uses' => 'QuestionsController@ask', 'can' => '']);
    Route::post(
        'employees/{id}/ask', ['uses' => 'QuestionsController@store', 'can' => '']);
    Route::get(
        'employees/{id}/answers', ['as' => 'employees.answers', 'uses' => 'QuestionsController@answers', 'can' => '']);
});

/**
 * Questions
 */
Route::group([], function () {
//    Route::get('questions', ['as' => 'questions', 'uses' => 'QuestionsController@index']);
    Route::get('answer/{id}', ['as' => 'questions.answer', 'uses' => 'QuestionsController@answer']);
    Route::post('answer/{id}', ['uses' => 'QuestionsController@storeAnswer']);
    Route::get('answers/{id}/edit', ['uses' => 'QuestionsController@edit']);
    Route::get('answers/{id}/destroy', ['uses' => 'QuestionsController@destroy']);
});

/**
 * Studyplan
 */
Route::group(['middleware' => 'acl'], function ()
{
    Route::get(
        'sudyplan', ['as' => 'studyplan', 'uses' => 'StudyplansController@index', 'can' => '']);
    Route::get(
        'sudyplan/create', ['as' => 'studyplan.create', 'uses' => 'StudyplansController@create', 'can' => 'create.studyplan']);
    Route::get(
        'sudyplan/{id}/edit', ['as' => 'studyplan.edit', 'uses' => 'StudyplansController@edit', 'can' => 'update.studyplan']);
    Route::post(
        'sudyplan', ['uses' => 'StudyplansController@store', 'can' => 'create.studyplan,update.studyplan']);
    Route::patch(
        'sudyplan/{id}', ['uses' => 'StudyplansController@update', 'can' => 'update.studyplan']);
    Route::get(
        'sudyplan/{id}', ['uses' => 'StudyplansController@show', 'can' => '']);
    Route::delete(
        'sudyplan/{id}', ['uses' => 'StudyplansController@destroy', 'can' => 'delete.studyplan']);
});

/**
 * Subjects
 */
Route::group(['middleware' => 'acl'], function ()
{
    Route::get(
        'subjects', ['as' => 'subjects', 'uses' => 'SubjectsController@index', 'can' => '']);
    Route::get(
        'subjects/create', ['as' => 'subjects.create', 'uses' => 'SubjectsController@create', 'can' => 'create.subjects']);
    Route::get(
        'subjects/{id}/edit', ['as' => 'subjects.edit', 'uses' => 'SubjectsController@edit', 'can' => 'update.subjects']);
    Route::post(
        'subjects', ['uses' => 'SubjectsController@store', 'can' => 'create.subjects,update.subjects']);
    Route::patch(
        'subjects/{id}', ['uses' => 'SubjectsController@update', 'can' => 'update.subjects']);
    Route::get(
        'subjects/{id}', ['as' => 'subjects.show', 'uses' => 'SubjectsController@show', 'can' => '']);
    Route::delete(
        'subjects/{id}', ['uses' => 'SubjectsController@destroy', 'can' => 'delete.subjects']);
});

/**
 * Publications
 */
Route::group(['middleware' => 'acl'], function ()
{
    Route::get(
        'publications', ['as' => 'publications', 'uses' => 'PublicationsController@index', 'can' => '']);
    Route::get(
        'publications/create', ['as' => 'publications.create', 'uses' => 'PublicationsController@create', 'can' => 'create.publications']);
    Route::get(
        'publications/{id}/edit', ['as' => 'publications.edit', 'uses' => 'PublicationsController@edit', 'can' => 'update.publications']);
    Route::post(
        'publications', ['uses' => 'PublicationsController@store', 'can' => 'create.publications,update.publications']);
    Route::patch(
        'publications/{id}', ['uses' => 'PublicationsController@update', 'can' => 'update.publications']);
    Route::get(
        'publications/{id}', ['as' => 'publications.show', 'uses' => 'PublicationsController@show', 'can' => '']);
    Route::delete(
        'publications/{id}', ['uses' => 'PublicationsController@destroy', 'can' => 'delete.publications']);
});

/**
 * Disciplines
 */
Route::group(['middleware' => 'acl'], function ()
{
    Route::get(
        'disciplines', ['as' => 'discipline', 'uses' => 'DisciplinesController@index', 'can' => '']);
    Route::get(
        'disciplines/create', ['as' => 'discipline.create', 'uses' => 'DisciplinesController@create', 'can' => 'create.disciplines']);
    Route::get(
        'disciplines/{id}/edit', ['as' => 'discipline.edit', 'uses' => 'DisciplinesController@edit', 'can' => 'update.disciplines']);
    Route::post(
        'disciplines', ['uses' => 'DisciplinesController@store', 'can' => 'create.disciplines,update.disciplines']);
    Route::patch(
        'disciplines/{id}', ['uses' => 'DisciplinesController@update', 'can' => 'update.disciplines']);
    Route::get(
        'disciplines/{id}', ['as' => 'discipline.show', 'uses' => 'DisciplinesController@show', 'can' => '']);
    Route::delete(
        'disciplines/{id}', ['uses' => 'DisciplinesController@destroy', 'can' => 'delete.disciplines']);
});

/**
 * Gallery
 */
Route::group(['middleware' => 'acl'], function ()
{

    Route::get(
        'gallery', ['as' => 'gallery', 'uses' => 'AlbumsController@index', 'can' => '']);
    Route::get(
        'gallery/create', ['as' => 'gallery.create', 'uses' => 'AlbumsController@create', 'can' => 'create.albums']);
    Route::get(
        'gallery/{id}/upload', ['as' => 'gallery.upload', 'uses' => 'AlbumsController@upload', 'can' => 'create.albums,update.albums']);
    Route::post(
        'gallery/{id}/upload', ['as' => 'gallery.upload', 'uses' => 'AlbumsController@upload', 'can' => 'create.albums,update.albums']);
    Route::get(
        'gallery/{id}/edit', ['as' => 'gallery.edit', 'uses' => 'AlbumsController@edit', 'can' => 'update.albums']);
    Route::post(
        'gallery/{id}/sort', ['uses' => 'AlbumsController@sort', 'can' => 'update.albums']);
    Route::post(
        'gallery', ['uses' => 'AlbumsController@store', 'can' => 'create.albums,update.albums']);
    Route::patch(
        'gallery/{id}', ['uses' => 'AlbumsController@update', 'can' => 'update.albums']);
    Route::get(
        'gallery/{id}', ['as' => 'gallery.show', 'uses' => 'AlbumsController@show', 'can' => '']);
    Route::delete(
        'gallery/{id}', ['uses' => 'AlbumsController@destroy', 'can' => 'delete.albums']);
});

/**
 * Messages and mailing lists.
 */
Route::group(['middleware' => ['auth', 'acl']], function ()
{
    Route::group(['prefix' => 'messages/mailinglists'], function() {
        Route::get('/', 'MailingListsController@index');
        Route::get('create', 'MailingListsController@create');
        Route::post('store', 'MailingListsController@store');
        Route::get('show/{id}', 'MailingListsController@show');
        Route::get('edit/{id}', 'MailingListsController@edit');
        Route::get('composemessage/{id}', 'MailingListsController@composeMessage');
        Route::post('sendmessage/{id}', 'MailingListsController@sendMessage');
        Route::patch('update/{id}', 'MailingListsController@update');
        Route::delete('/', 'MailingListsController@destroy');
    });

    Route::get('messages/sendtoall', 'MessagesController@sendToAll');
    Route::get('messages/inbox', 'MessagesController@inbox');
    Route::get('messages/outbox', 'MessagesController@outbox');
    Route::get('messages/create', 'MessagesController@create');
    Route::get('messages/{id}', 'MessagesController@show');
    Route::get('messages/users/search', 'MessagesController@searchUsers');
    Route::post('messages', 'MessagesController@send');
    Route::delete('messages', 'MessagesController@destroyAll');
});
/**
 * Password reset
 */
Route::controllers([
    'password' => 'Auth\PasswordController',
]);

/**
 * Auth and register
 */
Route::group([], function ()
{
    Route::get('login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@getLogin']);
    Route::get('register', ['as' => 'auth.register', 'uses' => 'Auth\AuthController@getRegister']);
    Route::get('logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@getLogout']);
    Route::get('verify/{code}', ['as' => 'auth.verify', 'uses' => 'Auth\AuthController@getVerify']);
    Route::post('login', ['uses' => 'Auth\AuthController@postLogin']);
    Route::post('register', ['as' => 'auth.registered', 'uses' => 'Auth\AuthController@postRegister']);
});

// Backend routes
Route::group([
    'namespace'  => 'Admin',
    'prefix'     => 'admin',
    'middleware' => ['auth', 'acl'],
    'is'         => 'admin',
], function ()
{
    Route::get('/', 'DashboardController@index');
    Route::group([], function ()
    {
        Route::get(
            'categories', 'CategoriesController@index');
        Route::get(
            'categories/create/{type}', 'CategoriesController@create');
        Route::get(
            'categories/{type}/edit', 'CategoriesController@edit');
        Route::post(
            'categories', 'CategoriesController@store');
        Route::patch(
            'categories/{id}', 'CategoriesController@update');
        Route::get(
            'categories/{id}/up', 'CategoriesController@up');
        Route::get(
            'categories/{id}/down', 'CategoriesController@down');
        Route::get(
            'categories/{id}', 'CategoriesController@show');
        Route::delete(
            'categories/{id}', 'CategoriesController@destroy');
    });

    Route::group([], function ()
    {
        Route::get(
            'settings', 'SettingsController@index');
        Route::patch(
            'settings', 'SettingsController@update');
    });

    Route::group([], function ()
    {
        Route::get(
            'users/search/{term}', 'UsersController@search');
        Route::get(
            'users', 'UsersController@index');
        Route::get(
            'users/{id}', 'UsersController@show');
        Route::get(
            'users/{id}/destroy', 'UsersController@destroy');
        Route::get(
            'users/{id}/activate', 'UsersController@activate');
        Route::get(
            'users/{id}/deactivate', 'UsersController@deactivate');
        Route::get(
            'users/{id}/confirm', 'UsersController@confirm');
        Route::get(
            'users/create/student', 'UsersController@createStudent');
        Route::get(
            'users/create/employee', 'UsersController@createEmployee');
        Route::post(
            'users/create/student', 'UsersController@storeStudent');
        Route::post(
            'users/create/employee', 'UsersController@storeEmployee');
        Route::patch(
            'users/update/{id}/student', 'UsersController@updateStudent');
        Route::patch(
            'users/update/{id}/employee', 'UsersController@updateEmployee');
        Route::post(
            'users/create/employee', 'UsersController@storeEmployee');
        Route::get(
            'users/{id}/edit', 'UsersController@edit');
    });

    // Forum categories
    Route::group(['prefix' => 'forum/categories'], function()
    {
        Route::get('/', 'ForumController@index');

        Route::get('createCategory/{parent_category?}', 'ForumController@createCategory');

        Route::post('storeCategory}', 'ForumController@storeCategory');

        Route::get('editCategory/{id}', 'ForumController@editCategory');

        Route::patch('updateCategory/{id}', 'ForumController@updateCategory');

        Route::delete('destroyCategory/{id}', 'ForumController@destroyCategory');
    });
});

// Language switcher
Route::get('/ru', function ()
{
    return redirect()->back()->withCookie(cookie('lang', 'ru', 43200));
});
Route::get('/en', function ()
{
    return redirect()->back()->withCookie(cookie('lang', 'en', 43200));
});

Route::get('/{page}', ['as' => 'pages', function ($page)
{
    $category = Category::where('content', '=', $page)->first();
    if ($category == null) abort(404);
    $lang = user_lang_or_default();

    return view('pages.' . $lang . '.' . $page);
}]);

