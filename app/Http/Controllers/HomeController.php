<?php namespace App\Http\Controllers;

use App\Page;
use Chromabits\Purifier\Purifier;
use Illuminate\Http\Request;

class HomeController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller shows a home page. Index page itself shown as a static
    | page and could be handled by CategoriesController, but we keep it as is
    | for future. If we ever need to add advanced logic on index page, it
    | will help.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return \App\Http\Controllers\HomeController
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
        $lang = user_lang_or_default();

        $page = Page::where('internal_name', 'main')->get()->first();
        $title = ($lang == 'en') ? ($page->title_en) : ($page->title_ru);
        $content = ($lang == 'en') ? ($page->content_en) : ($page->content_ru);

        return view('pages.home', ['title' => $title, 'content' => $content]);
    }

    public function edit()
    {
        $page = Page::where('internal_name', 'main')->get()->first();

        return view('admin.home.edit', ['page' => $page]);
    }

    public function update(Request $request)
    {
        $page = Page::where('internal_name', 'main')->get()->first();
        $page->content_ru = $request->get('content_ru');
        $page->content_en = $request->get('content_en');
        $page->save();

        return redirect(action('HomeController@index'));
    }
}
