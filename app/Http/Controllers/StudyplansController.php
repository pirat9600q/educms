<?php namespace App\Http\Controllers;

use App\Discipline;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\StudyplansRequest;
use App\Studyplan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class StudyplansController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Studyplans Controller
    |--------------------------------------------------------------------------
    |
    | Controller for managing study plans.
    |
    */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $semester = 1;
        $degree = 'bachelor';

        if ($request->has('semester'))
        {
            $semester = $request->input('semester');
        }

        if ($request->has('degree'))
        {
            $degree = $request->input('degree');
        }

        $semesters = $this->getSemesters($degree);
        $degrees = $this->getDegrees();

        return view('studyplans.index')->with([
            'studyplan' => Studyplan::whereSemester($semester)
                ->whereDegree($degree)
                ->get(),
            'selectedSemester' => $semester,
            'semesters' => $semesters,
            'selectedDegree' => $degree,
            'degrees' => $degrees,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('studyplans.create')->with([
            'disciplines' => Discipline::all()->sortBy('name')->lists('name', 'id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StudyplansRequest $request
     * @return Response
     */
    public function store(StudyplansRequest $request)
    {
        $studyplan = new Studyplan($request->all());
        $studyplan->has_project = $request->has('has_project');
        $studyplan->has_exam = $request->has('has_exam');
        $studyplan->save();

        return redirect(action('StudyplansController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return view('studyplans.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('studyplans.edit')->with([
            'plan' => Studyplan::findOrFail($id),
            'disciplines' => Discipline::all()->sortBy('name')->lists('name', 'id')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param StudyplansRequest $request
     * @return Response
     */
    public function update($id, StudyplansRequest $request)
    {
        $plan = Studyplan::findOrFail($id);
        $plan->update($request->all());
        $plan->has_project = $request->has('has_project');
        $plan->has_exam = $request->has('has_exam');
        $plan->save();

        return redirect(action('StudyplansController@index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $studyplan = Studyplan::findOrFail($id);
        $studyplan->delete();

        return redirect()->back();
    }
    
    protected function getDegrees()
    {
        return ['bachelor', 'master'];
    }
    
    public function getSemesters($degree)
    {
        return DB::table('studyplans')
            ->select('semester')
            ->distinct()
            ->where('degree', '=', $degree)
            ->orderBy('semester')
            ->lists('semester');
    }

}
