<?php namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CategoriesRequest;
use App\StaticPage;
use Illuminate\Http\Response;

class CategoriesController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Categories Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles all operations with categories of site and
    | creates navigation menu dynamically.
    |
    */

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.categories.index')->with([
            'categories' => Category::defaultOrder()->withDepth()->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param String $type -> ['page', 'stub']
     * @return Response
     */
    public function create($type)
    {
        return view('admin.categories.create_' . $type)->with([
            'stubs' => Category::defaultOrder()->where('type', '=', 'stub')->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoriesRequest $request
     * @return Response
     */
    public function store(CategoriesRequest $request)
    {
        Category::create($request->all());

        return redirect('admin/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin.categories.show')->with([
            'category' => Category::findOrFail($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);

        if ($category->type == 'page')
        {
            $category->text_ru = StaticPage::read($category->content, 'ru');
            $category->text_en = StaticPage::read($category->content, 'en');
        }

        return view('admin.categories.edit_' . $category->type)->with([
            'category' => $category,
            'stubs'    => Category::defaultOrder()->where('type', '=', 'stub')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param CategoriesRequest $request
     * @return Response
     */
    public function update($id, CategoriesRequest $request)
    {
        $category = Category::findOrFail($id);
        $category->update($request->all());

        return redirect('/admin/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        return redirect('/admin/categories');
    }

    /**
     * Moves page up in a menu tree.
     *
     * @param $id
     * @return string
     */
    public function up($id)
    {
        $isMoved = Category::find($id)->up();

        if ($isMoved)
        {
            Category::storeHtmlMenuTree();
        }

        return redirect()->back();
    }

    /**
     * Moves page down in a menu tree.
     *
     * @param $id
     * @return string
     */
    public function down($id)
    {
        $isMoved = Category::find($id)->down();

        if ($isMoved)
        {
            Category::storeHtmlMenuTree();
        }

        return redirect()->back();
    }

}
