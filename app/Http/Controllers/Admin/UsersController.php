<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\Registrar;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Kodeine\Acl\Models\Eloquent\Permission;

class UsersController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Users Controller
    |--------------------------------------------------------------------------
    |
    | Users management controller. It takes care of creating students and
    | employees, activate/deactivate users, edit accounts and user rights.
    | It is also provides moderated registration, which works as regular
    | registration with email confirmation but instead of instantly sending an
    | email after registration, it is foremost required to be checked by staff.
    |
    */

    protected $_PERMISSIONS;

    public function __construct(Registrar $registrar)
    {
        parent::__construct();

        $this->_PERMISSIONS = ['news', 'employees', 'studyplan', 'publications', 'albums'];

        $this->registrar = $registrar;
    }


    /**
     * Show user control panel.
     *
     * @return $this
     */
    public function index()
    {
        return view('admin.users.index')->with([
            'users' => User::orderBy('created_at', 'desc')->get(),
        ]);
    }

    /**
     * Show a single user.
     *
     * @param $id
     * @return $this
     */
    public function show($id)
    {
        $user = User::find($id);

        return view('admin.users.show')->with([
            'user' => $user,
        ]);
    }

    /**
     * Activate user account.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate($id)
    {
        $user = User::find($id);

        $user->active = true;
        $user->save();

        return redirect()->back();
    }

    /**
     * Deactivate user account.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deactivate($id)
    {
        $user = User::find($id);

        $user->active = false;
        $user->save();

        return redirect()->back();
    }

    /**
     * Confirm user account and send an email verification code.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function confirm($id)
    {
        $user = User::findOrFail($id);

        $code = str_random(64);
        $name = $user->firstname;
        $email = $user->email;

        $user->confirmation_code = $code;

        if ($user->save())
        {
            Mail::queue(
                ['emails.confirm', 'emails.confirm_plane'],
                ['name' => $name, 'code' => $code],
                function ($message) use ($email)
                {
                    $message->from('is@sevgtu.ru', 'Кафедра ИС');
                    $message->to($email);
                    $message->subject('Подтверждение регистрации');
                });
        }

        return redirect()->back();
    }

    /**
     * Render form for student creation.
     *
     * @return \Illuminate\View\View
     */
    public function createStudent()
    {
        return view('admin.users.create_student');
    }

    /**
     * Store user as student.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeStudent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname'  => 'required|string|alpha|min:3|max:60',
            'lastname'   => 'required|string|alpha|min:3|max:60',
            'patronymic' => 'string|min:3|alpha|max:60',
            'email'      => 'required|email|unique:users',
            'group'      => 'numeric|exists:groups,id',
        ]);

        if ($validator->fails())
        {
            return view('admin.users.create_student')
                ->withInput()
                ->withErrors($validator);
        }

        $user = $this->registrar->create($request->all());
        $user->confirmed = true;
        $user->active = true;
        $user->save();

        $user->assignRole('student');

        return redirect(action('Admin\UsersController@index'));
    }

    /**
     * Update student info.
     *
     * @return \Illuminate\View\View
     */
    public function updateStudent($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname'  => 'required|string|alpha|min:3|max:60',
            'lastname'   => 'required|string|alpha|min:3|max:60',
            'patronymic' => 'string|min:3|alpha|max:60',
            'email'      => 'required|email|unique:users',
            'group'      => 'numeric|exists:groups,id',
        ]);

        if ($validator->fails())
        {
            return view('admin.users.edit_student')
                ->withInput()
                ->withErrors($validator);
        }

        $user = User::find($id);
        $user->update($request->all());

        dd($request->all());
    }

    /**
     * Update employee info.
     *
     * @return \Illuminate\View\View
     */
    public function updateEmployee($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname'  => 'required|string|alpha|min:3|max:60',
            'lastname'   => 'required|string|alpha|min:3|max:60',
            'patronymic' => 'string|min:3|alpha|max:60',
            'email'      => 'email',
        ]);

        if ($validator->fails())
        {
            return redirect(action('Admin\UsersController@edit', $id))
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::find($id);
        $permissions = $request->input('perm');
        if (!$permissions) $permissions = array();

        $user->firstname = $request->input('firstname');
        $user->lastname = $request->input('lastname');
        $user->patronymic = $request->input('patronymic');
        $user->email = $request->input('email');

        if ($request->input('password') != '')
        {
            $user->password = Hash::make($request->input('password'));
        }

        $user->save();

        foreach ($this->_PERMISSIONS as $perm)
        {
            if (in_array($perm, $permissions))
                $user->assignPermission($perm);
            else
                $user->revokePermission($perm);
        }

        if ($request->has('admin'))
            $user->assignRole('admin');
        else
            $user->revokeRole('admin');

        $pages = $user->employeesPages();
        if (count($pages) > 0)
        {
            foreach ($pages as $page)
            {
                $user->revokePermission("employee_{$page->id}");
            }
        }

        if ($request->input('employee') != -1)
        {
            $newEmpId = $request->input('employee');
            $user->assignPermission("employee_{$newEmpId}");
        }

        return redirect(action('Admin\UsersController@show', $id));
    }

    /**
     * Render form for employee creation.
     *
     * @return \Illuminate\View\View
     */
    public function createEmployee()
    {
        return view('admin.users.create_employee');
    }

    /**
     * Store user as employee.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeEmployee(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname'  => 'required|string|alpha|min:3|max:60',
            'lastname'   => 'required|string|alpha|min:3|max:60',
            'patronymic' => 'string|min:3|alpha|max:60',
            'email'      => 'email|unique:users',
            'password'   => 'required|min:5'
        ]);

        if ($validator->fails())
        {
            return redirect(action('Admin\UsersController@createEmployee'))
                ->withErrors($validator)
                ->withInput();
        }

        $user = new User();
        $permissions = $request->input('perm');
        if (!$permissions) $permissions = array();

        $user->firstname = $request->input('firstname');
        $user->lastname = $request->input('lastname');
        $user->patronymic = $request->input('patronymic');
        $user->email = $request->input('email');
        $user->active = true;
        $user->confirmed = true;
        $user->password = Hash::make($request->input('password'));

        $user->save();
        $user->assignRole('employee');

        foreach ($this->_PERMISSIONS as $perm)
        {
            if (in_array($perm, $permissions))
                $user->assignPermission($perm);
            else
                $user->revokePermission($perm);
        }

        if ($request->has('admin'))
            $user->assignRole('admin');
        else
            $user->revokeRole('admin');

        $pages = $user->employeesPages();
        if (count($pages) > 0)
        {
            foreach ($pages as $page)
            {
                $user->revokePermission("employee_{$page->id}");
            }
        }

        if ($request->input('employee') != -1)
        {
            $newEmpId = $request->input('employee');
            $user->assignPermission("employee_{$newEmpId}");
        }

        return redirect(action('Admin\UsersController@show', $user->id));
    }

    /**
     * Handle ajax query for typeahead.js plugin.
     *
     * @param null $term
     * @return mixed
     */
    public function search($term = null)
    {
        $users = DB::table('users')
            ->where('firstname', 'LIKE', '%' . $term . '%')
            ->orWhere('lastname', 'LIKE', '%' . $term . '%')
            ->orWhere('patronymic', 'LIKE', '%' . $term . '%')
            ->get([
                'id',
                'firstname',
                'lastname',
                'patronymic',
            ]);

        return response()->json($users);
    }

    /**
     * Delete user.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $user = User::find($id);

        $user->delete();

        return redirect(action('Admin\UsersController@index'));
    }

    /**
     * Edit user.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id)
    {
        $user = User::find($id);

        if ($user->is('student'))
        {
            return view('admin.users.edit_student')->with(['user' => $user]);
        }

        return view('admin.users.edit_employee')->with(['user' => $user]);
    }

}
