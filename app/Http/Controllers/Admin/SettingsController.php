<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingsController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Settings Controller
    |--------------------------------------------------------------------------
    |
    | This controller manages global settings of site, such as site name and
    | so on.
    |
    */

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $settings = DB::table('settings')->lists('value', 'key');

        return view('admin.settings.index')->with([
            'settings'      => $settings,
            'labelStyle'    => ['class' => 'col-xs-12 col-sm-4 col-md-4 col-lg-3 control-label'],
            'inputStyle'    => ['class' => 'form-control'],
            'inputDivStyle' => 'col-xs-12 col-sm-8 col-md-8 col-lg-7'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        Setting::setValue('global.title', $request->input('global_title'));
        Setting::setValue('global.departmentName', $request->input('global_departmentName'));
        Setting::setValue('auth.allowRegistration', $request->has('auth_allowRegistration'));
        Setting::setValue('auth.allowLogin', $request->has('auth_allowLogin'));

        return redirect(action('Admin\SettingsController@index'));
    }
}
