<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Riari\Forum\Models\Category;

class ForumController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        function discriminator($category) {
            return ($category->parent_category != null) ? ($category->parent_category) : ($category->id);
        };

        function group($all) {
            $groups = [];
            foreach($all as $c) {
                $group = discriminator($c);
                if(!isset($groups[$group])) {
                    $groups[$group] = [];
                }
                array_push($groups[$group], $c);
            }
            return $groups;
        }

        $groups = group(Category::all());

        foreach($groups as $groupId => &$group) {
            usort($group, function($l, $r) use($groupId) {
                if($l->id == $groupId) {
                    return -1;
                }
                else if($r->id == $groupId) {
                    return 1;
                }
                else {
                    return strcmp($l->title, $r->title);
                }
            });
        }

        usort($groups, function($l, $r) {
            return strcmp($l[0]->title, $r[0]->title);
        });

        $g = array_reduce($groups, function($all, $elem) {
            return array_merge($all, $elem);
        }, []);

        return view('admin.forum.index', ['categories' => $g]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function createCategory($parent_category = null)
    {
        return view('admin.forum.createCategory', [
            'category' => null,
            'parent_category' => $parent_category,
            'weight' => Category::all()->count() + 1,
            'topLevelCategories' => $this->getTopLevelCategoriesAsOptions()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function storeCategory(Requests\ForumCategoryRequest $request)
    {
        $category = new Category();
        $category->title = $request->get('title');
        $category->subtitle = $request->get('subtitle');
        $category->weight = $request->get('weight');
        $category->parent_category = ($request->get('parent_category') == 0) ? (null) : ($request->get('parent_category'));
        $category->save();

        return redirect(action('Admin\ForumController@index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function editCategory($id)
    {
        $category = Category::findOrFail($id);
        $parent_categories = $this->getTopLevelCategoriesAsOptions();
        unset($parent_categories[$category->id]);
        return view('admin.forum.editCategory', [
            'category' => $category,
            'topLevelCategories' => $parent_categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function updateCategory($id, Requests\ForumCategoryRequest $request) {
        $category = Category::findOrFail($id);
        $category->title = $request->get('title');
        $category->subtitle = $request->get('subtitle');
        $category->parent_category = ($request->get('parent_category') == 0) ? (null) : ($request->get('parent_category'));
        $category->save();
        return redirect(action('Admin\ForumController@index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroyCategory($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        if($category->parent_category == null) {
            Category::where('parent_category', $category->id)->delete();
        }
        return redirect(action('Admin\ForumController@index'));
    }

    private function getTopLevelCategoriesAsOptions()
    {
        return array_reduce($this->getTopLevelCategories()->all(), function ($result, $element) {
            $result[$element->id] = $element->title;
            return $result;
        }, ['null' => 'Категория верхнего уровня']);
    }

    private function getTopLevelCategories()
    {
        return Category::whereNull('parent_category')->get();
    }

}
