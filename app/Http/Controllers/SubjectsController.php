<?php namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\SubjectsRequest;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubjectsController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Subjects Controller
    |--------------------------------------------------------------------------
    |
    | Controller for working with diploma subjects.
    |
    */

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        return view('subjects.index')->with([
            'employees' => Employee::orderBy('surname')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $employees = Employee::select(
            DB::raw("(CONCAT(surname,' ',first_name,' ',patronymic)) AS full_name, id")
        )->orderBy('full_name')->lists('full_name', 'id');

        return view('subjects.create')->with([
            'employees' => $employees,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SubjectsRequest $request
     * @return Response
     */
    public function store(SubjectsRequest $request)
    {
        $subject = new Subject($request->all());
        $subject->save();

        return redirect(action('SubjectsController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return view('subjects.show')->with([
            'subject' => Subject::findOrFail($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $employees = Employee::select(
            DB::raw("(CONCAT(surname,' ',first_name,' ',patronymic)) AS full_name, id")
        )->orderBy('full_name')->lists('full_name', 'id');

        return view('subjects.edit')->with([
            'subject' => Subject::findOrFail($id),
            'employees' => $employees,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param SubjectsRequest $request
     * @return Response
     */
    public function update($id, SubjectsRequest $request)
    {
        $subject = Subject::findOrFail($id);
        $subject->update($request->all());

        return redirect(action('SubjectsController@index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $subject = Subject::findOrFail($id);
        $subject->delete();

        return redirect(action('SubjectsController@index'));
    }

}
