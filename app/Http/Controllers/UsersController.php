<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller {

	public function search($term = '')
    {
        $user = Auth::user();

        if (!$user) abort(404);

        $users = DB::table('users')
            ->where('firstname', 'LIKE', '%' . $term . '%')
            ->orWhere('lastname', 'LIKE', '%' . $term . '%')
            ->orWhere('patronymic', 'LIKE', '%' . $term . '%')
            ->where('id', '<>', $user->id)
            ->get([
                'id',
                'firstname',
                'lastname',
                'patronymic',
            ]);

        return response()->json($users);
    }

}
