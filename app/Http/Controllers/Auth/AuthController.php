<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
//use App\Http\Controllers\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    protected $redirectPath;
    protected $loginPath;

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard $auth
     * @param  \Illuminate\Contracts\Auth\Registrar $registrar
     * @return \App\Http\Controllers\Auth\AuthController
     */
    public function __construct(Guard $auth, Registrar $registrar)
    {
        parent::__construct();
        $this->auth = $auth;
        $this->registrar = $registrar;

        $this->redirectPath = action('HomeController@index');
        $this->loginPath = route('auth.login');

        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Handle a login request to the application.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if ($this->auth->attempt($credentials, $request->has('remember'), false))
        {
            $user = User::whereEmail($request->input('email'))->first();
            if ($user->active && $user->confirmed)
            {
                $this->auth->login($user, $request->has('remember'));

                return redirect()->intended($this->redirectPath());
            }
        }

        return redirect($this->loginPath())
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => $this->getFailedLoginMessage(),
            ]);
    }

    /**
     * Email verification confirmation.
     *
     * @param $code
     * @return \Illuminate\Http\Response
     */
    public function getVerify($code)
    {
        $user = User::where('confirmation_code', '=', $code)->first();

        if (!$user) abort(404);

        $user->active = true;
        $user->confirmed = true;
        $user->confirmation_code = null;

        if ($user->save())
        {
            $this->auth->login($user, true);

            return view('auth.confirmed')->with([
                'user' => $user,
            ]);
        }
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname'  => 'required|string|alpha|min:3|max:60',
            'lastname'   => 'required|string|alpha|min:3|max:60',
            'patronymic' => 'string|min:3|alpha|max:60',
            'email'      => 'required|email|unique:users',
            'group'      => 'numeric|exists:groups,id',
        ]);

        if ($validator->fails())
        {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user = $this->registrar->create($request->all());

        $user->assignRole('student');

        return view('auth.codesent')->with([
            'user' => $user,
        ]);
    }

}
