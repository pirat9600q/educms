<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Cookie;

abstract class Controller extends BaseController {

    use DispatchesCommands, ValidatesRequests;

    public function __construct()
    {
        if (!Cookie::has('lang'))
        {
            Cookie::queue('lang', 'ru', 43200);
        }
    }

    public function success($message)
    {
        session()->flash('message_type', 'success');
        session()->flash('message', $message);
    }

}
