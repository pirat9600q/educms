<?php namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Publication;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class QuestionsController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function ask($id)
    {
        $emp = Employee::findOrFail($id);

        if (!$emp->questions_enabled) abort(404);

        return view('questions.ask')->with([
            'emp' => $emp,
            'questions' => $emp->questions()->orderBy('created_at', 'desc')->paginate(5),
        ]);
    }

    /**
     * Show answer form.
     *
     * @return Response
     */
    public function answer($id)
    {
        $user = Auth::user();
        $question = Question::findOrFail($id);

        if (!$user || !$user->can('update.employee.' . $question->employee->id . '|update.employees')
            || !$question->employee->questions_enabled)
        {
            abort(404);
        }

        return view('questions.answer')->with([
            'question' => $question,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id, Request $request)
    {
        $employee = Employee::findOrFail($id);
        $email = '';

        if (!$employee->questions_enabled) abort(404);

        if (!Auth::user())
        {
            $validator = Validator::make($request->all(), [
                'text' => 'required|min:10|max:255',
                'submitter_email' => 'required|email'
            ]);

            if ($validator->fails())
            {
                return redirect(action('QuestionsController@ask', $id))
                    ->withErrors($validator)
                    ->withInput();
            }

            $email = $request->input('submitter_email');
        } else {
            $email = Auth::user()->email;
        }

        $question = new Question();
        $question->employee_id = $employee->id;
        $question->text = $request->input('text');
        $question->submitter_email = $email;
        $question->submitter_ip_address = $request->ip();
        $question->save();

        return redirect(action('EmployeesController@show', $id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function storeAnswer($id, Request $request)
    {
        $user = Auth::user();
        $question = Question::findOrFail($id);

        if (!$user || !$user->can('update.employee.' . $question->employee->id . '|update.employees')
            || !$question->employee->questions_enabled)
        {
            abort(404);
        }

        $question->answer = $request->input('answer');
        $question->save();

        return redirect(action('QuestionsController@ask', $question->employee->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $question = Question::findOrFail($id);
        $user = Auth::user();
        $empId = $question->employee->id;

        if (!$user|| !$user->can("update.employee.{$empId}|update.employees"))
        {
            abort(404);
        }

        $question->delete();
        return redirect(action('QuestionsController@ask', $empId));
    }

}
