<?php namespace App\Http\Controllers;

use App\Discipline;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\DisciplinesRequest;
use Illuminate\Http\Request;

class DisciplinesController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Disciplines Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for working with disciplines.
    |
    */

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('disciplines.index')->with([
            'disciplines' => Discipline::orderBy('name')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('disciplines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DisciplinesRequest $request
     * @return Response
     */
    public function store(DisciplinesRequest $request)
    {
        $discipline = new Discipline($request->all());
        $employees = $request->input('employees');
        $discipline->save();

        if ($employees)
        {
            $discipline->employees()->sync($employees);
        }

        return redirect(action('DisciplinesController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return view('disciplines.show')->with([
            'discipline' => Discipline::find($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('disciplines.edit')->with([
            'discipline' => Discipline::findOrFail($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param DisciplinesRequest $request
     * @return Response
     */
    public function update($id, DisciplinesRequest $request)
    {
        $discipline = Discipline::findOrFail($id);
        $employees = $request->input('employees');

        $discipline->employees()->sync($employees ? $employees : array());
        $discipline->update($request->all());

        return redirect(action('DisciplinesController@show', $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $discipline = Discipline::findOrFail($id);
        $discipline->delete();

        return redirect(action('DisciplinesController@index'));
    }

}
