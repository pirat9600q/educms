<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\MessagesRequest;
use App\Http\Controllers\Controller;

use App\MailingList;
use DB;
use App\IncomingMessage;
use App\Message;
use App\OutcomingMessage;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class MessagesController extends Controller {

	/**
	 * Display inbox.
	 *
	 * @return Response
	 */
	public function inbox()
	{
		return view('messages.index')->with([
            'messages' => Auth::user()->incomingMessages()->orderBy('created_at', 'desc')->get(),
        ]);
	}

    /**
     * Display outbox.
     *
     * @return $this
     */
    public function outbox()
    {
        return view('messages.index')->with([
            'messages' => Auth::user()->outcomingMessages()->orderBy('created_at', 'desc')->get(),
        ]);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('messages.create');
	}

    /**
     * Show message.
     *
     * @param $id
     * @return $this
     */
    public function show($id)
    {
        $message = Message::find($id);

        if ($message->sender->id != Auth::user()->id)
        {
            $incomingMessage = Auth::user()->incomingMessages()->where('message_id', $message->id)->first();
            $incomingMessage->opened = true;
            $incomingMessage->save();
        }

        return view('messages.show')->with([
            'message' => $message
        ]);
    }

    /**
     * Send message to user.
     *
     * @param  MessagesRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function send(MessagesRequest $request)
    {
        $receiver = (int) $request->input('receiver');
        $sender = Auth::user()->id;

        if ($receiver == $sender)
        {
            throw new BadRequestHttpException;
        }

        $message = new Message($request->all());
        $message->sender_id = $sender;
        $message->save();

        $message->receivers()->attach($receiver);

        $incomingMessage = new IncomingMessage();
        $incomingMessage->message()->associate($message);
        $incomingMessage->user_id = $receiver;
        $incomingMessage->save();

        $outcomingMessage = new OutcomingMessage();
        $outcomingMessage->message()->associate($message);
        $outcomingMessage->user_id = $sender;
        $outcomingMessage->save();

        return redirect(action('MessagesController@outbox'));
    }

    public function destroyAll(Request $request)
    {
        $messages = $request->input('messages');

        DB::table('incoming_messages')
            ->where('user_id', Auth::user()->id)
            ->whereIn('message_id', $messages)
            ->delete();
        DB::table('outcoming_messages')
            ->where('user_id', Auth::user()->id)
            ->whereIn('message_id', $messages)
            ->delete();

        return redirect()->back();
    }

    public function sendToAll(Request $request)
    {
        $ml = MailingList::findOrFail(session('mailingList'));
        $sender = Auth::user();

        $message = new Message();
        $message->title = session('title');
        $message->text = session('text');
        $message->sender()->associate($sender);
        $message->save();

        $outcomingMessage = new OutcomingMessage();
        $outcomingMessage->message()->associate($message);
        $outcomingMessage->user()->associate($sender);
        $outcomingMessage->save();

        foreach($ml->receivers as $receiver)
        {
            $incomingMessage = new IncomingMessage();
            $incomingMessage->message()->associate($message);
            $incomingMessage->user()->associate($receiver);
            $incomingMessage->save();

            $message->receivers()->attach($receiver);
        }

        $message->save();

        return redirect(action('MessagesController@inbox'));
    }

}
