<?php namespace App\Http\Controllers;

use App\Http\Requests;

use App\Http\Requests\NewsRequest;
use App\News;
use Chromabits\Purifier\Purifier;

class NewsController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | News Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles news creation, editing and display.
    |
    */

    /**
     * HtmlPurifier.
     *
     * @var Purifier
     */
    private $purifier;

    public function __construct(Purifier $purifier)
    {
        parent::__construct();
        $this->purifier = $purifier;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $news = News::latest()->paginate(5);

        return view('news.index')->with(['news' => $news]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NewsRequest $request
     * @return Response
     */
    public function store(NewsRequest $request)
    {
        $article = new News($request->all());
        $article->save();

        return redirect('news');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $article = News::findOrFail($id);

        return view('news.show')->with(['article' => $article]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $article = News::findOrFail($id);

        return view('news.edit')->with(['article' => $article]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  NewsRequest $request
     * @return Response
     */
    public function update($id, NewsRequest $request)
    {
        $article = News::findOrFail($id);
        $article->update($request->all());

        return redirect(url('news/' . $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $article = News::findOrFail($id);
        $article->delete();

        return redirect('news');
    }

}
