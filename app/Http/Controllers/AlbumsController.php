<?php namespace App\Http\Controllers;

use App\Album;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\AlbumsRequest;
use App\Http\Requests\GalleryRequest;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AlbumsController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Albums Controller
    |--------------------------------------------------------------------------
    |
    | Controller that handles gallery management.
    |
    */

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);

        return view('albums.index')->with([
            'albums' => Album::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('albums.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param GalleryRequest $request
     * @return Response
     */
    public function store(GalleryRequest $request)
    {
        $album = new Album($request->all());
        $album->save();

        return redirect(action('AlbumsController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $album = Album::findOrFail($id);
        $images = array();

        if ($album->images()->count() > 0)
        {
            $images = $album->images()->orderBy('order')->get();
        }

        return view('albums.show')->with([
            'album'  => $album,
            'images' => $images,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $album = Album::findOrFail($id);

        return view('albums.edit')->with([
            'album'  => $album,
            'images' => $album->images()->orderBy('order')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param GalleryRequest $request
     * @return Response
     */
    public function update($id, GalleryRequest $request)
    {
        $album = Album::findOrFail($id);
        $album->update($request->all());
        return redirect(action('AlbumsController@show', $id));
    }


    /**
     * Images uploading.
     *
     * @param $id
     * @param Request $request
     * @return $this
     */
    public function upload($id, Request $request)
    {
        if ($request->ajax())
        {

            $image = new Image();
            $file = Input::file('image');
            $image->name_ru = $file->getClientOriginalName();
            $image->name_en = $file->getClientOriginalName();
            $image->image = $file;
            $image->album_id = $id;
            $image->save();

            return redirect(action('AlbumsController@show', $id));
        }

        $album = Album::findOrFail($id);

        return view('albums.upload')->with([
            'album' => $album
        ]);
    }

    /**
     * Sorts images in a given order.
     * Input comes as an array of ids.
     *
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sort($id, Request $request)
    {
        $order = $request->get('order');

        if ($order)
        {
            $index = 0;

            foreach ($order as $imageId)
            {
                Image::find(intval($imageId))->update(['order' => $index++]);
            }

            if ($request->ajax())
            {
                return response()->json(['success' => true]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $album = Album::findOrFail($id);
        $album->delete();
        return redirect(action('AlbumsController@index'));
    }

}
