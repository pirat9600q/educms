<?php namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Publication;
use Illuminate\Http\Request;

class PublicationsController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Publications Controller
    |--------------------------------------------------------------------------
    |
    | Controller for publications management.
    |
    */

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('publications.index')->with([
            'publications' => Publication::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('publications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $publication = new Publication($request->all());
        $employees = $request->input('employees');
        $publication->save();

        if ($employees)
        {
            $publication->employees()->sync($employees);
        }

        return redirect(action('PublicationsController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return view('publications.show')->with([
            'publication' => Publication::findOrFail($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('publications.edit')->with([
            'publication' => Publication::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $publication = Publication::findOrFail($id);
        $employees = $request->input('employees');

        $publication->employees()->sync($employees ? $employees : array());
        $publication->update($request->all());

        return redirect(action('PublicationsController@show', $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $publication = Publication::findOrFail($id);
        $publication->delete();

        return redirect(action('PublicationsController@index'));
    }

}
