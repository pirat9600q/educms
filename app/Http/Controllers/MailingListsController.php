<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MailingList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class MailingListsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('messages.mailinglists.index', [
            'mailingLists' => Auth::user()->mailingLists
        ]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('messages.mailinglists.createOrEdit', [
                'action' => action('MailingListsController@store'),
                'method' => 'POST',
                'mailingList' => new MailingList()
            ]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\MailingListsRequest $request)
	{
        $receivers = array_unique($request->input('receivers'));

		$mailingList = new MailingList();
        $mailingList->name = $request->input('name');
        $mailingList->user()->associate(Auth::user());
        $mailingList->save();
        $mailingList->receivers()->attach($receivers);
        $mailingList->save();

        return redirect(action('MailingListsController@index'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $ml = MailingList::findOrFail($id);

		return view('messages.mailinglists.show', [
            'mailingList' => $ml
        ]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$ml = MailingList::findOrFail($id);

        return view('messages.mailinglists.createOrEdit', [
            'action' => action('MailingListsController@update', ['id' => $id]),
            'method' => 'PATCH',
            'mailingList' => $ml
        ]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Requests\MailingListsRequest $request)
	{
		$ml = MailingList::findOrFail($id);

        $receivers = array_unique($request->input('receivers'));

        $ml->name = $request->input('name');
        $ml->receivers()->sync($receivers);
        $ml->save();

        return redirect(action('MailingListsController@index'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @return Response
	 */
	public function destroy(Request $request)
	{
        $ids = $request->input('mailingLists');

        MailingList::query()->whereIn('id', $ids)->delete();

        return redirect()->back();
	}

    public function composeMessage($id)
    {
        $ml = MailingList::findOrFail($id);

        return view('messages.mailinglists.composeMessage', [
            'mailingList' => $ml
        ]);
    }

    public function sendMessage($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:100',
            'text' => 'required'
        ]);

        return redirect(action('MessagesController@sendToAll'))
            ->with('mailingList', $id)
            ->with('title', $request->input('title'))
            ->with('text', $request->input('text'));
    }

}
