<?php namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Kodeine\Acl\Models\Eloquent\Permission;

class EmployeesController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Employees Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for working with employees.
    |
    */

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('employees.index')->with([
            'employees' => Employee::orderBy('order', 'asc')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|alpha|min:3|max:32',
            'surname' => 'required|alpha|min:3|max:32',
            'patronymic' => 'required|alpha|min:3|max:32',
            'degree' => 'min:3|max:255',
            'post' => 'min:3|max:255',
            'pagetext' => 'max:255',
            'photo' => 'image|max:8142',
            'questions_enabled' => 'boolean'
        ]);

        if ($validator->fails())
        {
            return redirect(action('EmployeesController@create'))
                    ->withErrors($validator)
                    ->withInput();
        }

        $emp = new Employee($request->all());
        $emp->save();

        $permEmp = Permission::create([
            'name'        => 'employee_' . $emp->id,
            'slug'        => [
                'create' => true,
                'view'   => true,
                'update' => true,
                'delete' => true,
            ],
            'description' => 'Manage page of employee with id ' . $emp->id,
        ]);

        return redirect(action('EmployeesController@index'));
    }

    public function sort(Request $request)
    {
        $order = $request->get('order');
        foreach($order as $index => $employeeId)
        {
            Employee::findOrFail($employeeId)->update(['order'=>$index]);
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $emp = Employee::findOrFail($id);

        return view('employees.show')->with([
            'emp'                      => $emp,
            'publications'             => array_slice($emp->publications->toArray(), 0, 10),
            'thereAreMorePublications' => $emp->publications()->count() > 10,
            'user'                     => Auth::user()
        ]);
    }

    /**
     * Show publications of employee.
     *
     * @param $id
     * @return Response
     */
    public function showPublications($id)
    {
        $emp = Employee::find($id);

        return view('employees.publications')->with([
            'publications' => $emp->publications()->paginate(15),
        ]);
    }

    /**
     * Handle ajax query for typeahead.js plugin.
     *
     * @param null $term
     * @return mixed
     */
    public function search($term = null)
    {
        $emps = DB::table('employees')
            ->where('first_name', 'LIKE', '%' . $term . '%')
            ->orWhere('surname', 'LIKE', '%' . $term . '%')
            ->orWhere('patronymic', 'LIKE', '%' . $term . '%')
            ->get([
                'id',
                'first_name',
                'surname',
                'patronymic',
            ]);

        return response()->json($emps);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        if (!Auth::user()->can("update.employee_$id|update.employees"))
        {
            return abort(404);
        }

        if ($request->ajax())
        {
            // At the moment, the only case where ajax request occurs is when
            // updating page text field, so we handle it in another function.
            return $this->updatePagetext($id, $request);
        }

        $validator = Validator::make($request->all(), [
            'first_name' => 'required|alpha|min:3|max:32',
            'surname' => 'required|alpha|min:3|max:32',
            'patronymic' => 'required|alpha|min:3|max:32',
            'degree' => 'min:3|max:255',
            'post' => 'min:3|max:255',
            'photo' => 'image|max:8142',
            'questions_enabled' => 'boolean'
        ]);

        if ($validator->fails())
        {
            return redirect(action('EmployeesController@show', $id))
                ->withErrors($validator)
                ->withInput();
        }

        $attribures = $request->all();

        if ($request->hasFile('photo'))
        {
            $attribures['photo'] = $request->file('photo');
        }

        if (!isset($attribures['questions_enabled']))
        {
            $attribures['questions_enabled'] = 0;
        }

        Employee::find($id)->update($attribures);

        return redirect(action('EmployeesController@show', $id));
    }

    /**
     * Update page text data of employee.
     *
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updatePagetext($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pagetext' => 'max:1024'
        ]);

        if ($validator->fails())
        {
            return response()->json(null, 500);
        }

        Employee::find($id)->update($request->all());

        return response()->json(null, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $emp = Employee::findOrFail($id);
        $emp->delete();
        $permEmp = Permission::where('name', '=', 'employee_' . $id);
        $permEmp->delete();

        return redirect(action('EmployeesController@index'));
    }

}
