<?php

use App\Album;
use App\Category;
use App\Discipline;
use App\Employee;
use App\News;
use App\Publication;
use App\Studyplan;
use App\Subject;
use Kalnoy\Nestedset\Node;

/**
 * Home
 */
Breadcrumbs::register('home', function ($breadcrumbs)
{
    $breadcrumbs->push(_t('pages.home'), action('HomeController@index'));
});

/**
 * News
 */
Breadcrumbs::register('news', function ($breadcrumbs)
{
    $breadcrumbs = push_parents(category('NewsController@index'), $breadcrumbs);
    $breadcrumbs->push(_t('pages.news'), action('NewsController@index'));
});

Breadcrumbs::register('news.create', function ($breadcrumbs)
{
    $breadcrumbs->parent('news');
    $breadcrumbs->push(_t('labels.create') . " " . _t('labels.article'), action('NewsController@create'));
});

Breadcrumbs::register('news.edit', function ($breadcrumbs)
{
    $breadcrumbs->parent('news');
    $breadcrumbs->push(_t('labels.edit') . " " . _t('labels.article'), action('NewsController@edit'));
});

Breadcrumbs::register('news.show', function ($breadcrumbs, $articleId)
{
    $article = News::find($articleId);
    $breadcrumbs->parent('news');
    $breadcrumbs->push($article->title(), action('NewsController@show', $articleId));
});

/**
 * Employees
 */
Breadcrumbs::register('employees', function ($breadcrumbs)
{
    $breadcrumbs = push_parents(category('EmployeesController@index'), $breadcrumbs);
    $breadcrumbs->push(_t('pages.employees'), action('EmployeesController@index'));
});

Breadcrumbs::register('employees.create', function ($breadcrumbs)
{
    $breadcrumbs->parent('employees');
    $breadcrumbs->push(_t('labels.create') . " " . _t('labels.of_employee'), action('EmployeesController@create'));
});

Breadcrumbs::register('employees.show', function ($breadcrumbs, $id)
{
    $employee = Employee::find($id);
    $breadcrumbs->parent('employees');
    $breadcrumbs->push($employee->fullName(), action('EmployeesController@show', $id));
});

Breadcrumbs::register('employees.publications', function ($breadcrumbs, $id)
{
    $breadcrumbs->parent('employees.show', $id);
    $breadcrumbs->push('Публикации', action('EmployeesController@showPublications', $id));
});

Breadcrumbs::register('employees.ask', function ($breadcrumbs, $id)
{
    $breadcrumbs->parent('employees.show', $id);
    $breadcrumbs->push('Задать вопрос', action('QuestionsController@ask', $id));
});

Breadcrumbs::register('employees.answers', function ($breadcrumbs, $id)
{
    $breadcrumbs->parent('employees.show', $id);
    $breadcrumbs->push('Ответы на вопросы', action('QuestionsController@answers', $id));
});

/**
 * Publications
 */
Breadcrumbs::register('publications', function ($breadcrumbs)
{
    $breadcrumbs = push_parents(category('PublicationsController@index'), $breadcrumbs);
    $breadcrumbs->push(_t('pages.publications'), action('PublicationsController@index'));
});

Breadcrumbs::register('publications.show', function ($breadcrumbs, $id)
{
    $pub = Publication::find($id);
    $breadcrumbs->parent('publications');
    $breadcrumbs->push(_t('labels.viewing') . " " . _t('labels.of_publication'), action('PublicationsController@show', $id));
});

Breadcrumbs::register('publications.create', function ($breadcrumbs)
{
    $breadcrumbs->parent('publications');
    $breadcrumbs->push('Создание', action('PublicationsController@show'));
});

Breadcrumbs::register('publications.edit', function ($breadcrumbs, $id)
{
    $breadcrumbs->parent('publications.show', $id);
    $breadcrumbs->push('Редактирование');
});


/**
 * Studyplan
 */
Breadcrumbs::register('studyplan', function ($breadcrumbs)
{
    $breadcrumbs = push_parents(category('StudyplansController@index'), $breadcrumbs);
    $breadcrumbs->push(_t('pages.studyplan'), action('StudyplansController@index'));
});

Breadcrumbs::register('studyplan.create', function ($breadcrumbs)
{
    $breadcrumbs->parent('studyplan');
    $breadcrumbs->push('Добавить', action('StudyplansController@create'));
});

Breadcrumbs::register('studyplan.edit', function ($breadcrumbs, $id)
{
    $pub = Studyplan::find($id);
    $breadcrumbs->parent('studyplan');
    $breadcrumbs->push('Редактирование', action('StudyplansController@edit', $id));
});

/**
 * Gallery
 */
Breadcrumbs::register('gallery', function ($breadcrumbs)
{
    $breadcrumbs = push_parents(category('AlbumsController@index'), $breadcrumbs);
    $breadcrumbs->push(_t('pages.gallery'), action('AlbumsController@index'));
});

Breadcrumbs::register('gallery.show', function ($breadcrumbs, $id)
{
    $album = Album::find($id);
    $breadcrumbs->parent('gallery');
    $breadcrumbs->push($album->name(), action('AlbumsController@show', $id));
});

Breadcrumbs::register('gallery.edit', function ($breadcrumbs, $id)
{
    $album = Album::find($id);
    $breadcrumbs->parent('gallery');
    $breadcrumbs->push($album->name(), action('AlbumsController@show', $id));
    $breadcrumbs->push(_t('labels.editing'));
});

Breadcrumbs::register('gallery.upload', function ($breadcrumbs, $id)
{
    $album = Album::find($id);
    $breadcrumbs->parent('gallery');
    $breadcrumbs->push($album->name(), action('AlbumsController@show', $id));
    $breadcrumbs->push('Загрузка изображений');
});

/**
 * Disciplines
 */
Breadcrumbs::register('discipline', function ($breadcrumbs)
{
    $breadcrumbs = push_parents(category('DisciplinesController@index'), $breadcrumbs);
    $breadcrumbs->push('Дисциплины', action('DisciplinesController@index'));
});

Breadcrumbs::register('discipline.show', function ($breadcrumbs, $id)
{
    $discipline = Discipline::find($id);
    $breadcrumbs->parent('discipline');
    $breadcrumbs->push($discipline->name, action('DisciplinesController@show', $id));
});

Breadcrumbs::register('discipline.create', function ($breadcrumbs)
{
    $breadcrumbs->parent('discipline');
    $breadcrumbs->push('Создание', action('DisciplinesController@show'));
});

Breadcrumbs::register('discipline.edit', function ($breadcrumbs, $id)
{
    $breadcrumbs->parent('discipline.show', $id);
    $breadcrumbs->push('Редактирование');
});

/**
 * Subjects
 */
Breadcrumbs::register('subjects', function ($breadcrumbs)
{
    $breadcrumbs = push_parents(category('SubjectsController@index'), $breadcrumbs);
    $breadcrumbs->push('Темы дипломов', action('SubjectsController@index'));
});

Breadcrumbs::register('subjects.show', function ($breadcrumbs, $id)
{
    $discipline = Subject::find($id);
    $breadcrumbs->parent('subjects');
    $breadcrumbs->push($discipline->name, action('SubjectsController@show', $id));
});

Breadcrumbs::register('subjects.create', function ($breadcrumbs)
{
    $breadcrumbs->parent('subjects');
    $breadcrumbs->push('Создание', action('SubjectsController@show'));
});

Breadcrumbs::register('subjects.edit', function ($breadcrumbs, $id)
{
    $breadcrumbs->parent('subjects.show', $id);
    $breadcrumbs->push('Редактирование');
});

/**
 * Pages
 */
Breadcrumbs::register('pages', function ($breadcrumbs, $page)
{
    $category = category($page);
    $breadcrumbs = push_parents($category, $breadcrumbs);
    $breadcrumbs->push($category->title(), url('/' . $page));
});

if (!function_exists('push_parents'))
{
    function push_parents($node, $breadcrumbs)
    {
        if ($node == null) return $breadcrumbs;

        $ancestors = $node->getAncestors();

        if ($ancestors->count() > 0)
        {
            foreach ($ancestors as $ancestor)
            {
                $breadcrumbs->push($ancestor->title());
            }
        }

        return $breadcrumbs;
    }
}

if (!function_exists('category'))
{
    function category($content)
    {
        return Category::where('content', '=', $content)->first();
    }
}