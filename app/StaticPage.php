<?php namespace App;

use App\Category;

class StaticPage {

    /**
     * Read static page data from view.
     *
     * @param $filename
     * @param null $lang
     * @return \Illuminate\View\View
     */
    public static function read($filename, $lang = null)
    {
        if ($lang == null)
        {
            $lang = user_lang_or_default();
        }

        return static::unwrapContent(
            file_get_contents(static::pagesDir($lang) . DIRECTORY_SEPARATOR . $filename . '.blade.php')
        );
    }

    /**
     * Write static page data to view.
     *
     * @param string $filename
     * @param string $text
     * @param string $lang
     */
    public static function write($filename, $text, $lang = 'ru')
    {
        $path = static::pagesDir($lang) . DIRECTORY_SEPARATOR . $filename . '.blade.php';
        file_put_contents($path, static::wrapContent($text));
    }

    /**
     * Wraps given page content in so it takes default page view.
     *
     * @param $content
     * @return string
     */
    protected static function wrapContent($content)
    {
        return "@extends('app')@section('content')"
        . "<div class=\"panel panel-default\">"
        . "<div class=\"panel-body\">"
        . $content
        . "</div></div>@endsection";
    }

    /**
     * Unwraps content from page template.
     *
     * @param $content
     * @return string
     */
    public static function unwrapContent($content)
    {
        $needle = "@section('content')" . '<div class="panel panel-default"><div class="panel-body">';

        $sectionStart = strpos($content, $needle) + strlen($needle);
        $sectionEnd = strpos($content, "@endsection", $sectionStart);

        return substr($content, $sectionStart, $sectionEnd - $sectionStart);
    }

    /**
     * Returns string path to 'pages' views directory.
     *
     * @param null $lang
     * @return string
     */
    public static function pagesDir($lang = null)
    {
        $result = views_path() . DIRECTORY_SEPARATOR . 'pages';
        if ($lang != null)
        {
            $result .= DIRECTORY_SEPARATOR . $lang;
        }

        return $result;
    }

    /**
     * Deletes static page.
     *
     * @param $filename
     */
    public static function delete($filename)
    {
        $pageRu = static::pagesDir('ru') . DIRECTORY_SEPARATOR . $filename . '.blade.php';
        $pageEn = static::pagesDir('en') . DIRECTORY_SEPARATOR . $filename . '.blade.php';
        if (file_exists($pageRu)) unlink($pageRu);
        if (file_exists($pageEn)) unlink($pageEn);
    }
}
