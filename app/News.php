<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;

class News extends Model {

    protected $fillable = [
        'title_ru',
        'title_en',
        'text_ru',
        'text_en'
    ];

    /**
     * Returns title of given $lang locale.
     * Otherwise, returns title of article depending on user language settings.
     *
     * @param null $lang
     * @return mixed
     */
    public function title($lang = null)
    {
        if ($lang == null || !in_array($lang, ['ru', 'en']))
        {
            $lang = user_lang_or_default();
        }

        if ($lang == 'en' && $this->title_en == null)
        {
            $lang = 'ru';
        }

        return $this->attributes['title_' . $lang];
    }

    /**
     * Returns text of given $lang locale.
     * Otherwise, returns text of article depending on user language settings.
     *
     * @param null $lang
     * @return mixed
     */
    public function text($lang = null)
    {
        if ($lang == null || !in_array($lang, ['ru', 'en']))
        {
            $lang = user_lang_or_default();
        }

        if ($lang == 'en' && $this->text_en == null)
        {
            $lang = 'ru';
        }

        return $this->attributes['text_' . $lang];
    }

}
