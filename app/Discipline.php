<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Discipline extends Model {

    protected $fillable = [
        'name',
        'abbreviation',
        'description'
    ];

    /**
     * Study plans in which discipline appears.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function studyplans()
    {
        return $this->hasMany('App\Studyplan');
    }

    /**
     * Employees that connected with current discipline.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function employees()
    {
        return $this->belongsToMany('App\Employee');
    }

}
