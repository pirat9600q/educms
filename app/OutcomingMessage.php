<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class OutcomingMessage extends Model {

    /**
     * Belongs to one user.
     *
     * @returns \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Message for user.
     *
     * @returns \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function message()
    {
        return $this->belongsTo('App\Message');
    }

}
