<?php namespace App;

use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Database\Eloquent\Model;

class Album extends Model implements StaplerableInterface {

    use EloquentTrait;

    protected $fillable = [
        'name_ru',
        'name_en',
        'description_ru',
        'description_en',
        'thumbnail'
    ];

    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('thumbnail', [
            'styles'         => [
                'thumbnail' => [
                    'dimensions'      => '500#x360#',
                    'convert_options' => ['quality' => '60']
                ],
                'medium'    => [
                    'dimensions'      => '640x480',
                    'convert_options' => ['quality' => '80']
                ],
            ],
            'url'            => '/img/a/:id/:style/:secure_hash.:extension',
            'keep_old_files' => false,
            'default_url'    => '/img/image-placeholder.png',
        ]);
        parent::__construct($attributes);
    }

    /**
     * Album can have many images.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany('App\Image');
    }

    /**
     * Returns name of given $lang locale.
     * Otherwise, returns name of album depending on user language settings.
     *
     * @param null $lang
     * @return mixed
     */
    public function name($lang = null)
    {
        if ($lang == null || !in_array($lang, ['ru', 'en']))
        {
            $lang = user_lang_or_default();
        }

        if ($lang == 'en' && ($this->name_en == null || $this->name_en == ''))
        {
            $lang = 'ru';
        }

        return $this->attributes['name_' . $lang];
    }

}
