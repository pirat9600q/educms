<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model {

	protected $table = 'pages';

	protected $fillable = ['title_ru', 'title_en', 'content_ru', 'content_en'];

}
