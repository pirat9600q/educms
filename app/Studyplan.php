<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Studyplan extends Model {

    protected $fillable = [
        'discipline_id',
        'semester',
        'hours',
        'has_project',
        'degree'
    ];


    /**
     * Study plan refers to discipline.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function discipline()
    {
        return $this->belongsTo('App\Discipline');
    }

}
