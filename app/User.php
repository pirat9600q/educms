<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Kodeine\Acl\Traits\HasRole;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword, HasRole;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['firstname', 'lastname', 'patronymic', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Messages that user can access.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function messages()
    {
        return $this->belongsToMany('App\Message');
    }

    /**
     * User's unread messages.
     *
     * @return Collection
     */
    public function unreadMessages()
    {
        $unread = new Collection();

        foreach ($this->incomingMessages()->get() as $message)
        {
            if (!$message->opened)
            {
                $unread->add($message);
            }
        }

        return $unread;
    }

    /**
     * Returns user's short name. (i.e "Иван Иванов")
     *
     * @return string
     */
    public function name()
    {
        return $this->firstname . " " . $this->lastname;
    }

    /**
     * Returns user's full name. (i.e "Иванов Иван Иванович")
     *
     * @return string
     */
    public function fullName()
    {
        return $this->lastname . " " . $this->firstname . " " . $this->patronymic;
    }

    /**
     * Returns all employees pages that current user can edit.
     *
     * @return array|null
     */
    public function employeesPages()
    {
        $permissions = $this->getPermissions();
        if ($permissions == null || count($permissions) == 0) return null;

        $keys = preg_grep('/employee_([0-9]+)/', array_keys($permissions));
        $values = [];

        foreach ($keys as $key)
        {
            $id = (int) substr($key, strpos($key, '_') + 1);
            $values[] = Employee::find($id);
        }

        return $values;
    }

    /**
     * Returns user's studentinfo.
     *
     * @return string
     */
    public function studentInfo()
    {
        return $this->hasOne('App\StudentInfo');
    }

    public function hasPermission($permission)
    {
        return array_key_exists($permission, $this->getPermissions());
    }

    /**
     * Messages in user`s inbox.
     *
     * @return \Illuminate\Database\\Eloquent\Relations\HasMany
     */
    public function incomingMessages()
    {
        return $this->hasMany('App\IncomingMessage');
    }

    /**
     * Messages in user`s outbox.
     *
     * @return \Illuminate\Database\\Eloquent\Relations\HasMany
     */
    public function outcomingMessages()
    {
        return $this->hasMany('App\OutcomingMessage');
    }

    public function mailingLists()
    {
        return $this->hasMany('App\MailingList');
    }

    /**
     * Implements magic property "name" for using by laravel-forum component.
     *
     * @param string $name
     * @return mixed|string
     */
    public function __get($name)
    {
        return ($name === 'name') ? $this->fullName() : parent::__get($name);
    }

    /**
     * Implements magic property "name" for using by laravel-forum component.
     *
     * @param string $name
     * @return bool
     */
    public function __isset($name)
    {
        return ($name === 'name') ? true : parent::__isset($name);
    }

}
