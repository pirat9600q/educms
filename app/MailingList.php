<?php
/**
 * Created by PhpStorm.
 * User: lexa
 * Date: 7/6/15
 * Time: 12:47 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class MailingList extends Model {

    /**
     * User, which owns this mailing list.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function receivers()
    {
        return $this->belongsToMany('App\User');
    }

}
