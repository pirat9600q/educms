<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Publication extends Model {

    protected $fillable = [
        'title',
        'description'

    ];

    /**
     * All employees associated with publication.
     */
    public function employees()
    {
        return $this->belongsToMany('App\Employee');
    }

    /**
     * Get results sorted in alphabetical order.
     *
     * @param $query
     * @return mixed
     */
    public function scopeAlphabetically($query)
    {
        return $query->orderBy('title');
    }
}
