<?php namespace App\Services;

use App\StudentInfo;
use App\User;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
//		return Validator::make($data, [
//			'name' => 'required|max:255',
//			'email' => 'required|email|max:255|unique:users',
//			'password' => 'required|confirmed|min:6',
//		]);
        return Validator::make($data, [
            'firstname'  => 'required|string|alpha|min:3|max:60',
            'lastname'   => 'required|string|alpha|min:3|max:60',
            'patronymic' => 'required|string|min:0|alpha|max:60',
            'email'      => 'required|email|unique:users',
            'password'   => 'required|confirmed|min:6',
            'group'      => 'numeric|exists:groups,id',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    public function create(array $data)
    {
//		return User::create([
//			'name' => $data['name'],
//			'email' => $data['email'],
//			'password' => bcrypt($data['password']),
//		]);
        $user = new User([
            'firstname'  => $data['firstname'],
            'lastname'   => $data['lastname'],
            'patronymic' => $data['patronymic'],
            'email'      => $data['email'],
            'password'   => bcrypt($data['password']),
        ]);
        $user->active = true;
        $user->save();
        $user->assignRole('student');

        $studentInfo = new StudentInfo();
        $studentInfo->group_id = $data['group'];
        $studentInfo->user_id = $user->id;
        $studentInfo->save();
        
        return $user;
    }

}
