<?php

define('DS', DIRECTORY_SEPARATOR);

if (!function_exists('_t'))
{

    /**
     * Wrapper for trans() function.
     * Returns localized string depending on user settings, not global ones.
     *
     * @param   null $id
     * @param   array $parameters
     * @param   string $domain
     * @param   null $locale
     * @return  string
     */
    function _t($id = null, $parameters = array(), $domain = 'messages', $locale = null)
    {
        return trans($id, $parameters, $domain, $locale ? $locale : user_lang_or_default());
    }
}

if (!function_exists('user_lang_or_default'))
{

    /**
     * Returns language from cookie or default.
     *
     * @return string
     */
    function user_lang_or_default()
    {
        $lang = 'ru';

        if (Cookie::has('lang'))
        {
            $cookieLang = Cookie::get('lang');

            if (in_array($cookieLang, ['en', 'ru']))
            {
                $lang = $cookieLang;
            }
        }

        return $lang;
    }
}

if (!function_exists('views_path'))
{

    /**
     * Returns language from cookie or default.
     *
     * @return string
     */
    function views_path()
    {
        return base_path() . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'views';
    }
}

if (!function_exists('prepared_data'))
{

    /**
     * Returns item from 'prepared_data' folder.
     *
     * @param $item
     * @return string
     */
    function prepared_data($item)
    {
        $file = base_path() . DS . 'prepared_data' . DS . $item . '.php';
        if (!file_exists($file)) return '';

        return require($file);
    }
}

if (!function_exists('unlink_pages'))
{

    /**
     * Clears all static pages from views/pages folder.
     *
     * @return string
     */
    function unlink_pages()
    {
        $pathRu = views_path() . DS . 'pages' . DS . 'ru';
        $pathEn = views_path() . DS . 'pages' . DS . 'en';
        foreach (glob($pathRu . '/*') as $file) unlink($file);
        foreach (glob($pathEn . '/*') as $file) unlink($file);
    }
}