<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentInfo extends Model {

    /**
     * Student's user account.
     *
     * @return string
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
