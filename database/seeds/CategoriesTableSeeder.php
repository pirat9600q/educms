<?php

use App\Category;
use App\StaticPage;

class CategoriesTableSeeder extends DatabaseSeeder {

    public function run()
    {
        DB::table('categories')->delete();
        unlink_pages();
        
        $home = Category::create([
            'title_ru'  => 'Главная',
            'title_en'  => 'Home',
            'type'      => 'controller',
            'content'   => 'HomeController@index',
            'parent_id' => 'null'
        ]);

        $news = Category::create([
            'title_ru'  => 'Новости',
            'title_en'  => 'News',
            'type'      => 'controller',
            'content'   => 'NewsController@index',
            'parent_id' => 'null'
        ]);

        $employees = Category::create([
            'title_ru'  => 'Сотрудники кафедры',
            'title_en'  => 'Employees',
            'type'      => 'controller',
            'content'   => 'EmployeesController@index',
            'parent_id' => 'null'
        ]);

        $forStudents = Category::create([
            'title_ru'  => 'Студентам',
            'title_en'  => 'For students',
            'type'      => 'stub',
            'content'   => 'for_students',
            'parent_id' => 'null',
        ]);

        $studyplan = Category::create([
            'title_ru'  => 'Учебный план',
            'title_en'  => 'Study plan',
            'type'      => 'controller',
            'content'   => 'StudyplansController@index',
            'parent_id' => $forStudents->id
        ]);

        $disciplines = Category::create([
            'title_ru'  => 'Список дисциплин',
            'title_en'  => 'Disciplines',
            'type'      => 'controller',
            'content'   => 'DisciplinesController@index',
            'parent_id' => $forStudents->id
        ]);

        $subjects = Category::create([
            'title_ru'  => 'Темы дипломов',
            'title_en'  => 'Diplomas',
            'type'      => 'controller',
            'content'   => 'SubjectsController@index',
            'parent_id' => $forStudents->id
        ]);

        $commonInfo = Category::create([
            'title_ru'  => 'Общая информация',
            'title_en'  => 'Common info',
            'text_ru'   => prepared_data('common_ru'),
            'text_en'   => prepared_data('common_en'),
            'type'      => 'page',
            'content'   => 'common',
            'parent_id' => $forStudents->id
        ]);

        $forApplicants = Category::create([
            'title_ru'  => 'Для абитуриентов',
            'title_en'  => 'For applicants',
            'type'      => 'stub',
            'content'   => 'for_applicants',
            'parent_id' => 'null',
        ]);
        $studyProgram = Category::create([
            'title_ru'  => 'Программа обучения',
            'title_en'  => 'Study program',
            'text_ru'   => prepared_data('program_ru'),
            'text_en'   => prepared_data('program_ru'),
            'type'      => 'page',
            'content'   => 'program',
            'parent_id' => $forApplicants->id
        ]);

        $terms = Category::create([
            'title_ru'  => 'Условия поступления',
            'title_en'  => 'Admission',
            'text_ru'   => prepared_data('terms_ru'),
            'text_en'   => prepared_data('terms_ru'),
            'type'      => 'page',
            'content'   => 'terms',
            'parent_id' => $forApplicants->id
        ]);

        $science = Category::create([
            'title_ru'  => 'Научная работа',
            'title_en'  => 'Science',
            'text_ru'   => prepared_data('science_ru'),
            'text_en'   => prepared_data('science_ru'),
            'type'      => 'page',
            'content'   => 'science',
            'parent_id' => $forApplicants->id
        ]);

        $publications = $gallery = Category::create([
            'title_ru'  => 'Публикации',
            'title_en'  => 'Publications',
            'type'      => 'controller',
            'content'   => 'PublicationsController@index',
            'parent_id' => 'null'
        ]);

        $gallery = Category::create([
            'title_ru'  => 'Галерея',
            'title_en'  => 'Gallery',
            'type'      => 'controller',
            'content'   => 'AlbumsController@index',
            'parent_id' => 'null'
        ]);

        $path = Category::create([
            'title_ru'  => 'Схема проезда',
            'title_en'  => 'Map',
            'text_ru'   => prepared_data('path_ru'),
            'text_en'   => prepared_data('path_ru'),
            'type'      => 'page',
            'content'   => 'path',
            'parent_id' => 'null'
        ]);

        $forum = Category::create([
            'title_ru'  => 'Форум',
            'title_en'  => 'Forum',
            'text_ru'   => prepared_data('path_ru'),
            'text_en'   => prepared_data('path_ru'),
            'type'      => 'page',
            'content'   => 'forum',
            'parent_id' => 'null'
        ]);

    }

} 