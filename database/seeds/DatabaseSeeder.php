<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('PagesSeeder');
        $this->call('SettingsTableSeeder');

        $this->call('RolesAndPermissionsSeeder');
        $this->call('UsersTableSeeder');
        $this->call('NewsTableSeeder');
        $this->call('CategoriesTableSeeder');
        $this->call('AlbumsAndImagesSeeder');
        $this->call('EmployeesTableSeeder');
        $this->call('PublicationsTableSeeder');
        $this->call('DisciplinesTableSeeder');
        $this->call('StudyplansTableSeeder');
        $this->call('GroupsTableSeeder');
        $this->call('SubjectsTableSeeder');
    }

}
