<?php

use App\Discipline;
use App\Employee;
use App\Subject;

class SubjectsTableSeeder extends DatabaseSeeder {

    public function run()
    {
        DB::table('subjects')->delete();

        $subjects = prepared_data('subjects');

        foreach ($subjects as $s)
        {
            $subject = new Subject();

            $subject->name = $s[4];
            $subject->year = $s[3];

            $emp = Employee::where('surname', '=', $s[0])->first();
            $subject->employee_id = $emp ? $emp->id : null;

            $subject->save();
        }
    }
}
