<?php

use App\Group;

class GroupsTableSeeder extends DatabaseSeeder {

    public function run()
    {
        DB::table('groups')->delete();

        $groups = [
            ['ИТб-11', true, 1],
            ['ИТб-12', true, 1],
            ['ИТб-13', true, 1],
            ['ИТб-14', true, 1],
            ['ИТб-21', true, 2],
            ['ИТб-22', true, 2],
            ['ИТб-23', true, 2],
            ['ИТб-24', true, 2],
            ['ИТб-31', true, 3],
            ['ИТб-32', true, 3],
            ['ИТб-41', true, 4],
        ];

        foreach ($groups as $group)
        {
            Group::create([
                'name'     => $group[0],
                'fulltime' => $group[1],
                'course'   => $group[2],
            ]);
        }
    }

}