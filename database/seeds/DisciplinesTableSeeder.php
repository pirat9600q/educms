<?php

use App\Discipline;
use App\Employee;

class DisciplinesTableSeeder extends DatabaseSeeder {

    public function run()
    {
        DB::table('disciplines')->delete();

        $disciplines = [
            ['Высшая математика', 'ВМ', 'Высшая математика и начало анализа.', array()],
            ['Алгоритмизация и программирование', 'АиП', 'Алгоритмизация и программирование.', emp(['Бондарев', 'Сметанина', 'Забаштанский'])],
            ['Химия', '', '', array()],
            ['Физика', '', 'Физика.', array()],
            ['Электротехника и электроника', 'ЭиЭ', '', emp(['Кудрявченко', 'Ляшенко', 'Коваленко'])],
            ['Схемотехника', '', '', emp(['Чернега', 'Кудрявченко'])],
            ['Архитектура компьютеров', 'АК', '', emp(['Чернега', 'Коваленко', 'Дрозин'])],
            ['Архитектура информационных систем', 'АИС', '', emp(['Чернега'])],
            ['Объектно-ориентированное программирование', 'ООП', '', emp(['Доронина', 'Забаштанский', 'Сметанина'])],
            ['Численные методы', 'ЧМ', '', emp(['Первухина', 'Рябовая'])],
            ['Интеллектуальный анализ данных', 'ИАД', '', emp(['Первухина', 'Голикова'])],
            ['Системный анализ', 'СА', '', emp(['Первухина', 'Рябовая'])],
            ['Теория вероятности, вероятностные процессы и математическая статистика', 'ТВПиМС', '', emp(['Доценко', 'Коваленко', 'Кузнецов'])],
            ['Теория информации', 'ТИ', '', emp(['Доценко', 'Коваленко', 'Кузнецов'])],
            ['Компьютерные сети', 'КС', '', emp(['Чернега', 'Коваленко', 'Кротов', 'Гущин'])],
            ['Системы искусственного интеллекта', 'СИИ', '', emp(['Бондарев', 'Сметанина', 'Забаштанский'])],
            ['Технологии защиты информации', 'ТЗИ', '', emp(['Кудрявченко'])],
            ['Технология распределенных систем и параллельных вычислений', 'ТРСиПВ', '', emp(['Кротов'])],
            ['Теория принятия решения', 'ТПР', '', emp(['Кротов'])],
            ['Веб-технологии и веб дизайн', '', '', emp(['Овчинников', 'Гущин', 'Тлуховская'])],
            ['Генетические алгоритмы и нечеткая обработка данных', 'ГАиНОД', '', emp(['Кротов'])],
            ['Компьютерная графика', 'КГ', '', emp(['Карлусов'])],
            ['Математические методы исследования операций', 'ММИО', '', emp(['Карлусов'])],
            ['Дополнительные разделы компьютерной графики', 'ДРКГ', '', emp(['Карлусов'])],
            ['Системное программирование', 'СП', '', emp(['Карлусов', 'Кузнецов'])],
            ['Управления IT проектами', '', '', emp(['Доронина'])],
            ['Организация баз данных и знаний', 'ОБДиЗ', '', emp(['Доронина', 'Тимофеева', 'Валентюк'])],
            ['Проектирование информационных систем', 'ТИС', '', emp(['Доронина'])],
            ['Основы теории марковских цепей', 'ОТМЦ', '', emp(['Доронина'])],
            ['Технологии баз данных', 'ТБД', '', emp(['Доронина', 'Тимофеева'])],
            ['Платформа Java', '', '', emp(['Овчинников', 'Кузнецов'])],
            ['Методы и средства хранения информации', 'МиСХИ', '', emp(['Овчинников', 'Тлуховская'])],
            ['Операционные системы', 'ОС', '', emp(['Овчинников', 'Дрозин'])],
            ['Дискретная математика', 'ДМ', '', emp(['Доценко', 'Заикина', 'Валентюк'])],
            ['Моделирование систем', 'МС', '', emp(['Тимофеева'])],
            ['Технологии компьютерного проектирование', 'ТКП', '', emp(['Строганов', 'Дрозин'])],
            ['Технологии создания программных продуктов', 'ТСПП', '', emp(['Строганов', 'Дрозин'])],
            ['Теория алгоритмов', 'ТА', '', emp(['Шишкевич'])],
            ['Кросс-платформенное программирование', 'КПП', '', emp(['Строганов', 'Гущин'])],
            ['Платформа .Net', '', '', emp(['Шишкевич'])],
            ['Платформа 1С', '', '', emp(['Шишкевич'])],
            ['Мобильные информационные технологии', 'МИС', '', emp(['Шишкевич'])],
            ['Основы разработки интерфейса пользователя', 'ОРИП', '', emp(['Шишкевич'])],
        ];

        foreach ($disciplines as $discipline)
        {
            $d = Discipline::create([
                'name'         => $discipline[0],
                'abbreviation' => $discipline[1],
                'description'  => $discipline[2]
            ]);

            foreach ($discipline[3] as $emp)
            {
                $d->employees()->attach($emp);
            }
        }
    }

}

function emp(array $surnames = array())
{
    $result = array();

    foreach ($surnames as $surname)
    {
        $emp = Employee::where('surname', '=', $surname)->first();

        if ($emp)
        {
            $result[] = $emp;
        }
    }

    return $result;
}