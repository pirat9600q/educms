<?php

class NewsTableSeeder extends DatabaseSeeder {
    
    public function run()
    {
        DB::table('news')->delete();

        $faker = Faker\Factory::create();

        for ($i = 0; $i < 50; $i++)
        {
            $article = [
                'user_id'  => $i + 1,
                'title_ru' => $faker->sentence(),
                'title_en' => $faker->sentence(),
                'text_ru'  => $faker->realText() . $faker->realText() . $faker->realText(),
                'text_en'  => $faker->realText() . $faker->realText() . $faker->realText()
            ];

            App\News::create($article);
        }
    }
    
}