<?php

use App\Album;
use App\Image;

class AlbumsAndImagesSeeder extends DatabaseSeeder {

    public function run()
    {
        Album::whereNotNull('id')->delete();
        DB::table('images')->delete();
        DB::table('albums')->delete();

        // Seeding albums with images from %prepared_data%/images/albums
        $path = base_path() . DS . 'prepared_data' . DS . 'images' . DS . 'albums';

        foreach (glob($path . DS . '*') as $albumPath)
        {
            $albumName = basename($albumPath);
            $album = new Album();
            $album->name_ru = $albumName;
            $album->name_en = $albumName . '_en';
            $album->description_ru = "Описание для {$album->name_ru}.";
            $album->description_en = "Description for {$album->name_en} goes here.";
            $album->save();

            // Adding images to album.
            $order = 1;
            foreach (glob($albumPath . DS . '*') as $imagePath)
            {
                $imageName = basename($imagePath);
                $image = new Image();
                $image->name_ru = $imageName;
                $image->name_en = $imageName . '_en';
                $image->order = $order;

                // For some reason, image gets removed after uploading
                // (Stapler is responsible for uploading, issue might come from him).
                // So we backup an image here before seeding it to the database.
                copy($albumPath . DS . $imageName, $albumPath . DS . $imageName . '_');
                $image->image = $albumPath . DS . $imageName;
                $image->album_id = $album->id;

                $image->save();

                // Rename an image to its origin filename so we can use seeder again without any problems.
                rename($albumPath . DS . $imageName . '_', $albumPath . DS . $imageName);

                ++$order;
            }

            $album->thumbnail = Image::all()->last()->image->path();
            $album->save();
        }
    }
}
