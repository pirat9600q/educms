<?php

use App\Setting;

class SettingsTableSeeder extends DatabaseSeeder {

    public function run()
    {
        DB::table('settings')->delete();

        Setting::setValue('global.title', 'Кафедра ИС');
        Setting::setValue('global.departmentName', 'Кафедра Информационных систем');

        Setting::setValue('auth.allowRegistration', true);
        Setting::setValue('auth.allowLogin', true);
    }

}