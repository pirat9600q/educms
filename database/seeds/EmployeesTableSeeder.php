<?php

use App\Employee;
use App\User;
use Kodeine\Acl\Models\Eloquent\Permission;

class EmployeesTableSeeder extends DatabaseSeeder {

    public function run()
    {
        DB::table('employees')->delete();

        $imagePath = base_path() . DS . 'prepared_data' . DS . 'images' . DS . 'employees';

        foreach (prepared_data('employees') as $e)
        {
            $employee = new Employee();
            $employee->first_name = $e[2];
            $employee->surname = $e[1];
            $employee->patronymic = $e[3];
            $employee->degree = $e[4];
            $employee->pagetext = $e[5];
            $employee->post = $e[6];
            $employee->questions_enabled = false;

            $img = $imagePath . DS . $e[7] . '.jpg';
            if (file_exists($img))
            {
                copy($img, $imagePath . DS . $e[7] . '_' . '.jpg');
                $employee->photo = $img;
                $employee->save();
                rename($imagePath . DS . $e[7] . '_' . '.jpg', $img);
            } else
            {
                $employee->save();
            }

            $permEmployee = Permission::create([
                'name'        => 'employee_' . $employee->id,
                'slug'        => [
                    'create' => true,
                    'view'   => true,
                    'update' => true,
                    'delete' => true,
                ],
                'description' => 'Manage page of employee with id ' . $employee->id,
            ]);

            if ($employee->surname == 'Доценко')
            {
                User::find(2)->assignPermission($permEmployee);
            }
        }
    }

}