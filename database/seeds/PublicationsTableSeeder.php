<?php

use App\Employee;
use App\Publication;

class PublicationsTableSeeder extends DatabaseSeeder {
    
    public function run()
    {
        DB::table('employee_publication')->delete();
        DB::table('publications')->delete();

        foreach (prepared_data('publications') as $p)
        {
            $publication = Publication::create($p['data']);
            $publication->employees()->attach($this->idsBySurnames($p['employees']));
        }
    }

    /**
     * Get id of author by his surname.
     *
     * @param $surname
     * @return mixed
     */
    public function idBySurname($surname = "")
    {
        return Employee::where('surname', '=', $surname)->first()->id;
    }


    /**
     * Get ids array for all given surnames.
     *
     * @param $surnames
     * @return array
     */
    public function idsBySurnames(array $surnames = array())
    {
        $result = array();

        foreach ($surnames as $surname)
        {
            $result[] = $this->idBySurname($surname);
        }

        return $result;
    }
    
}