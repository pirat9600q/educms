<?php

use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends DatabaseSeeder {

    public function run()
    {
        DB::table('users')->delete();

        $testAdmin = new User();
        $testAdmin->firstname = 'Ярослав';
        $testAdmin->lastname = 'Ильин';
        $testAdmin->patronymic = 'Иванович';
        $testAdmin->email = 'test.admin@example.net';
        $testAdmin->password = Hash::make('password');
        $testAdmin->active = true;
        $testAdmin->confirmed = true;
        $testAdmin->save();
        $testAdmin->assignRole('admin');

        $testEmployee = new User();
        $testEmployee->firstname = 'Роман';
        $testEmployee->lastname = 'Данилов';
        $testEmployee->patronymic = 'Романович';
        $testEmployee->email = 'test.employee@example.net';
        $testEmployee->password = Hash::make('password');
        $testEmployee->active = true;
        $testEmployee->confirmed = true;
        $testEmployee->save();
        $testEmployee->assignRole('employee');

        $testStudent = new User();
        $testStudent->firstname = 'Антон';
        $testStudent->lastname = 'Кулаков';
        $testStudent->patronymic = 'Витальевич';
        $testStudent->email = 'test.student@example.net';
        $testStudent->password = Hash::make('password');
        $testStudent->active = true;
        $testStudent->confirmed = true;
        $testStudent->save();
        $testStudent->assignRole('student');
    }

}