<?php

use Kodeine\Acl\Models\Eloquent\Permission;
use Kodeine\Acl\Models\Eloquent\Role;

class RolesAndPermissionsSeeder extends DatabaseSeeder {

    public function run()
    {
        DB::table('roles')->delete();
        DB::table('permissions')->delete();

        $permission = new Permission();

        $permNews = $permission->create([
            'name' => 'news',
            'slug' => [
                'create' => true,
                'view'   => true,
                'update' => true,
                'delete' => true,
            ],
        ]);

        $permAlbums = $permission->create([
            'name' => 'albums',
            'slug' => [
                'create' => true,
                'view'   => true,
                'update' => true,
                'delete' => true,
            ],
        ]);

        $permEmployees = $permission->create([
            'name' => 'employees',
            'slug' => [
                'create' => true,
                'view'   => true,
                'update' => true,
                'delete' => true,
            ],
        ]);

        $permStudyplans = $permission->create([
            'name' => 'studyplan',
            'slug' => [
                'create' => true,
                'view'   => true,
                'update' => true,
                'delete' => true,
            ],
        ]);

        $permDisciplines = $permission->create([
            'name' => 'disciplines',
            'slug' => [
                'create' => true,
                'view'   => true,
                'update' => true,
                'delete' => true,
            ],
        ]);

        $permSubjects = $permission->create([
            'name' => 'subjects',
            'slug' => [
                'create' => true,
                'view'   => true,
                'update' => true,
                'delete' => true,
            ],
        ]);

        $permPublications = $permission->create([
            'name' => 'publications',
            'slug' => [
                'create' => true,
                'view'   => true,
                'update' => true,
                'delete' => true,
            ],
        ]);

        $roleAdmin = new Role();
        $roleAdmin->name = 'Администратор';
        $roleAdmin->slug = 'admin';
        $roleAdmin->description = 'Can do anything.';
        $roleAdmin->save();
        $roleAdmin->assignPermission([
            $permNews,
            $permAlbums,
            $permEmployees,
            $permStudyplans,
            $permDisciplines,
            $permSubjects,
            $permPublications,
        ]);

        $roleStudent = new Role();
        $roleStudent->name = 'Студент';
        $roleStudent->slug = 'student';
        $roleStudent->description = 'Student.';
        $roleStudent->save();

        $roleEmployee = new Role();
        $roleEmployee->name = 'Сотрудник';
        $roleEmployee->slug = 'employee';
        $roleEmployee->description = 'Employee.';
        $roleEmployee->save();
    }
    
}