<?php

use \App\Page;

class PagesSeeder extends DatabaseSeeder
{

    public function run()
    {
        DB::table('pages')->delete();

        $page = new Page();
        $page->internal_name = 'main';
        $page->title_ru = 'Главная';
        $page->title_en = 'Main';
        $page->content_ru = prepared_data('home_ru');
        $page->content_en = prepared_data('home_en');
        $page->save();
    }
}