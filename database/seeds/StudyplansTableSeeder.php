<?php

use App\Discipline;
use App\Studyplan;

class StudyplansTableSeeder extends DatabaseSeeder {

    public function run()
    {
        DB::table('studyplans')->delete();

        $studyplans = [
            // discipline_id, semester, hours, degree, has_project
            [d('Высшая математика'), 1, 0, 'bachelor', 0],
            [d('Высшая математика'), 2, 0, 'bachelor', 0],
            [d('Высшая математика'), 3, 0, 'bachelor', 0],
            [d('Алгоритмизация и программирование'), 1, 0, 'bachelor', 0],
            [d('Алгоритмизация и программирование'), 2, 0, 'bachelor', 0],
            [d('Алгоритмизация и программирование'), 3, 0, 'bachelor', 1],
            [d('Химия'), 1, 0, 'bachelor', 0],
            [d('Физика'), 1, 0, 'bachelor', 0],
            [d('Физика'), 2, 0, 'bachelor', 0],
            [d('Электротехника и электроника'), 2, 0, 'bachelor', 0],
            [d('Схемотехника'), 3, 0, 'bachelor', 0],
            [d('Архитектура компьютеров'), 4, 0, 'bachelor', 0],
            [d('Архитектура компьютеров'), 5, 0, 'bachelor', 0],
        ];

        foreach ($studyplans as $studyplan)
        {
            Studyplan::create([
                'discipline_id' => $studyplan[0],
                'semester'      => $studyplan[1],
                'hours'         => $studyplan[2],
                'degree'        => $studyplan[3],
                'has_project'   => $studyplan[4],
            ]);
        }
    }
}

function d($disciplineName)
{
    return Discipline::where('name', '=', $disciplineName)->first()->id;
}