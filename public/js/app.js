$(document).ready(function () {
    $('.contains-subgroup').hover(function () {
        var $this = $(this);
        var $subgroup = $this.next('.subgroup');

        $subgroup.css({
            'top': $this.offset().top + 'px',
            'left': ($this.offset().left + parseInt($this.css('width'))) + 'px',
            'display': 'inline'
        });

        console.log($this.offset().top + 'px');
        console.log($this.offset().left + parseInt($this.css('width')));
    }, function () {
        var $this = $(this);
        var $subgroup = $this.next('.subgroup');

        $subgroup.css('display', 'none');
    });
});